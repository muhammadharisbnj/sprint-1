package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.design.hrh.banjaegaandroidapp.Model.VendorModel;
import com.design.hrh.banjaegaandroidapp.R;

import java.util.ArrayList;


/**
 * Created by Haroon G on 2/28/2018.
 */

public class VendorNetworkAdapter extends RecyclerView.Adapter<VendorNetworkAdapter.MyViewHolder> {
    private final Context context;

    private ArrayList<VendorModel> arraynavicons;

    private VendorModel Navscrollitemes;
    OnArchitectItemClickListener mItemClickListener;
    int selected_position = -1;


    public VendorNetworkAdapter(Context context, ArrayList<VendorModel> arraynavicons) {

        this.context = context;
        this.arraynavicons = arraynavicons;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView Navicon;
        TextView txtImgtag;


        public MyViewHolder(View view) {
            super(view);
            Navicon = view.findViewById(R.id.img_icon);
//            txtImgtag = view.findViewById(R.id.txt_category_name);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);

            if (mItemClickListener != null) {
                mItemClickListener.OnArchitectItemClick(view, getPosition());
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.architect_item, parent, false);

        return new MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Navscrollitemes = arraynavicons.get(position);


//        holder.Navicon.setImageResource(selected_position == position ? arraynavicons.get(position).getChangedrawable() : arraynavicons.get(position).getDrawable());

//        holder.txtImgtag.setText(arraynavicons.get(position).getImagetag());

        holder.Navicon.setImageResource(Navscrollitemes.getVendorImage());
//        holder.txtImgtag.setText(Navscrollitemes.getCategoryName());

       /* if (arraynavicons.get(position).isImagechanged()) {
            holder.Navicon.setImageResource(arraynavicons.get(position).getChangedrawable());

        } else {
            holder.Navicon.setImageResource(arraynavicons.get(position).getDrawable());
        }*/


    }


    @Override
    public int getItemCount() {
        return arraynavicons.size();
    }

    public interface OnArchitectItemClickListener {
        public void OnArchitectItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnArchitectItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }
    public void clearselection()
    {

        notifyItemChanged(1);
        selected_position = -1;
        notifyItemChanged(selected_position);
        notifyDataSetChanged();

    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}





