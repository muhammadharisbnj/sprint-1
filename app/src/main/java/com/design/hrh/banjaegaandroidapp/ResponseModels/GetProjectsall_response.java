package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetProjectsall_response {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("total_pages")
    @Expose
    private String totalPages;


    @SerializedName("result")
    @Expose
    private ArrayList<GetProjects_list> result ;

    @SerializedName("associated_architects")
    @Expose
    private Object associatedArchitects;


    @SerializedName("associated_vendors")
    @Expose
    private Object associatedVendors;



    @SerializedName("products_used")
    @Expose
    private Object productsUsed;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetProjectsall_response() {
    }
    public GetProjectsall_response(Integer status, String totalPages, ArrayList<GetProjects_list> result, Object associatedArchitects, Object associatedVendors, Object productsUsed) {
        super();
        this.status = status;
        this.totalPages = totalPages;
        this.result = result;
        this.associatedArchitects = associatedArchitects;
        this.associatedVendors = associatedVendors;
        this.productsUsed = productsUsed;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public List<GetProjects_list> getResult() {
        return result;
    }

    public void setResult(ArrayList<GetProjects_list> result) {
        this.result = result;
    }

    public Object getAssociatedArchitects() {
        return associatedArchitects;
    }

    public void setAssociatedArchitects(Object associatedArchitects) {
        this.associatedArchitects = associatedArchitects;
    }

    public Object getAssociatedVendors() {
        return associatedVendors;
    }

    public void setAssociatedVendors(Object associatedVendors) {
        this.associatedVendors = associatedVendors;
    }

    public Object getProductsUsed() {
        return productsUsed;
    }

    public void setProductsUsed(Object productsUsed) {
        this.productsUsed = productsUsed;
    }
    public class Result {
    }

}




