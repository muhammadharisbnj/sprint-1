package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.design.hrh.banjaegaandroidapp.AppController.VendorRatingNewActivity;
import com.design.hrh.banjaegaandroidapp.Fragments.No_problem;
import com.design.hrh.banjaegaandroidapp.Fragments.Price;
import com.design.hrh.banjaegaandroidapp.Fragments.Quality;
import com.design.hrh.banjaegaandroidapp.Fragments.Service;
import com.design.hrh.banjaegaandroidapp.Fragments.Submit;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;


public class Viewpager_adapter extends FragmentPagerAdapter {

    public Viewpager_adapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    Context context;


    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:

                return new Price();
            case 1:
                    return new Quality();
            case 2:

                return new Service();
            case 3:

                return new Submit();
            case 4:

                return new No_problem();
        }

        return null;
    }

    @Override
    public int getCount() {

        return 5;
    }
}
