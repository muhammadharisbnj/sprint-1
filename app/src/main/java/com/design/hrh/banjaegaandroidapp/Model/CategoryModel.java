package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/17/2018.
 */

public class CategoryModel {

    String CategoryName ;
    int CategoryDrawable;


    public CategoryModel(String categoryName, int categoryDrawable) {
        CategoryName = categoryName;
        CategoryDrawable = categoryDrawable;
    }


    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public int getCategoryDrawable() {
        return CategoryDrawable;
    }

    public void setCategoryDrawable(int categoryDrawable) {
        CategoryDrawable = categoryDrawable;
    }
}
