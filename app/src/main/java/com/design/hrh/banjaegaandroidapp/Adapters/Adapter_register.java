package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.design.hrh.banjaegaandroidapp.Model.NavScrollitems;
import com.design.hrh.banjaegaandroidapp.R;

import java.util.ArrayList;

public class Adapter_register  extends RecyclerView.Adapter<Adapter_register.MyViewHolder> {
    private final Context context;

    private ArrayList<NavScrollitems> arraynavicons;

    private NavScrollitems Navscrollitemes;
    Adapter_register.OnItemClickListener mItemClickListener;
    int selected_position = -1;


    public Adapter_register(Context context, ArrayList<NavScrollitems> arraynavicons) {

        this.context = context;
        this.arraynavicons = arraynavicons;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView Navicon;
        TextView txtImgtag;


        public MyViewHolder(View view) {
            super(view);
            Navicon = view.findViewById(R.id.img_icons);
            txtImgtag = view.findViewById(R.id.txt_tag);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    @Override
    public Adapter_register.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.register_items, parent, false);

        return new Adapter_register.MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final Adapter_register.MyViewHolder holder, int position) {
        Navscrollitemes = arraynavicons.get(position);


        holder.Navicon.setImageResource(selected_position == position ? arraynavicons.get(position).getChangedrawable() : arraynavicons.get(position).getDrawable());

        holder.txtImgtag.setText(arraynavicons.get(position).getImagetag());



    }


    @Override
    public int getItemCount() {
        return arraynavicons.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final Adapter_register.OnItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }
    public void clearselection()
    {

        notifyItemChanged(1);
        selected_position = -1;
        notifyItemChanged(selected_position);
        notifyDataSetChanged();

    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}






