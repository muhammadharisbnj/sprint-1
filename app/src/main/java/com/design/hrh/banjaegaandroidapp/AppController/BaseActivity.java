package com.design.hrh.banjaegaandroidapp.AppController;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public abstract class BaseActivity extends FragmentActivity {


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

    }
}
