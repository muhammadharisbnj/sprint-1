package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Search_model_category {
    @SerializedName("vendor_id")
    @Expose
    private Object vendorId;
    @SerializedName("architect_id")
    @Expose
    private String architectId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category")
    @Expose
    private Object category;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("membership_type")
    @Expose
    private String membershipType;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("is_mobile")
    @Expose
    private String isMobile;
    @SerializedName("is_email")
    @Expose
    private String isEmail;
    @SerializedName("is_trusted")
    @Expose
    private String isTrusted;
    @SerializedName("is_featured")
    @Expose
    private String isFeatured;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("cloud_tags")
    @Expose
    private String cloudTags;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("user_reviews")
    @Expose
    private String userReviews;
    @SerializedName("avg_service")
    @Expose
    private String avgService;
    @SerializedName("avg_pricing")
    @Expose
    private String avgPricing;
    @SerializedName("avg_quality")
    @Expose
    private String avgQuality;

    public Search_model_category(Object vendorId, String architectId, String userId, Object category, String phone, String mobile, String userType, String username, String membershipType, String logo, String city, String country, String isVerified, String isMobile, String isEmail, String isTrusted, String isFeatured, String address, String views, String cloudTags, String lat, String _long, String userReviews, String avgService, String avgPricing, String avgQuality) {
        this.vendorId = vendorId;
        this.architectId = architectId;
        this.userId = userId;
        this.category = category;
        this.phone = phone;
        this.mobile = mobile;
        this.userType = userType;
        this.username = username;
        this.membershipType = membershipType;
        this.logo = logo;
        this.city = city;
        this.country = country;
        this.isVerified = isVerified;
        this.isMobile = isMobile;
        this.isEmail = isEmail;
        this.isTrusted = isTrusted;
        this.isFeatured = isFeatured;
        this.address = address;
        this.views = views;
        this.cloudTags = cloudTags;
        this.lat = lat;
        this._long = _long;
        this.userReviews = userReviews;
        this.avgService = avgService;
        this.avgPricing = avgPricing;
        this.avgQuality = avgQuality;
    }

    public Object getVendorId() {
        return vendorId;
    }

    public void setVendorId(Object vendorId) {
        this.vendorId = vendorId;
    }

    public String getArchitectId() {
        return architectId;
    }

    public void setArchitectId(String architectId) {
        this.architectId = architectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getCategory() {
        return category;
    }

    public void setCategory(Object category) {
        this.category = category;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }

    public String getIsEmail() {
        return isEmail;
    }

    public void setIsEmail(String isEmail) {
        this.isEmail = isEmail;
    }

    public String getIsTrusted() {
        return isTrusted;
    }

    public void setIsTrusted(String isTrusted) {
        this.isTrusted = isTrusted;
    }

    public String getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getCloudTags() {
        return cloudTags;
    }

    public void setCloudTags(String cloudTags) {
        this.cloudTags = cloudTags;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getUserReviews() {
        return userReviews;
    }

    public void setUserReviews(String userReviews) {
        this.userReviews = userReviews;
    }

    public String getAvgService() {
        return avgService;
    }

    public void setAvgService(String avgService) {
        this.avgService = avgService;
    }

    public String getAvgPricing() {
        return avgPricing;
    }

    public void setAvgPricing(String avgPricing) {
        this.avgPricing = avgPricing;
    }

    public String getAvgQuality() {
        return avgQuality;
    }

    public void setAvgQuality(String avgQuality) {
        this.avgQuality = avgQuality;
    }

}



