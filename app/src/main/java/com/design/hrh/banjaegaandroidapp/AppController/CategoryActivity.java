package com.design.hrh.banjaegaandroidapp.AppController;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.design.hrh.banjaegaandroidapp.Adapters.CategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Model.CategoryModel;
import com.design.hrh.banjaegaandroidapp.R;

import java.util.ArrayList;


public class CategoryActivity extends AppCompatActivity implements CategoryAdapter.OnItemClickListener , View.OnClickListener{
RecyclerView rvCategory;
ArrayList<CategoryModel> Categories= new ArrayList<CategoryModel>();
CategoryAdapter mAdapter;
EditText etSearchCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        init();
    }

    private void init() {
        etSearchCategory=(EditText)findViewById(R.id.et_category_search);
        rvCategory=(RecyclerView)findViewById(R.id.rv_category);
        rvCategory.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
        Categories.add(new CategoryModel("Consultants",R.drawable.consultants));
        Categories.add(new CategoryModel("Builders",R.drawable.builders));
        Categories.add(new CategoryModel("Gold Vendord",R.drawable.vendors));
        Categories.add(new CategoryModel("Bathrooms",R.drawable.bathrooms));
        Categories.add(new CategoryModel("Kitchen",R.drawable.kitchens));
        Categories.add(new CategoryModel("Flooring",R.drawable.flooring));
        Categories.add(new CategoryModel("Paint & Polish",R.drawable.paint_polish));
        Categories.add(new CategoryModel("Horticulture",R.drawable.horticulture));
        Categories.add(new CategoryModel("Furniture",R.drawable.furniture));
        Categories.add(new CategoryModel("Tools & Hardware",R.drawable.tools_hardware));
        mAdapter=new CategoryAdapter(CategoryActivity.this,Categories);
        mAdapter.SetOnItemClickListener(this);
        rvCategory.setAdapter(mAdapter);
        etSearchCategory.setOnClickListener(this);
        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {



    }

    @Override
    public void onClick(View v) {
        if(v==etSearchCategory)
        {
            startActivity(new Intent(CategoryActivity.this,WhatAreYouLooking.class));
        }

    }
}
