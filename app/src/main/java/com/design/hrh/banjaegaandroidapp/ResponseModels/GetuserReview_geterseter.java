package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetuserReview_geterseter implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("pricing")
    @Expose
    private String pricing;
    @SerializedName("quality")
    @Expose
    private String quality;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("approved")
    @Expose
    private String approved;
    @SerializedName("project_id")
    @Expose
    private Object projectId;
    private final static long serialVersionUID = -5459952381742780274L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetuserReview_geterseter() {
    }

    /**
     *
     * @param pricing
     * @param id
     * @param approved
     * @param username
     * @param imageUrl
     * @param email
     * @param userId
     * @param quality
     * @param service
     * @param projectId
     * @param review
     * @param userType
     */
    public GetuserReview_geterseter(String id, String username, String email, String imageUrl, String userId, String userType, String service, String pricing, String quality, String review, String approved, Object projectId) {
        super();
        this.id = id;
        this.username = username;
        this.email = email;
        this.imageUrl = imageUrl;
        this.userId = userId;
        this.userType = userType;
        this.service = service;
        this.pricing = pricing;
        this.quality = quality;
        this.review = review;
        this.approved = approved;
        this.projectId = projectId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPricing() {
        return pricing;
    }

    public void setPricing(String pricing) {
        this.pricing = pricing;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public Object getProjectId() {
        return projectId;
    }

    public void setProjectId(Object projectId) {
        this.projectId = projectId;
    }

}
