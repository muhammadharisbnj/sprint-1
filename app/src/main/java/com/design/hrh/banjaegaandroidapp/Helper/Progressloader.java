package com.design.hrh.banjaegaandroidapp.Helper;

import android.app.ProgressDialog;
import android.content.Context;

import com.design.hrh.banjaegaandroidapp.R;

/**
 * Created by MuhammadAbubakar on 2/27/2018.
 */

public class Progressloader
{
    static ProgressDialog pd;

    public static void loader(Context context)
    {

         pd = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        pd.setMessage("Loading Please wait...");

        pd.show();
    }


    public static void hideloader()
    {

        pd.hide();
    }
}
