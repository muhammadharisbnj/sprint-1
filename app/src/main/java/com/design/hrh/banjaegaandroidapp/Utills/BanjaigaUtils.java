package com.design.hrh.banjaegaandroidapp.Utills;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.design.hrh.banjaegaandroidapp.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by karan on 26-11-2017.
 */

public class BanjaigaUtils {



    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        System.err.println(matcher.matches());
        return matcher.matches();
    }

    public static void showToast(String toastMsg, Context mActivity) {
        try {
            if (toastMsg != null) {
                Toast.makeText(mActivity, toastMsg, Toast.LENGTH_SHORT).show();
            }
        } catch (NullPointerException e) {

        }
    }

    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static File scaleBitmap(File fileForImage, int widthResolution){

        Bitmap bm = BitmapFactory.decodeFile(fileForImage.toString());

        int width=bm.getWidth();
        int height=bm.getHeight();

        if(width<=widthResolution){
            return fileForImage;
        }

        int newWidth=widthResolution;
        int newHeight=height*widthResolution/width;



        Bitmap newBitmap=Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);

        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(fileForImage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            newBitmap.compress(Bitmap.CompressFormat.PNG, 90, fOut);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileForImage;
    }
//
//    public static void replaceFragment(FragmentActivity activity, Fragment fragment, boolean addToBackStack){
//
//        if(addToBackStack) {
//            activity
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.frame_container_objects, fragment, fragment.getClass().getName())
//                    .addToBackStack(fragment.getClass().getName())
//                    .commitAllowingStateLoss();
//        }
//        else{
//            activity
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.frame_container_objects, fragment, fragment.getClass().getName())
//                    .commitAllowingStateLoss();
//        }
//    }
}
