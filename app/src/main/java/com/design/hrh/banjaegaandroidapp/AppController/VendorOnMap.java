package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Model.FeaturedArchitectures;
import com.design.hrh.banjaegaandroidapp.Model.YCategoryModel;
import com.design.hrh.banjaegaandroidapp.R;

import java.util.ArrayList;


public class VendorOnMap extends AppCompatActivity implements View.OnClickListener{

    Activity context = VendorOnMap.this;
    RecyclerView rvYCategory;
    ArrayList<YCategoryModel> YCategories = new ArrayList<>();
    YCategoryAdapter mAdapter;

    RelativeLayout rlVendorInfo;

    Button call,message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_on_map);
        init();
    }

    private void init() {

        call=(Button)findViewById(R.id.btn_call);
        message=(Button)findViewById(R.id.btn_message);
        rlVendorInfo=(RelativeLayout)findViewById(R.id.rl_archtect_info);
        rlVendorInfo.setOnClickListener(this);
        rvYCategory=(RecyclerView) findViewById(R.id.rv_category_yellow);
        rvYCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        YCategories.add(new YCategoryModel("Architect", R.drawable.architect_grey));
        YCategories.add(new YCategoryModel("Tiles", R.drawable.tiles));
        YCategories.add(new YCategoryModel("Bathrooms", R.drawable.bathrooms_grey));
        YCategories.add(new YCategoryModel("Builder", R.drawable.builders_grey));
        YCategories.add(new YCategoryModel("Kitchen", R.drawable.kitchens_grey));
        YCategories.add(new YCategoryModel("More", R.drawable.more));
        mAdapter = new YCategoryAdapter(context, YCategories);
        rvYCategory.setAdapter(mAdapter);

        call.setOnClickListener(this);

        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v==rlVendorInfo)
        {
            startActivity(new Intent(context,VendorListingOnMap.class));
        }
        if (v == call){
            MyHelper.makeCall(this,"123456789");
        }if (v == message){
            MyHelper.sendSMS(this,"hello");
        }

    }
}
