package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/17/2018.
 */

public class YCategoryModel {

    String CategoryName ;
    int CategoryDrawable;
    String CategoryTag;


    public YCategoryModel(String categoryName, int categoryDrawable) {
        CategoryName = categoryName;
        CategoryDrawable = categoryDrawable;
    }

    public YCategoryModel(String categoryName, int categoryDrawable, String categoryTag) {
        CategoryName = categoryName;
        CategoryDrawable = categoryDrawable;
        CategoryTag = categoryTag;
    }

    public String getCategoryTag() {
        return CategoryTag;
    }

    public void setCategoryTag(String categoryTag) {
        CategoryTag = categoryTag;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public int getCategoryDrawable() {
        return CategoryDrawable;
    }

    public void setCategoryDrawable(int categoryDrawable) {
        CategoryDrawable = categoryDrawable;
    }
}
