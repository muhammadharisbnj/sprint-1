package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.SerializedName;

public class Architects_lat_lang {
    @SerializedName("lat")
    public Double lat;
    @SerializedName("long_")
    public Double long_;


    public Architects_lat_lang(Double lat, Double long_) {
        this.lat = lat;
        this.long_ = long_;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLong_() {
        return long_;
    }

    public void setLong_(Double long_) {
        this.long_ = long_;
    }
}
