/*
package com.design.hrh.banjaegaandroidapp.AppController;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.design.hrh.banjaegaandroidapp.Adapters.AdapterSearchNames;
import com.design.hrh.banjaegaandroidapp.Adapters.Adapters_Nav;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.Model.NavScrollitems;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.Utills.CustomClusterRenderer;
import com.design.hrh.banjaegaandroidapp.Utills.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class MapsParent extends FragmentActivity implements OnMapReadyCallback, LocationSource.OnLocationChangedListener, View.OnClickListener, ClusterManager.OnClusterClickListener, ClusterManager.OnClusterItemClickListener, NavigationView.OnNavigationItemSelectedListener {
    private static LatLng LOCATIONS_POINTS = null;
    private GoogleMap mMap;
    private ClusterManager<StringClusterItem> mClusterManager;
    DataHelper dataHelper;
    private boolean isFoundArch = false;
    CardView contactInfo, cardEmailCall, cardupdateloc;
    RelativeLayout cardProfileInfo;
    TextView title, tvHeading, tvAddressName, tvOptionDesc, btnRateMe, tvcontactus, tvReviews, tvOptionDescdialog;
    ImageView canceCard, cancelArchView, cancelupdatefrom, cancelupdatefromdialog, cancelcardemaildialog;
    Button buttonSend, buttonSendVendorloc, buttonSendVendorlocdialog, buttonSenddialog, buttonsearch;
    EditText etUsername, etContact, etVendorName, etVendorContact, etvendorLocation,
            etVendorNamedialog, etVendorContactdialog, etvendorLocationdialog, etUsernamedialog, etContactdialog;
    ImageView ivThumbs, ivHome, ivCurtain, ivManGrey, ivFurniture, ivBath, ivNearBy, ivKitchen, ivCall, ivMsg, ivAdminCall, ivAdminMessage,
            ivCardHome, ivCardInterior, ivCardArch, ivCardCal, ivCardHomedialog, ivCardInteriordialog, ivCardArchdialog, ivCardCaldialog, ivNext,
            ivBack, ivVerifiedBadge, ivMedalBadge, ivMobileBadge, ivEmailBadge, ivpophelpcancel;

    double currentLatitude, currentLongitude;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    GPSTracker gpsTracker;
    private static final String TAG = MapsParent.class.getSimpleName();
    int viewWidth, mWidth, PLACE_AUTOCOMPLETE_REQUEST_CODE = 1, PLACE_AUTOCOMPLETE_REQUEST_CODE1 = 2, PLACE_AUTOCOMPLETE_REQUEST_CODE2 = 3;
    String ImgTag = "", Usertype = "", phoneNum, bitmapString, Showhidekeyboard = "yes";

    LinearLayout footerLayout;
    RadioButton emailView, archProfileView, updateloc;

    AutoCompleteTextView autoCompleteTextView, autoCompleteTextViewLoc;

    double latitude = 33.7205829, longitude = 73.0736015, searchLatitude, searchLongitude, vendorLatitude, vendorLongitude;
    CircleImageView circleImageView;
    String EMAILTAG;

    RecyclerView recyclerView;
    Adapters_Nav adapters_navicons;

    private ArrayList<NavScrollitems> arraynavicons = new ArrayList<>();
    private ArrayList<String> autocompletearray = new ArrayList<String>();
    private NavScrollitems Navscrollitemes;
    RadioGroup radioGroup;

    DrawerLayout drawer;
    Toolbar toolbar;
    NavigationView navigationView;
    AlertDialog alertDialog;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ScaleRatingBar Ratingbasedprofile;
    LinearLayout llNavMenu, llArchitechts, llKitchen, llFurniture, llBathrooms, llTiles, llProjects, llLight, llInterior;
    RelativeLayout popupHelp;
    // Top menu default images,change on click images and tag arrays

//    int[] myImageList = new int[]{R.drawable.man_grey, R.drawable.img_kitchen, R.drawable.furniture_grey,
//            R.drawable.bath_grey, R.drawable.tiles_grey, R.drawable.home_grey, R.drawable.lights_grey};

    int[] myImageList = new int[]{R.drawable.architect_grey, R.drawable.tiles, R.drawable.bathrooms_grey,
        R.drawable.builders_grey, R.drawable.kitchens_grey , R.drawable.more};
    int[] myImageListcolor = new int[]{R.drawable.man_yellow, R.drawable.kitchen_yellow, R.drawable.furniture_yellow,
            R.drawable.bath_yellow, R.drawable.tiles_yellow , R.drawable.tiles_yellow};
    String[] imagestag = new String[]{"Architects_lat_lang", "Kitchens", "Furniture", "Bathrooms", "Tiles", "More"};
    HashMap<String, String> searchparams = new HashMap<String, String>();
    HashMap<String, String> requestCall = new HashMap<String, String>();
    android.app.AlertDialog alertDialogRequestCall;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_layout);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();
        prefs.getBoolean("isFirstTime", false);
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            gpsTracker = new GPSTracker(this);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

 searchLatitude = gpsTracker.getLatitude();
            searchLongitude = gpsTracker.getLongitude();



        } else {
            gpsTracker.showSettingsAlert();
        }

        adjustKeyoardSettings();
        initalizeControls();
        settingNavigationdrawer();
        initVolleyCallback();

        //  Recyclervha
        // iew  top menu
        populaterecyclerviewdata();


        dataHelper = DataHelper.getInstance();
        if (savedInstanceState == null) {
            setupMapFragment();
        }

        onClickCalls();

        changelatlongtoaddress(latitude, longitude, "etvendor");

        contactInfo.setVisibility(View.GONE);
        mVolleyService = new VolleyService(mResultCallback, this);
        JSONObject sendObj = null;
        Display display = getWindowManager().getDefaultDisplay();
        mWidth = display.getWidth(); // deprecated
        viewWidth = mWidth / 3;
//        onPopUpHelpClick();
    }

    private void settingNavigationdrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void adjustKeyoardSettings() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }

    private void onClickCalls() {
        canceCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true);  // default selection of radio button ist one
                contactInfo.setVisibility(View.GONE);
                cardProfileInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                tvcontactus.setVisibility(View.GONE);

                //setting  default values of email and updatelocation card
                clearcardstodefault();
            }
        });

        cancelArchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true); // default selection of radio button ist one
                cardProfileInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                contactInfo.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                tvcontactus.setVisibility(View.GONE);

                clearcardstodefault();
            }
        });

        cancelupdatefrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true); // default selection of radio button ist one
                cardProfileInfo.setVisibility(View.GONE);
                contactInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                tvcontactus.setVisibility(View.GONE);

                clearcardstodefault();
            }
        });

        ivAdminCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall("03041116663");
            }
        });
        ivpophelpcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });


        ivAdminMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS("03000801546");
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etContact.getText().length() == 0 || etUsername.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter your information", Toast.LENGTH_SHORT).show();
                } else {
                    ivThumbs.setVisibility(View.VISIBLE);

                    Progressloader.loader(MapsParent.this);

                    mVolleyService.get_sendemailDataVolley("memberInquiry", Become_memberparams(etUsername.getText().toString(), etContact.getText().toString(), EMAILTAG));

                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dataHelper.arrayArchitecht != null) {
                            ivThumbs.setVisibility(View.GONE);
                        }
                    }
                }, 500);
            }
        });

        emailView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.VISIBLE);
                        contactInfo.setVisibility(View.GONE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.GONE);
                    }
                }
            }
        });

        updateloc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.GONE);
                        contactInfo.setVisibility(View.GONE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        archProfileView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.GONE);
                        contactInfo.setVisibility(View.VISIBLE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.GONE);
                    }
                }
            }
        });

        btnRateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToArchticechClass();
            }
        });

        buttonSendVendorloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etVendorName.getText().length() == 0 || etVendorContact.getText().length() == 0 || etvendorLocation.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter complete information", Toast.LENGTH_SHORT).show();
                } else {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.updateUserLocation("GetCall", "https://www.banjaiga.com/web_services/index.php/updateLocation/"
                            + tvHeading.getText().toString() + "/" + Usertype + "/" + etVendorName.getText().toString() + "/"
                            + etVendorContact.getText().toString() + "/" + vendorLatitude + "/" + vendorLongitude
                    );
                }

            }
        });

        etvendorLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showplacefragment(PLACE_AUTOCOMPLETE_REQUEST_CODE1);
            }
        });

        autoCompleteTextViewLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearSearchview();
                showplacefragment(PLACE_AUTOCOMPLETE_REQUEST_CODE);

            }


        });


 autoCompleteTextViewLoc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    return true;
                }
                return false;
            }
        });




autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "search&search=" + autoCompleteTextView.getText().toString() + "&lat=" + "" + "&long=" + "");
                   ClearSearchview();
                    return true;
                }
                return false;
            }
        });



        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearSearchview();

                autoCompleteTextView.setFocusableInTouchMode(true);
                autoCompleteTextView.requestFocus();

            }
        });

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                Progressloader.loader(MapsParent.this);
                String query = autoCompleteTextView.getText().toString();

                searchparams.put("term", query);
                searchparams.put("lat", String.valueOf(latitude));
                searchparams.put("long", String.valueOf(longitude));
                mVolleyService.getDataVolley("search", searchparams);
                keyboardDown(arg1);
            }

        });

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {
                    adapters_navicons.clearselection();
                    ImgTag = "";
                    Progressloader.loader(MapsParent.this);
                    searchparams.clear();
                    searchparams.put("lat", String.valueOf(latitude));
                    searchparams.put("long", String.valueOf(longitude));
                    mVolleyService.getDataVolley("search", searchparams);
                    keyboardDown(autoCompleteTextView);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void keyboardDown(View v) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

    }

    private void clearcardstodefault() {
        ivCardHome.setImageResource(R.drawable.projects_round);
        ivCardInterior.setImageResource(R.drawable.vendor_round);
        ivCardArch.setImageResource(R.drawable.man_greyround);
        ivCardCal.setImageResource(R.drawable.calculator_round);
        etContact.setText("");
        etUsername.setText("");
        etVendorContact.setText("");
        etVendorName.setText("");
        EMAILTAG = "";

    }

    private void goToArchticechClass() {
        startActivity(new Intent(MapsParent.this, ArchitechtActivity.class));
    }


    public void showplacefragment(int requestcode) {
        try {
            //AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("PK").setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)//.setFilter(typeFilter)
                    .build(MapsParent.this);
            startActivityForResult(intent, requestcode);
        } catch (GooglePlayServicesRepairableException e) {
            isGooglePlayServicesAvailable(MapsParent.this);
        } catch (GooglePlayServicesNotAvailableException e) {
            isGooglePlayServicesAvailable(MapsParent.this);
        }

    }


    public boolean emailValidator(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean phonevalidator(String phone) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^((\\+92)|(0092))-{0,1}\\d{3}-{0,1}\\d{7}$|^\\d{11}$|^\\d{4}-\\d{7}$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    private void showRatingMenu() {
        final Dialog dialog = new Dialog(MapsParent.this);
        dialog.setContentView(R.layout.rating_menu);
        dialog.setTitle("Banjaiga");
        ScaleRatingBar ratingBasedService = (ScaleRatingBar) dialog.findViewById(R.id.rating_based_service);
        ScaleRatingBar ratingBasedCost = (ScaleRatingBar) dialog.findViewById(R.id.rating_based_cost);
        ScaleRatingBar ratingBasedQuality = (ScaleRatingBar) dialog.findViewById(R.id.rating_based_quality);
        dialog.show();


    }

    private void initalizeControls() {

        recyclerView = findViewById(R.id.rv);

        ivVerifiedBadge = findViewById(R.id.iv_verified);
        ivMedalBadge = findViewById(R.id.badge_star);
        ivMobileBadge = findViewById(R.id.tv_phone_verified);
        ivEmailBadge = findViewById(R.id.email_verified_badge);
        ivpophelpcancel = findViewById(R.id.cancel_image_popup);

        llNavMenu = findViewById(R.id.top_navigation_menu);
        llArchitechts = findViewById(R.id.architechts);
        llKitchen = findViewById(R.id.kitchens);
        llFurniture = findViewById(R.id.furniture);
        llBathrooms = findViewById(R.id.bathrooms);
        llTiles = findViewById(R.id.tiles);
        llProjects = findViewById(R.id.projects);
        llLight = findViewById(R.id.light);
        llInterior = findViewById(R.id.interior);

        popupHelp = findViewById(R.id.popup_help);
        contactInfo = findViewById(R.id.card_contact_form);
        btnRateMe = findViewById(R.id.btn_rate_me);
        cardProfileInfo = findViewById(R.id.rlLayout);
        cardEmailCall = findViewById(R.id.card_email_callform);
        cardupdateloc = findViewById(R.id.card_update_location);
        title = findViewById(R.id.tv_title);
        canceCard = findViewById(R.id.cancel_image);
        cancelArchView = findViewById(R.id.arch_view_cancel);
        cancelupdatefrom = findViewById(R.id.cancel_updateform);
        buttonSend = findViewById(R.id.btnSend);
        buttonsearch = findViewById(R.id.btn_search);
        buttonSendVendorloc = findViewById(R.id.btn_updateloc);
        ivThumbs = findViewById(R.id.iv_thumbs);
        ivCall = findViewById(R.id.img_call);
        tvHeading = findViewById(R.id.tvHeading);
        tvcontactus = findViewById(R.id.txt_contactus);
        tvOptionDesc = findViewById(R.id.tv_option_desc);
        tvReviews = findViewById(R.id.tv_reviews);
        tvAddressName = findViewById(R.id.tvaddress_name);
        ivMsg = findViewById(R.id.img_msg);
        circleImageView = findViewById(R.id.img_logo);
        ivNearBy = findViewById(R.id.img_near_by);
        ivCardHome = findViewById(R.id.img_card_home);
        ivCardInterior = findViewById(R.id.img_card_interior);
        ivCardArch = findViewById(R.id.img_card_arch);
        ivCardCal = findViewById(R.id.img_card_cal);
        radioGroup = findViewById(R.id.radio_btngroup);
        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        Ratingbasedprofile = findViewById(R.id.baseratingbar_main);

        ivAdminCall = findViewById(R.id.iv_admin_call);
        ivAdminMessage = findViewById(R.id.iv_admin_message);
        emailView = findViewById(R.id.emailViewRb);
        updateloc = findViewById(R.id.thirdView);
        etUsername = findViewById(R.id.et_name);
        etContact = findViewById(R.id.et_contact);
        etVendorName = findViewById(R.id.et_vendorname);
        etVendorContact = findViewById(R.id.et_vendorcontact);
        etvendorLocation = findViewById(R.id.et_vendor_loc);
        etvendorLocation.setFocusable(false);
        archProfileView = findViewById(R.id.profileView);
        footerLayout = findViewById(R.id.footer);
        ivNext = findViewById(R.id.img_next);
        ivBack = findViewById(R.id.img_back);

        autoCompleteTextViewLoc = findViewById(R.id.search_by_text);
        autoCompleteTextView = findViewById(R.id.search_by_tags);

        ivCall.setOnClickListener(this);
        ivMsg.setOnClickListener(this);
        buttonsearch.setOnClickListener(this);

        ivCardHome.setOnClickListener(this);
        ivCardInterior.setOnClickListener(this);
        ivCardArch.setOnClickListener(this);
        ivCardCal.setOnClickListener(this);

        ivNearBy.setOnClickListener(this);
        ivNext.setOnClickListener(this); // top muenu next button click event
        ivBack.setOnClickListener(this); // top muenu next button click event

//        if (!prefs.getBoolean("isFirstTime", false)) {
//            llNavMenu.setVisibility(View.GONE);
//            popupHelp.setVisibility(View.VISIBLE);
//        } else {
//            llNavMenu.setVisibility(View.VISIBLE);
//            popupHelp.setVisibility(View.GONE);
//        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
//        mMap.setMyLocationEnabled(true);
        mClusterManager = new ClusterManager<>(this, mMap);
        mMap.setOnCameraChangeListener(mClusterManager);
        if (dataHelper != null && dataHelper.arrayArchitecht != null && dataHelper.arrayArchitecht.size() > 0) {
            isFoundArch = true;
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                AllArchitectsDataModel arcData = dataHelper.arrayArchitecht.get(i);
                Double lat = 0.0;
                Double longitude = 0.0;
                autocompletearray.add(dataHelper.arrayArchitecht.get(i).getUserName());

                if (!arcData.getLatitude().matches(""))
                    lat = Double.valueOf(arcData.getLatitude());
                if (!arcData.getLongitude().matches(""))
                    longitude = Double.valueOf(arcData.getLongitude());
                LOCATIONS_POINTS = new LatLng(lat, longitude);
                mClusterManager.addItem(new StringClusterItem("Marker #" + (i + 1), LOCATIONS_POINTS, i,arcData.getMembershipType()));
            }
        } else {
            isFoundArch = false;
        }
        mClusterManager.cluster();


        // Setting autocompelte keyword suggestions

        Autocompletetextdata();


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                ClearSearchview();

            }
        });


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (isFoundArch)
                    //    mMap.animateCamera(CameraUpdateFactory.newLatLng(LOCATIONS_POINTS));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATIONS_POINTS, 11f));
            }
        });


        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterClickListener(this);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getPosition() != null) {
                    AllArchitectsDataModel userData = getUserData(marker.getPosition());
                    if (userData != null) {
                        showMarkerDetail(userData);
                    }
                }
                if (emailView.isChecked()) {
                    contactInfo.setVisibility(View.GONE);
                    cardProfileInfo.setVisibility(View.VISIBLE);

                } else if (archProfileView.isChecked()) {
                    cardProfileInfo.setVisibility(View.GONE);
                    contactInfo.setVisibility(View.VISIBLE);


                }
                if (footerLayout.getVisibility() == View.GONE || footerLayout.getVisibility() == View.VISIBLE) {
                    radioGroup.setVisibility(View.VISIBLE);
                    tvcontactus.setVisibility(View.VISIBLE);

                }

                return false;
            }
        });
    }

    private void Autocompletetextdata() {

        AdapterSearchNames namesAdapter = new AdapterSearchNames(
                MapsParent.this,
                R.layout.activity_maps_layout,
                R.id.lbl_name,
                autocompletearray
        );
        //set adapter into listStudent
        autoCompleteTextView.setThreshold(2);
        autoCompleteTextView.setAdapter(namesAdapter);


    }


    private void showMarkerDetail(AllArchitectsDataModel userData) {
        MyHelper.currentUserData = userData;
        title.setText("Get banjaiga services");
        phoneNum = userData.getPhoneNumberONE();
        bitmapString = userData.getLogo();
        tvHeading.setText(userData.getUserName());
        tvAddressName.setText(userData.getArchaddress() + "\n" + userData.getViews() + " Views");
        Usertype = userData.getFlags();
        if (userData.getViews().matches("0") || userData.getViews().matches("")) {
            tvReviews.setVisibility(View.INVISIBLE);
        } else {
            tvReviews.setText(userData.getViews() + " Views");
        }
        if (userData.getIsEmail().matches("1"))
            ivEmailBadge.setVisibility(View.VISIBLE);
        else
            ivEmailBadge.setVisibility(View.GONE);

        if (userData.getIsMobile().matches("1"))
            ivMobileBadge.setVisibility(View.VISIBLE);
        else
            ivMobileBadge.setVisibility(View.GONE);

        if (userData.getIsVerified().matches("1")) {
            ivVerifiedBadge.setVisibility(View.VISIBLE);
            Picasso.with(this).load(userData.getLogo()).error(R.drawable.error_logo).into(circleImageView);
        } else {
            ivVerifiedBadge.setVisibility(View.GONE);
            Picasso.with(this).load(R.drawable.unverified).error(R.drawable.error_logo).into(circleImageView);

        }
        if (userData.getMembershipType().matches("gold")) {
            ivMedalBadge.setVisibility(View.VISIBLE);
            ivMedalBadge.setImageResource(R.drawable.badgestargolden);
        } else if (userData.getMembershipType().matches("silver")) {
            ivMedalBadge.setVisibility(View.VISIBLE);
            ivMedalBadge.setImageResource(R.drawable.badgestarsilver);
        } else {
            ivMedalBadge.setVisibility(View.GONE);
        }
        if (userData.getFlags().equalsIgnoreCase("architect")) {
            Ratingbasedprofile.setRating(Float.parseFloat(userData.getAvgServices()));

        } else {

            float total = 0;
            if (userData.getAvgQuality() != null)
                total += Float.parseFloat(userData.getAvgQuality());
            else if (userData.getAvgPrice() != null)
                total += Float.parseFloat(userData.getAvgPrice());
            else if (userData.getAvgServices() != null)
                total += Float.parseFloat(userData.getAvgServices());

            float average = total / 3;

            Ratingbasedprofile.setRating(average);

        }

        Log.d("Logo", userData.getLogo());
    }

    private AllArchitectsDataModel getUserData(LatLng position) {
        AllArchitectsDataModel allArchitectsDataModel = null;
        if (dataHelper.arrayArchitecht != null && dataHelper.arrayArchitecht.size() > 0) {
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                String latitude = String.valueOf(position.latitude);
                String longitude = String.valueOf(position.longitude);
                String curentLat = "";
                String curentLong = "";
                if (!dataHelper.arrayArchitecht.get(i).getLatitude().matches("")) {
                    curentLat = dataHelper.arrayArchitecht.get(i).getLatitude();
                }
                if (!dataHelper.arrayArchitecht.get(i).getLongitude().matches("")) {
                    curentLong = dataHelper.arrayArchitecht.get(i).getLongitude();
                }
                if (curentLat.matches(latitude) && curentLong.matches(longitude)) {
                    allArchitectsDataModel = dataHelper.arrayArchitecht.get(i);
                    return allArchitectsDataModel;
                }
            }
        }
        return allArchitectsDataModel;
    }


    private void populaterecyclerviewdata() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        for (int i = 0; i < myImageList.length; i++) {
            Navscrollitemes = new NavScrollitems();
            Navscrollitemes.setDrawable(myImageList[i]);
            Navscrollitemes.setImagetag(imagestag[i]);
            Navscrollitemes.setChangedrawable(myImageListcolor[i]);
            arraynavicons.add(Navscrollitemes);
        }

        adapters_navicons = new Adapters_Nav(MapsParent.this, arraynavicons);
        recyclerView.setAdapter(adapters_navicons);



        adapters_navicons.SetOnItemClickListener(new Adapters_Nav.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                // adapters_navicons.changeimage(position);
                ImgTag = arraynavicons.get(position).getImagetag();
                ClearSearchview();
                keyboardDown(view);
                autoCompleteTextView.setText(ImgTag);
                if (ImgTag.equalsIgnoreCase("Architects_lat_lang")) {

                    // Clearing searchview hint and keyboard down on every top menu click
//                    Progressloader.loader(MapsParent.this);
                    //    mVolleyService.getDataVolley("GETCALL", "archlocation");

//                    mVolleyService.getDataVolley("get_architects", searchparams_architects());
                    startActivity(new Intent(MapsParent.this,ArchitectsActivity.class));

                }
                if (ImgTag.equalsIgnoreCase("interior")) {
                    Progressloader.loader(MapsParent.this);
                    //mVolleyService.getDataVolley("GETCALL", "category&id=108&lat=" + latitude + "&long=" + longitude);
                    //  mVolleyService.getDataVolley("search", searchparamsmethod("108"));
                    mVolleyService.getDataVolley("search", searchparamsmethod("interior"));

                }
                if (ImgTag.equalsIgnoreCase("tiles")) {
                    Progressloader.loader(MapsParent.this);
                    // mVolleyService.getDataVolley("GETCALL", "tiles");
                    //  mVolleyService.getDataVolley("search", "tiles&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("tiles"));


                }
                if (ImgTag.equalsIgnoreCase("kitchens")) {
                    Progressloader.loader(MapsParent.this);
                    //mVolleyService.getDataVolley("search", "category&id=5&lat=" + latitude + "&long=" + longitude);

                    mVolleyService.getDataVolley("search", searchparamsmethod("kitchens"));

                }
                if (ImgTag.equalsIgnoreCase("furniture")) {
                    Progressloader.loader(MapsParent.this);
                    //mVolleyService.getDataVolley("search", "category&id=15&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("furniture"));

                }
                if (ImgTag.equalsIgnoreCase("Bathrooms")) {
                    Progressloader.loader(MapsParent.this);
                    // mVolleyService.getDataVolley("search", "category&id=6&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("bathrooms"));

                }

                if (ImgTag.equalsIgnoreCase("lights")) {
                    Progressloader.loader(MapsParent.this);

                    //   mVolleyService.getDataVolley("GETCALL", "category&id=148&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("lights"));

                }
                if (ImgTag.equalsIgnoreCase("More")) {
//                    Progressloader.loader(MapsParent.this);

                    //   mVolleyService.getDataVolley("GETCALL", "category&id=148&lat=" + latitude + "&long=" + longitude);
//                    mVolleyService.getDataVolley("search", searchparamsmethod("lights"));
                    startActivity(new Intent(MapsParent.this,CategoryActivity.class));

                }

            }
        });
    }

    private HashMap searchparams_architects() {
        searchparams.clear();
        searchparams.put("lat", String.valueOf(latitude));
        searchparams.put("long", String.valueOf(longitude));
        return searchparams;
    }

    private HashMap searchparamsmethod(String id) {
        searchparams.clear();
        searchparams.put("category", id);
        searchparams.put("lat", String.valueOf(latitude));
        searchparams.put("long", String.valueOf(longitude));
        return searchparams;
    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    private void ClearSearchview() {

        // above function is searchview clear hint and for hiding keyboard

        //  autoCompleteTextView.setHint("Enter keyword");
        autoCompleteTextViewLoc.setText("");
        autoCompleteTextViewLoc.clearFocus();
        autoCompleteTextView.clearFocus();
        autoCompleteTextViewLoc.setFocusable(false);
        emailView.setChecked(true);
        contactInfo.setVisibility(View.GONE);
        cardupdateloc.setVisibility(View.GONE);
        cardProfileInfo.setVisibility(View.GONE);


        // default set value email sending card and  updatelocation card
//        clearcardstodefault();


    }

    private void setupMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setRetainInstance(true);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()
to get clicked view id*
) {

            case R.id.img_near_by:
                categoriesSearchCall(ImgTag);
                break;

            case R.id.img_call:
                AlertDialog();
makeCall(phoneNum);

                break;

            case R.id.img_msg:
                if (phonevalidator(phoneNum)) {
                    sendSMS(phoneNum);
                } else {
                    Toast.makeText(MapsParent.this, "Not a valid number to text", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_card_home:
                Clearfields(view);
                EMAILTAG = "client";
                tvOptionDesc.setText("I want to build a house");
                cardStateChanger(EMAILTAG);
                break;
            case R.id.img_card_interior:
                Clearfields(view);
                EMAILTAG = "vendor";
                tvOptionDesc.setText("I am a vendor");
                cardStateChanger(EMAILTAG);
                break;

            case R.id.img_card_arch:
                Clearfields(view);
                EMAILTAG = "architect";
                tvOptionDesc.setText("I am an architect");
                cardStateChanger(EMAILTAG);
                break;

            case R.id.img_card_cal:
                Clearfields(view);
                EMAILTAG = "quote";
                tvOptionDesc.setText("I want a free quote");
                cardStateChanger(EMAILTAG);
                break;
            case R.id.img_next:
                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                recyclerView.getLayoutManager().scrollToPosition(llm.findLastVisibleItemPosition() + 1);
                break;

            case R.id.img_back:
                LinearLayoutManager llmb = (LinearLayoutManager) recyclerView.getLayoutManager();
                recyclerView.getLayoutManager().scrollToPosition(llmb.findFirstVisibleItemPosition() - 1);
                break;


            case R.id.btn_search:

                if (autoCompleteTextView.getText().length() == 0 && autoCompleteTextViewLoc.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter what and where you want to search", Toast.LENGTH_SHORT).show();
                } else if (autoCompleteTextViewLoc.getText().length() == 0) {
                    Progressloader.loader(MapsParent.this);
                    String text = autoCompleteTextView.getText().toString();

                    searchparams.clear();
                    searchparams.put("term", text);
                    mVolleyService.getDataVolley("search", searchparams);
                    ClearSearchview();
                    keyboardDown(view);
                } else {
                    Progressloader.loader(MapsParent.this);
                    String text = autoCompleteTextView.getText().toString();

                    searchparams.clear();
                    searchparams.put("term", text);
                    searchparams.put("lat", String.valueOf(latitude));
                    searchparams.put("long", String.valueOf(longitude));
                    mVolleyService.getDataVolley("search", searchparams);
                    ClearSearchview();
                    keyboardDown(view);
                }

                break;
            default:
                break;
        }
    }

    private void Clearfields(View view) {


        autoCompleteTextViewLoc.setText("");
        autoCompleteTextViewLoc.setFocusable(false);


    }

    public void onPopUpHelpClick() {
        llArchitechts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("Architects_lat_lang");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llInterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("interior");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("lights");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("No");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llTiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("tiles");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llBathrooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("bathrooms");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llFurniture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("furniture");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

        llKitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoriesSearchCall("kitchens");
                editor.putBoolean("isFirstTime", true).apply();
                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });

    }


    public void categoriesSearchCall(String ImgTag) {
        if (ImgTag.equalsIgnoreCase("Architects_lat_lang")) {
            ClearSearchview(); // Clearing searchview hint and keyboard down on nearby button click
            Progressloader.loader(MapsParent.this);
            mVolleyService.getDataVolley("get_architects", searchparams_architects());
        }
        if (ImgTag.equalsIgnoreCase("interior")) {
            ClearSearchview();
            // mVolleyService.getDataVolley("GetCall", "category&id=108&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod("interior"));
        }

        if (ImgTag.equalsIgnoreCase("bathrooms")) {
            ClearSearchview();
            Progressloader.loader(MapsParent.this);
            //   mVolleyService.getDataVolley("search", "category&id=6&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod("bathrooms"));

        }
        if (ImgTag.equalsIgnoreCase("kitchens")) {
            ClearSearchview();
            Progressloader.loader(MapsParent.this);
            //  mVolleyService.getDataVolley("search", "category&id=5&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod("kitchens"));


        }

        if (ImgTag.equalsIgnoreCase("furniture")) {
            ClearSearchview();
            Progressloader.loader(MapsParent.this);
            //  mVolleyService.getDataVolley("search", "category&id=15&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod("furniture"));


        }

        if (ImgTag.equalsIgnoreCase("lights")) {
            ClearSearchview();
            Progressloader.loader(MapsParent.this);
            //  mVolleyService.getDataVolley("search", "category&id=148&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod("lights"));

        }
        if (ImgTag.equalsIgnoreCase("tiles")) {
            ClearSearchview();
            Progressloader.loader(MapsParent.this);
            // mVolleyService.getDataVolley("search", "tiles&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod("tiles"));

        }

        if (ImgTag.isEmpty()) {
            ClearSearchview();
            Progressloader.loader(MapsParent.this);
            //   mVolleyService.getDataVolley("GETCALL", "search&lat=" + latitude + "&long=" + longitude);
            mVolleyService.getDataVolley("search", searchparamsmethod(""));

        }
    }

    private void cardStateChanger(String emaiTag) {
        if (emaiTag.equalsIgnoreCase("client")) {
            ivCardHome.setImageResource(R.drawable.house_selected);
            ivCardInterior.setImageResource(R.drawable.vendor_round);
            ivCardArch.setImageResource(R.drawable.man_greyround);
            ivCardCal.setImageResource(R.drawable.calculator_round);
        }
        if (emaiTag.equalsIgnoreCase("vendor")) {
            ivCardHome.setImageResource(R.drawable.projects_round);
            ivCardInterior.setImageResource(R.drawable.vendor_selected);
            ivCardArch.setImageResource(R.drawable.man_greyround);
            ivCardCal.setImageResource(R.drawable.calculator_round);

        }
        if (emaiTag.equalsIgnoreCase("architect")) {
            ivCardHome.setImageResource(R.drawable.projects_round);
            ivCardInterior.setImageResource(R.drawable.vendor_round);
            ivCardArch.setImageResource(R.drawable.architect_selected);
            ivCardCal.setImageResource(R.drawable.calculator_round);

        }
        if (emaiTag.equalsIgnoreCase("quote")) {
            ivCardHome.setImageResource(R.drawable.projects_round);
            ivCardInterior.setImageResource(R.drawable.vendor_round);
            ivCardArch.setImageResource(R.drawable.man_greyround);
            ivCardCal.setImageResource(R.drawable.quote_selected);
        }
    }


    private void cardStateChangerdialog(String emaiTag) {
        if (emaiTag.equalsIgnoreCase("client")) {
            ivCardHomedialog.setImageResource(R.drawable.house_selected);
            ivCardInteriordialog.setImageResource(R.drawable.vendor_round);
            ivCardArchdialog.setImageResource(R.drawable.man_greyround);
            ivCardCaldialog.setImageResource(R.drawable.calculator_round);
        }
        if (emaiTag.equalsIgnoreCase("vendor")) {
            ivCardHomedialog.setImageResource(R.drawable.projects_round);
            ivCardInteriordialog.setImageResource(R.drawable.vendor_selected);
            ivCardArchdialog.setImageResource(R.drawable.man_greyround);
            ivCardCaldialog.setImageResource(R.drawable.calculator_round);

        }
        if (emaiTag.equalsIgnoreCase("architect")) {
            ivCardHomedialog.setImageResource(R.drawable.projects_round);
            ivCardInteriordialog.setImageResource(R.drawable.vendor_round);
            ivCardArchdialog.setImageResource(R.drawable.architect_selected);
            ivCardCaldialog.setImageResource(R.drawable.calculator_round);

        }
        if (emaiTag.equalsIgnoreCase("quote")) {
            ivCardHomedialog.setImageResource(R.drawable.projects_round);
            ivCardInteriordialog.setImageResource(R.drawable.vendor_round);
            ivCardArchdialog.setImageResource(R.drawable.man_greyround);
            ivCardCaldialog.setImageResource(R.drawable.quote_selected);
        }
    }

    @Override
    public boolean onClusterClick(Cluster cluster) {
        Toast.makeText(MapsParent.this, "jhjh", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onClusterItemClick(ClusterItem clusterItem) {
        Toast.makeText(MapsParent.this, "jkkjkjkjk", Toast.LENGTH_SHORT).show();

        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.register_nav:
                show_dialogVendorRegister();
                break;
            case R.id.register_vendor:
                startActivity(new Intent(MapsParent.this, VendorRegistration.class));
                break;
            case R.id.help_nav:
                DataHelper.IntroTag = "No";
                Intent helpintent = new Intent(MapsParent.this, HelpActivity.class);
                // helpintent.putExtra("TAG", "yes");
                startActivity(helpintent);


                break;


        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static class StringClusterItem implements ClusterItem {
        public final String title;
        public final String type;
        public final LatLng latLng;
        public final int tag;

        public StringClusterItem(String title, LatLng latLng, int tag,String type) {
            this.title = title;
            this.latLng = latLng;
            this.tag = tag;
            this.type = type;
        }

        @Override
        public LatLng getPosition() {
            return latLng;
        }
    }


    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {

                Progressloader.hideloader();
                if (requestType.matches("requestcall")) {
                    try {
                        Toast.makeText(getApplicationContext(), response.getString("Message"), Toast.LENGTH_SHORT).show();
                        alertDialogRequestCall.cancel();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void notifySuccess(String requestType, JSONArray response) {
                Progressloader.hideloader();
                onResponseFinished(response);

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySucessEmail(String requestType, JSONObject response) {
                Progressloader.hideloader();
                try {

                    Toast.makeText(MapsParent.this, response.getString("Message"), Toast.LENGTH_SHORT).show();
                    etContact.setText("");
                    etUsername.setText("");
                    if (etUsernamedialog != null && etContactdialog != null) {
                        etContactdialog.setText("");
                        etUsernamedialog.setText("");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);

            }

            @Override
            public void notifyRatingSuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifyLocationsucess(String requestType, JSONObject response) {
                try {

                    Progressloader.hideloader();
                    Toast.makeText(MapsParent.this, response.getString("Message"), Toast.LENGTH_SHORT).show();
                    // radioGroup.setVisibility(View.GONE);
                    etVendorName.setText("");
                    etVendorContact.setText("");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyClientReviews(String requestType, JSONObject response) {

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

                Progressloader.hideloader();

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + "That didn't work!");

                if (error instanceof NetworkError) {

                    Toast.makeText(MapsParent.this,
                            "Please check your internet connection and try again",
                            Toast.LENGTH_LONG).show();

                    //TOimg_msgDO
                } else if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(MapsParent.this,
                            "Something went wrong please try again",
                            Toast.LENGTH_LONG).show();
                }


            }


        };
    }

    private void onResponseFinished(JSONArray response) {
        try {
            if (dataHelper.arrayArchitecht == null) {
                // mMap.clear();
                dataHelper.arrayArchitecht = new ArrayList<>();
            }
            if (response.length() > 0) {
                dataHelper.arrayArchitecht.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);

                    AllArchitectsDataModel dataModel = new AllArchitectsDataModel(this, jsonObject);
                    dataHelper.arrayArchitecht.add(dataModel);
                }
                plotAddressData();
            } else {
                Toast.makeText(this, "No record found", Toast.LENGTH_SHORT).show();
                mMap.clear();
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    void plotAddressData() {

        //  mClusterManager = new ClusterManager<>(this, mMap);

        mClusterManager.clearItems();
        mMap.setOnCameraChangeListener(mClusterManager);
        if (dataHelper != null && dataHelper.arrayArchitecht.size() > 0) {

            isFoundArch = true;
            mMap.clear();
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                AllArchitectsDataModel arcData = dataHelper.arrayArchitecht.get(i);

                Double lat = 0.0;
                Double longitude = 0.0;
                if (!arcData.getLatitude().matches(""))
                    lat = Double.valueOf(arcData.getLatitude());
                if (!arcData.getLongitude().matches(""))
                    longitude = Double.valueOf(arcData.getLongitude());
                LOCATIONS_POINTS = new LatLng(lat, longitude);
                mClusterManager.addItem(new StringClusterItem("Marker #" + (i + 1), LOCATIONS_POINTS, i,arcData.getMembershipType()));
            }
        } else {
            isFoundArch = false;
        }
        mClusterManager.cluster();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (isFoundArch)

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATIONS_POINTS, 10f));


            }
        });

        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterClickListener(this);

        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);


    }


    void changeImageColor(String tag) {
        if (tag.equalsIgnoreCase("home")) {
            ivHome.setImageResource(R.drawable.tiles_yellow);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("interior")) {
            ivCurtain.setImageResource(R.drawable.interio_yellow);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("arch")) {
            ivManGrey.setImageResource(R.drawable.man_yello);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("bath")) {
            ivBath.setImageResource(R.drawable.bath_yellow);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("kitchen")) {
            ivKitchen.setImageResource(R.drawable.kitchen_yellow);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }


        if (tag.equalsIgnoreCase("furniture")) {
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivFurniture.setImageResource(R.drawable.furniture_yellow);
        }
    }

    void changelatlongtoaddress(double latitude, double longitude, String ettoselect) {
        List<Address> addresses;
        Geocoder geoCoder = new Geocoder(MapsParent.this);
        try {

            addresses = geoCoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                etvendorLocation.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
                if (ettoselect.equalsIgnoreCase("etvendor")) {
                    etvendorLocation.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
                } else {
                    etvendorLocationdialog.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
                }
            }
        } catch (IOException e) { // TODO Auto-generated catch block
            e.printStackTrace();
        }

 pos = new LatLng(latitude, longtitude);
        googleMap.addMarker(new MarkerOptions().icon(
                BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(pos));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

    }

    public void sendSMS(String phone_num) {
        // The number on which you want to send SMS
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone_num, null)));
    }

    private void makeCall(String phone_num) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                autoCompleteTextViewLoc.setText(String.valueOf(place.getName()));
                autoCompleteTextViewLoc.setSelection(autoCompleteTextViewLoc.getText().length());
 autoCompleteTextViewLoc.setFocusableInTouchMode(true);
                autoCompleteTextViewLoc.requestFocus();


                LatLng destination = place.getLatLng();
                searchLatitude = destination.latitude;
                searchLongitude = destination.longitude;
                Showhidekeyboard = "no";

            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                etvendorLocation.setText(place.getName());
                LatLng destination = place.getLatLng();
                vendorLatitude = destination.latitude;
                vendorLongitude = destination.longitude;

            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE2) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                etvendorLocationdialog.setText(place.getName());
                LatLng destination = place.getLatLng();
                vendorLatitude = destination.latitude;
                vendorLongitude = destination.longitude;

            }
        }
    }

    @Override
    public void onResume() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        super.onResume();
    }

    @Override
    public void onStart() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onStart();
    }


    public void show_dialogupdate_location() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.vendor_locationupdatedialog, null);
        dialogBuilder.setView(dialogView);

        buttonSendVendorlocdialog = dialogView.findViewById(R.id.btn_updateloc);
        etVendorNamedialog = dialogView.findViewById(R.id.et_vendorname);
        etVendorContactdialog = dialogView.findViewById(R.id.et_vendorcontact);
        etvendorLocationdialog = dialogView.findViewById(R.id.et_vendor_loc);
        cancelupdatefromdialog = dialogView.findViewById(R.id.cancel_updateform);
        etvendorLocationdialog.setFocusable(false);
//        ClickcallbacksDialog();
        changelatlongtoaddress(latitude, longitude, "etvendordialog");


        alertDialog = dialogBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;


//        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }


    public void show_dialogVendorRegister() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.email_contact_layoutdialog, null);
        dialogBuilder.setView(dialogView);

        tvOptionDescdialog = dialogView.findViewById(R.id.tv_option_desc);
        buttonSenddialog = dialogView.findViewById(R.id.btnSend);
        etUsernamedialog = dialogView.findViewById(R.id.et_name);
        etContactdialog = dialogView.findViewById(R.id.et_contact);
        ivCardHomedialog = dialogView.findViewById(R.id.img_card_home);
        ivCardInteriordialog = dialogView.findViewById(R.id.img_card_interior);
        ivCardArchdialog = dialogView.findViewById(R.id.img_card_arch);
        ivCardCaldialog = dialogView.findViewById(R.id.img_card_cal);
 ivCardHomedialog.setOnClickListener(this);
        ivCardInteriordialog.setOnClickListener(this);
        ivCardArchdialog.setOnClickListener(this);
        ivCardCaldialog.setOnClickListener(this);


        cancelcardemaildialog = dialogView.findViewById(R.id.cancel_imagedialog);


//        ClickcallbacksemailDialog();


        alertDialog = dialogBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;


        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    private void ClickcallbacksemailDialog() {
        buttonSenddialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etContactdialog.getText().length() == 0 || etUsernamedialog.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter your information", Toast.LENGTH_SHORT).show();
                } else {
                    ivThumbs.setVisibility(View.VISIBLE);

                    Progressloader.loader(MapsParent.this);
                    mVolleyService.get_sendemailDataVolley("memberInquiry", Become_memberparams(etUsernamedialog.getText().toString(), etContactdialog.getText().toString(), EMAILTAG));

                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dataHelper.arrayArchitecht != null) {
                            ivThumbs.setVisibility(View.GONE);
                        }
                    }
                }, 500);
            }
        });
        cancelcardemaildialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        ivCardArchdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EMAILTAG = "architect";
                cardStateChangerdialog(EMAILTAG);
                tvOptionDescdialog.setText("I am an architect");
            }
        });

        ivCardInteriordialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EMAILTAG = "vendor";
                cardStateChangerdialog(EMAILTAG);
                tvOptionDescdialog.setText("I am a vendor");

            }
        });

        ivCardHomedialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EMAILTAG = "client";
                cardStateChangerdialog(EMAILTAG);
                tvOptionDescdialog.setText("I want to build a house");


            }
        });
        ivCardCaldialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EMAILTAG = "quote";
                cardStateChangerdialog(EMAILTAG);
                tvOptionDescdialog.setText("I want a free quote");

            }
        });


    }

    private HashMap Become_memberparams(String name, String contact, String type) {
        searchparams.clear();
        searchparams.put("type", type);
        searchparams.put("name", name);
        searchparams.put("contact", contact);
        return searchparams;


    }

    private void ClickcallbacksDialog() {
        etvendorLocationdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showplacefragment(PLACE_AUTOCOMPLETE_REQUEST_CODE2);
            }


        });

        buttonSendVendorlocdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etVendorNamedialog.getText().length() == 0 || etVendorContactdialog.getText().length() == 0 || etvendorLocationdialog.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter complete information", Toast.LENGTH_SHORT).show();
                } else {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.updateUserLocation("GetCall", "https://www.banjaiga.com/web_services/index.php/updateLocation/"
                            + tvHeading.getText().toString() + "/" + Usertype + "/" + etVendorNamedialog.getText().toString() + "/"
                            + etVendorContactdialog.getText().toString() + "/" + vendorLatitude + "/" + vendorLongitude
                    );
                }

            }

        });
        cancelupdatefromdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void AlertDialog() {

        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MapsParent.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.phone_contact_view, null);
        alertDialogBuilder.setView(dialogView);
        alertDialogBuilder.setCancelable(false);
        RelativeLayout crossLaout = dialogView.findViewById(R.id.upper_panel);
        final TextView phoneOne = dialogView.findViewById(R.id.phone_vend_one);
        final TextView phoneTwo = dialogView.findViewById(R.id.phone_vend_two);
        TextView phonneBanjaiga = dialogView.findViewById(R.id.phone_banjaiga);
        final EditText etContactInfo = dialogView.findViewById(R.id.call_back_info);
        Button btnCallMe = dialogView.findViewById(R.id.btnCallMe);
        if (MyHelper.currentUserData != null) {
            if (!MyHelper.currentUserData.getPhoneNumberONE().matches(MyHelper.currentUserData.getPhoneNumberTWO())) {
                phoneOne.setText("Phone 01 :" + " " + MyHelper.currentUserData.getPhoneNumberONE());
                phoneTwo.setText("Phone 02 :" + " " + MyHelper.currentUserData.getPhoneNumberTWO());
            } else {
                phoneOne.setText("Phone 01 :" + " " + MyHelper.currentUserData.getPhoneNumberONE());
                phoneTwo.setVisibility(View.GONE);
            }

            phonneBanjaiga.setText("03041116663");
        }

        alertDialogRequestCall = alertDialogBuilder.create();
        alertDialogRequestCall.show();
        btnCallMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogRequestCall.cancel();
            }
        });
        crossLaout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogRequestCall.cancel();
            }
        });
        phoneOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall(MyHelper.currentUserData.getPhoneNumberONE());
                alertDialogRequestCall.cancel();
            }
        });


        phonneBanjaiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall("03041116663");
                alertDialogRequestCall.cancel();
            }
        });
        phoneTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall(MyHelper.currentUserData.getPhoneNumberTWO());
                alertDialogRequestCall.cancel();
            }
        });

        btnCallMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Progressloader.loader(MapsParent.this);
                String number = etContactInfo.getText().toString();
                if (number.length() == 10 || number.length() == 11) {
                    requestCall.put("phone", number);
                    mVolleyService.requestACallToBanjaiga("requestcall", requestCall);
                } else {
                    Progressloader.hideloader();
                    Toast.makeText(getApplicationContext(), "Incoreect Phone Number , Enter a valid Phone Number", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
*/
