package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.design.hrh.banjaegaandroidapp.Adapters.ProjectGalleryAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.ReviewListAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.VendorNetworkAdapter;

import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Model.ProjectGalleryModel;
import com.design.hrh.banjaegaandroidapp.Model.ReviewModel;
import com.design.hrh.banjaegaandroidapp.Model.VendorModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjects_list;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjectsall_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Get_architects_data;


import com.design.hrh.banjaegaandroidapp.ResponseModels.GetuserReview_geterseter;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetuserReview_response;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiClient;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiInterface;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ArchitectProfileActivity extends AppCompatActivity implements ProjectGalleryAdapter.OnItemClickListener {
    Activity context=ArchitectProfileActivity.this;
    RecyclerView rvProjectGallery , rvVendorNetwork , rvReviews;
    VendorNetworkAdapter mVendorAdapter;
    ProjectGalleryAdapter mAdapter;
    ReviewListAdapter mReviewAdapter;
    ArrayList<ReviewModel> Reviews= new ArrayList<>();
    ArrayList<ProjectGalleryModel> Gallery = new ArrayList<>();
    ArrayList<VendorModel> Vendors=new ArrayList<>();

    ImageView imageView;
    Button call,messag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_architect_profile);
        initView();

    }

    private void initView() {

        call=(Button)findViewById(R.id.btn_call);
        messag=(Button)findViewById(R.id.btn_message);


        imageView=(ImageView)findViewById(R.id.img_banner);
        Glide.with(context).load(MyHelper.image_).into(imageView);

        TextView name=(TextView)findViewById(R.id.txt_project_name);
        TextView address=(TextView)findViewById(R.id.et_address);

        name.setText(MyHelper.name_);
        address.setText(MyHelper.address);

        CircleImageView circleImageView=(CircleImageView)findViewById(R.id.picture);
        Glide.with(context).load(MyHelper.image_).into(circleImageView);

        rvReviews=(RecyclerView)findViewById(R.id.rv_reviews);
        rvVendorNetwork=(RecyclerView)findViewById(R.id.rv_vendor_network);
        rvVendorNetwork.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvReviews.setLayoutManager(new LinearLayoutManager(context));
        rvProjectGallery=(RecyclerView)findViewById(R.id.rv_project_gallery);
        rvProjectGallery.setLayoutManager(new GridLayoutManager(context,2));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_pic));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_2));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_2));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_pic));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_pic));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_2));
        Vendors.add(new VendorModel("",R.drawable.profile_image_with_badge));
        Vendors.add(new VendorModel("",R.drawable.profile_image_with_badge));
        Vendors.add(new VendorModel("",R.drawable.profile_image_with_badge));
        Vendors.add(new VendorModel("",R.drawable.profile_image_with_badge));
        Vendors.add(new VendorModel("",R.drawable.profile_image_with_badge));

        Reviews.add(new ReviewModel("1","4","It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.","Ali khan",R.drawable.reviewer));
        Reviews.add(new ReviewModel("1","4","It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.","Ali khan",R.drawable.reviewer));
        Reviews.add(new ReviewModel("1","4","It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.","Ali khan",R.drawable.reviewer));
        Reviews.add(new ReviewModel("1","4","It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.","Ali khan",R.drawable.reviewer));
        Reviews.add(new ReviewModel("1","4","It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.","Ali khan",R.drawable.reviewer));
        Reviews.add(new ReviewModel("1","4","It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.","Ali khan",R.drawable.reviewer));




        mVendorAdapter=new VendorNetworkAdapter(context,Vendors);
        rvVendorNetwork.setAdapter(mVendorAdapter);

        Apicall();
        Rating();

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyHelper.makeCall(ArchitectProfileActivity.this,MyHelper.number);
            }
        });
        messag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyHelper.sendSMS(ArchitectProfileActivity.this,MyHelper.address);
            }
        });

        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
//getprojectresponse
    @Override
    public void onItemClick(View view, int position) {


    }


    // API call in
    List<GetProjects_list> arraylist_;
    private void Apicall() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetProjectsall_response> call = apiService.getprojectresponse("24966");
        call.enqueue(new Callback<GetProjectsall_response>() {
            @Override
            public void onResponse(Call<GetProjectsall_response> call, Response<GetProjectsall_response> response) {
                int statusCode = response.code();

                arraylist_ = response.body().getResult();


                mAdapter=new ProjectGalleryAdapter(context,arraylist_);
                mAdapter.SetOnItemClickListener(ArchitectProfileActivity.this);
                rvProjectGallery.setAdapter(mAdapter);
          }

            @Override
            public void onFailure(Call<GetProjectsall_response> call, Throwable t) {
                // Log error here since request failed
                //   Log.e(TAG, t.toString());
            }
        });
    }

List<GetuserReview_geterseter> reviewlist;
    private void Rating() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetuserReview_response> call = apiService.getuserreview();
        call.enqueue(new Callback<GetuserReview_response>() {
            @Override
            public void onResponse(Call<GetuserReview_response> call, Response<GetuserReview_response> response) {
                int statusCode = response.code();

                reviewlist = response.body().getResponse();


                mReviewAdapter=new ReviewListAdapter(context,reviewlist);
                rvReviews.setAdapter(mReviewAdapter);

            }

            @Override
            public void onFailure(Call<GetuserReview_response> call, Throwable t) {
                // Log error here since request failed
                //   Log.e(TAG, t.toString());
            }
        });
    }
}
