package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/21/2018.
 */

public class ReviewModel {

    String ReviewerID,Rating,ReviewText,ReviewerName;
    int ReviewerPicture;


    public ReviewModel(String reviewerID, String rating, String reviewText, String reviewerName, int reviewerPicture) {
        ReviewerID = reviewerID;
        Rating = rating;
        ReviewText = reviewText;
        ReviewerName = reviewerName;
        ReviewerPicture = reviewerPicture;
    }

    public String getReviewerID() {
        return ReviewerID;
    }

    public void setReviewerID(String reviewerID) {
        ReviewerID = reviewerID;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getReviewText() {
        return ReviewText;
    }

    public void setReviewText(String reviewText) {
        ReviewText = reviewText;
    }

    public String getReviewerName() {
        return ReviewerName;
    }

    public void setReviewerName(String reviewerName) {
        ReviewerName = reviewerName;
    }

    public int getReviewerPicture() {
        return ReviewerPicture;
    }

    public void setReviewerPicture(int reviewerPicture) {
        ReviewerPicture = reviewerPicture;
    }
}
