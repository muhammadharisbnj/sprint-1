package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Saveuserrating_response implements Serializable
{

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Message")
    @Expose
    private String message;
    private final static long serialVersionUID = -8605104747622175067L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Saveuserrating_response() {
    }

    /**
     *
     * @param message
     * @param status
     */
    public Saveuserrating_response(String status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}