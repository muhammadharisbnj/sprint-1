package com.design.hrh.banjaegaandroidapp.ExternalFiles.Request;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface IResult {
    public void notifySuccess(String servicename, JSONObject response) throws JSONException;

    public void notifySuccess(String servicename, JSONArray response);

    public void notifyError(String servicename, VolleyError error);

    public void notifySucessEmail(String servicename, JSONObject response);

    public void notifyRatingSuccess(String servicename, JSONObject response);

    void notifyLocationsucess(String servicename, JSONObject response);

    void notifyClientReviews(String servicename, JSONObject response);
}
