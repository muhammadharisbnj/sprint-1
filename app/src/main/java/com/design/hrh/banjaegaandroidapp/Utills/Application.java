package com.design.hrh.banjaegaandroidapp.Utills;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by MuhammadAbubakar on 3/3/2018.
 */

public class Application extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static Application mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
        //        .setDefaultFontPath("assets/fonts/Cassia-W01-Medium.ttf")
        //        .setFontAttrId(R.attr.fontPath)
        //        .build());
        //FontsOverride.setDefaultFont(this, "SERIF", "fonts/Cassia-W01-Medium.ttf");
        //  TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "Cassia-W01-Medium.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

        printHashkey();
    }
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(Application.this);
    }
 /*   public GoogleSignInOptions getGoogleSignInOptions(){
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        return gso;
    }*/

    public static synchronized Application getInstance() {
        return mInstance;
    }


    public void printHashkey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.design.hrh.banjaegaandroidapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }
}

