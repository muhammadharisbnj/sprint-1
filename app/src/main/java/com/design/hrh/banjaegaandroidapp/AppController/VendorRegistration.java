package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.Utills.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class VendorRegistration extends AppCompatActivity {
    EditText etBusinessName, etContacInfo, etSellingInfo, etAddress, etFullName, etEmailAddress, etType;
    Button btnRegister;
    VolleyService mVolleyService;
    IResult mResultCallback = null;
    GPSTracker gpsTracker;
    double latitude = 33.7205829, longitude = 73.0736015;
    Context context;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_registration);
        context = VendorRegistration.this;
        initVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback, this);
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            gpsTracker = new GPSTracker(this);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
           /* searchLatitude = gpsTracker.getLatitude();
            searchLongitude = gpsTracker.getLongitude();
*/
        } else {
            gpsTracker.showSettingsAlert();
        }
        init();
        onClick();
        changelatlongtoaddress(latitude,longitude);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void onClick() {

        etAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showplacefragment(PLACE_AUTOCOMPLETE_REQUEST_CODE);
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etBusinessName.getText().toString().matches("") &&
                        !etContacInfo.getText().toString().matches("") &&
                        !etSellingInfo.getText().toString().matches("") &&
                        !etFullName.getText().toString().matches("") &&
                        !etAddress.getText().toString().matches("")) {

                    String bussinessName = etBusinessName.getText().toString();
                    String contactInfo = etContacInfo.getText().toString();
                    String address = etAddress.getText().toString();
                    String sellingInfo = etSellingInfo.getText().toString();
                    String fullName = etFullName.getText().toString();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("business_name", bussinessName);
                        jsonObject.put("full_name", fullName);
                        jsonObject.put("user_type", "vendor");
                        jsonObject.put("phone", contactInfo);
                        jsonObject.put("address", address);
                        jsonObject.put("lat", latitude);
                        jsonObject.put("long", longitude);
                        jsonObject.put("tags", sellingInfo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Progressloader.loader(VendorRegistration.this);
                    mVolleyService.saveVendoRegistrationInfo("POST", "registration", jsonObject);

                } else {
                    Toast.makeText(VendorRegistration.this, "Please Fill All Fields To Register", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etFullName = (EditText) findViewById(R.id.et_fullname);
        // etEmailAddress= (EditText)findViewById(R.id.et_email);
        etBusinessName = (EditText) findViewById(R.id.et_bussiness);
        etContacInfo = (EditText) findViewById(R.id.et_contact);
        etAddress = (EditText) findViewById(R.id.et_address);
        // etType = (EditText)findViewById(R.id.et_type);
        etSellingInfo = (EditText) findViewById(R.id.et_selling);
        btnRegister = (Button) findViewById(R.id.btn_register);
    }


    void changelatlongtoaddress(double latitude, double longitude) {
        List<Address> addresses;
        Geocoder geoCoder = new Geocoder(VendorRegistration.this);
        try {

            addresses = geoCoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                etAddress.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());

            }
        } catch (IOException e) { // TODO Auto-generated catch block
            e.printStackTrace();

        }}

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String servicename, JSONObject response) {

                Progressloader.hideloader();
                try {
                    Toast.makeText(context, response.getString("Message"), Toast.LENGTH_SHORT).show();
                    clearvalues();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifySuccess(String servicename, JSONArray response) {
                Progressloader.hideloader();
            }

            @Override
            public void notifySucessEmail(String servicename, JSONObject response) {

            }

            @Override
            public void notifyRatingSuccess(String servicename, JSONObject response) {

            }

            @Override
            public void notifyLocationsucess(String servicename, JSONObject response) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyClientReviews(String servicename, JSONObject response) {

            }

            @Override
            public void notifyError(String servicename, VolleyError error) {

                Progressloader.hideloader();
            }
        };
    }

    private void clearvalues() {
        etAddress.setText("");
        etBusinessName.setText("");
        etContacInfo.setText("");
        etFullName.setText("");
        etSellingInfo.setText("");
    }

    public void showplacefragment(int requestcode) {
        try {
            //AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("PK").setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)//.setFilter(typeFilter)
                    .build(VendorRegistration.this);
            startActivityForResult(intent, requestcode);
        } catch (GooglePlayServicesRepairableException e) {
            isGooglePlayServicesAvailable(VendorRegistration.this);
        } catch (GooglePlayServicesNotAvailableException e) {
            isGooglePlayServicesAvailable(VendorRegistration.this);
        }

    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                etAddress.setText(String.valueOf(place.getName()));
                etAddress.setSelection(etAddress.getText().length());

                LatLng destination = place.getLatLng();
                latitude = destination.latitude;
                latitude = destination.longitude;


            }


        }
    }
}
