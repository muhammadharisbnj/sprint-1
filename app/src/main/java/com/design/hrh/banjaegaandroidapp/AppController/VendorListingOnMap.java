package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.design.hrh.banjaegaandroidapp.Adapters.VendorListAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.VendorModel;
import com.design.hrh.banjaegaandroidapp.Model.YCategoryModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Search_Response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Search_model_category;
import com.design.hrh.banjaegaandroidapp.Widget.SeparatorDecoration;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiClient;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VendorListingOnMap extends AppCompatActivity implements VendorListAdapter.OnArchitectItemClickListener {

    Activity context = VendorListingOnMap.this;
    RecyclerView rvYCategory,rvVendorList;
    ArrayList<YCategoryModel> YCategories = new ArrayList<>();
    ArrayList<VendorModel> Vendors = new ArrayList<>();
    YCategoryAdapter mAdapter;
    VendorListAdapter mVendorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_listing_on_map);
        init();
    }

    public void init()
    {
        rvVendorList=(RecyclerView) findViewById(R.id.rv_vendor_list);

        rvVendorList.setLayoutManager(new LinearLayoutManager(context));
        rvYCategory=(RecyclerView) findViewById(R.id.rv_category_yellow);
        rvYCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        YCategories.add(new YCategoryModel("Architect", R.drawable.architect_grey));
        YCategories.add(new YCategoryModel("Tiles", R.drawable.tiles));
        YCategories.add(new YCategoryModel("Bathrooms", R.drawable.bathrooms_grey));
        YCategories.add(new YCategoryModel("Builder", R.drawable.builders_grey));
        YCategories.add(new YCategoryModel("Kitchen", R.drawable.kitchens_grey));
        YCategories.add(new YCategoryModel("More", R.drawable.more));
        Vendors.add(new VendorModel("Creative Sol",R.drawable.profile_image_with_badge));
        Vendors.add(new VendorModel("Creative Sol",R.drawable.profile_image_with_badge));

        mAdapter = new YCategoryAdapter(context, YCategories);
        SeparatorDecoration itemDecoration=new SeparatorDecoration(getResources().getColor(R.color.colorPrimaryDark),5,0,0);
        rvVendorList.addItemDecoration(itemDecoration);

        vanderApi();


        rvYCategory.setAdapter(mAdapter);

        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }





// Api call**************************************************
    private void vanderApi() {
        Progressloader.loader(VendorListingOnMap.this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Search_Response> call = apiService.search_category("1");
        call.enqueue(new Callback<Search_Response>() {
            @Override
            public void onResponse(Call<Search_Response> call, Response<Search_Response> response) {
                Progressloader.hideloader();
                int statusCode = response.code();
                List<Search_model_category> movies=response.body().getResult();

                mVendorAdapter=new VendorListAdapter(context,movies);
                rvVendorList.setAdapter(mVendorAdapter);
                mVendorAdapter.SetOnItemClickListener(VendorListingOnMap.this);


            }

            @Override
            public void onFailure(Call<Search_Response> call, Throwable t) {
                // Log error here since request failed

                Toast.makeText(VendorListingOnMap.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void OnArchitectItemClick(View view, int position) {

   startActivity(new Intent(this, Vandor_detail.class));

    }
}
