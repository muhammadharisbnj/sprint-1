package com.design.hrh.banjaegaandroidapp.Helper;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;

import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.Model.SocialUserModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class MyHelper {
    private static MyHelper instance = null;

    public static final String BASE_URL = "https://www.banjaiga.com/web_services/index.php/";

    public static String name_ = "";
    public static  String image_ = "";
    public static  String address = "";
    public static  String number = "";

    public static  boolean aBoolean= true;


    public static Context context;
    public ArrayList<SocialUserModel> socialUserModels = new ArrayList<>();
    public static AllArchitectsDataModel currentUserData = null;
    public MyHelper(Context mContext)
    {
        context = mContext;

    }

    public static MyHelper getInstance(Context mContext) {
        if (instance == null) {
            instance = new MyHelper(mContext);
            SharedPreferences prefJobDetail = mContext.getSharedPreferences("jobDetailData", Context.MODE_PRIVATE);
            prefJobDetail.edit().putBoolean("isApiFirstCall",false).apply();
        }
        context = mContext;
        return instance;
    }
    public static String checkStringIsNull(JSONObject jsonObject, String parameter){
        String value = "";
        try{
            if (jsonObject.has(parameter)){
                if (!jsonObject.isNull(parameter) && !jsonObject.getString(parameter).equals("[]"))
                    value = jsonObject.getString(parameter);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return value;
    }

    public static String checkStringIsNull(JSONObject jsonObject, String parameter,String defaultValue) {
        String value = defaultValue;
        try{
            if (jsonObject.has(parameter)){
                if (!jsonObject.isNull(parameter) && !jsonObject.getString(parameter).equals("[]"))
                    value = jsonObject.getString(parameter);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return value;
    }

    // Phone Call
    public static void makeCall(Context context,String phone_num) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        context.startActivity(intent);
    }
    public static void sendSMS(Context context,String phone_num) {
        // The number on which you want to send SMS
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone_num, null)));
    }


}
