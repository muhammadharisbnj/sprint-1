package com.design.hrh.banjaegaandroidapp.Fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.design.hrh.banjaegaandroidapp.AppController.DashboardNewActivity;
import com.design.hrh.banjaegaandroidapp.AppController.Privacy_policy;
import com.design.hrh.banjaegaandroidapp.R;


public class Setting extends Fragment implements View.OnClickListener{

    View view;
    LinearLayout terms_condition,privacy_policy;

    // TODO: Rename and change types and number of parameters
    public static Setting newInstance(String param1, String param2) {
        Setting fragment = new Setting();
        Bundle args = new Bundle();
        args.putString("", param1);
        args.putString("", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_setting, container, false);
        terms_condition=(LinearLayout)view.findViewById(R.id.terms_condition);
        privacy_policy=(LinearLayout)view.findViewById(R.id.privacy_policy);



        terms_condition.setOnClickListener(this);
        privacy_policy.setOnClickListener(this);

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        if (v == terms_condition){

            try {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.banjaiga.com"));
                startActivity(myIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getActivity(), "No application can handle this request."
                        + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

          }

          if (v == privacy_policy){
             startActivity(new Intent(getActivity(),Privacy_policy.class));
        }
        ((DashboardNewActivity) getActivity()).drawrclose();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

/*try {
    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(download_link));
    startActivity(myIntent);
} catch (ActivityNotFoundException e) {
    Toast.makeText(this, "No application can handle this request."
        + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
    e.printStackTrace();
}*/