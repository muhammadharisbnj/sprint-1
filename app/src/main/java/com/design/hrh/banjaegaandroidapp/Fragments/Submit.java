package com.design.hrh.banjaegaandroidapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.design.hrh.banjaegaandroidapp.AppController.ArchitechtActivity;
import com.design.hrh.banjaegaandroidapp.AppController.Architects_review;
import com.design.hrh.banjaegaandroidapp.AppController.VendorRatingNewActivity;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Saveuserrating_response;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiClient;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Submit#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Submit extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Submit() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Submit.
     */
    // TODO: Rename and change types and number of parameters
    public static Submit newInstance(String param1, String param2) {
        Submit fragment = new Submit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_submit, container, false);

        Button submit=(Button)view.findViewById(R.id.next);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
if (CSPreferences.getBoolean(getActivity(),"fb_login")==true){
    Apicall();
}else {
    startActivity(new Intent(getActivity(), ArchitechtActivity.class));
}

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void Apicall() {
        Progressloader.loader(getActivity());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Saveuserrating_response> call = apiService.getprojectresponse("2496","sssss",""+CSPreferences.readString(getActivity(),"Fb_Email"),"aa","ss",""+CSPreferences.readString(getActivity(),"service"),""+CSPreferences.readString(getActivity(),"price"),""+CSPreferences.readString(getActivity(),"quality"),"1.2");

        Log.d("android", String.valueOf(call));
        call.enqueue(new Callback<Saveuserrating_response>() {
            @Override
            public void onResponse(Call<Saveuserrating_response> call, Response<Saveuserrating_response> response) {
                  Progressloader.hideloader();
                if (response.body().getStatus().equalsIgnoreCase("200")){
                    ((VendorRatingNewActivity)getActivity()).setCurrentItem (4, true);
                    Toast.makeText(getActivity(), ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(getActivity(), ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Saveuserrating_response> call, Throwable t) {
                Toast.makeText(getActivity(), ""+t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
