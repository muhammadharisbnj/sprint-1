package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.design.hrh.banjaegaandroidapp.Adapters.FeaturedArchitectsAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.FullImageActivity;
import com.design.hrh.banjaegaandroidapp.Adapters.ProductsUsedAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.ProjectGalleryAdapter;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Model.FeaturedArchitects;
import com.design.hrh.banjaegaandroidapp.Model.ProductsUsed;
import com.design.hrh.banjaegaandroidapp.Model.ProjectGalleryModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetArchitects_list;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.http.POST;

public class ProjectActivity extends AppCompatActivity implements ProjectGalleryAdapter.OnItemClickListener,View.OnClickListener {
Activity context=ProjectActivity.this ;
RecyclerView rvProjectGallery ,rvArchitectsInvolved , rvProductsUsed , rvManufacturesInvolved ;
ProjectGalleryAdapter mAdapter;
FeaturedArchitectsAdapter mArchitectsAdapter;
ProductsUsedAdapter mProductsAdapter;
ArrayList<FeaturedArchitects> ArchitectsManufactures =new ArrayList<>();
ArrayList<ProductsUsed> Products=new ArrayList<>();
ArrayList<ProjectGalleryModel> Gallery=new ArrayList<>();
RelativeLayout rlLeaveAReview;
    GetArchitects_list getArchitects_list = null;
    String ProjectJson;

    TextView textView,cuntory;
    ImageView Single_image;
    ScaleRatingBar ratingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        ProjectJson=getIntent().getStringExtra("ProjectJson");
        Gson gson=new Gson();
        getArchitects_list=gson.fromJson(ProjectJson,GetArchitects_list.class);
        System.err.println(getArchitects_list.getCity());



        init();

    }


    private void init() {

        textView=(TextView)findViewById(R.id.txt_project_name);
        cuntory=(TextView)findViewById(R.id.txt_architect_location);
        textView.setText(getArchitects_list.getUsername());
        cuntory.setText(getArchitects_list.getCity()+","+getArchitects_list.getCountry());


        Single_image=(ImageView)findViewById(R.id.img_banner) ;

        ratingBar = new ScaleRatingBar(this);
        ratingBar=(ScaleRatingBar)findViewById(R.id.rating_architecture);

        // Set rating
        ratingBar.setRating(Float.parseFloat(getArchitects_list.getUserReviews()));




        rvArchitectsInvolved=(RecyclerView)findViewById(R.id.rv_architects_involved);
        rvProductsUsed=(RecyclerView)findViewById(R.id.rv_products_used);
        rvManufacturesInvolved=(RecyclerView)findViewById(R.id.rv_manufacturer_involved);
        rvArchitectsInvolved.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvProductsUsed.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvManufacturesInvolved.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvProjectGallery=(RecyclerView)findViewById(R.id.rv_project_gallery);
        rvProjectGallery.setLayoutManager(new GridLayoutManager(context,2));
        Products.add(new ProductsUsed(R.drawable.img_product));
        Products.add(new ProductsUsed(R.drawable.img_product));
        Products.add(new ProductsUsed(R.drawable.img_product));
        Products.add(new ProductsUsed(R.drawable.img_product));
        Products.add(new ProductsUsed(R.drawable.img_product));
        Products.add(new ProductsUsed(R.drawable.img_product));
        Products.add(new ProductsUsed(R.drawable.img_product));
        ArchitectsManufactures.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        ArchitectsManufactures.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        ArchitectsManufactures.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        ArchitectsManufactures.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        ArchitectsManufactures.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        ArchitectsManufactures.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));


        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_pic));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_2));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_2));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_pic));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_pic));
        Gallery.add(new ProjectGalleryModel(R.drawable.img_project_2));
        mProductsAdapter=new ProductsUsedAdapter(context,Products);
      //  mAdapter=new ProjectGalleryAdapter(context,Gallery);
       // mArchitectsAdapter=new FeaturedArchitectsAdapter(context,ArchitectsManufactures);
       // rvManufacturesInvolved.setAdapter(mArchitectsAdapter);

        rvArchitectsInvolved.setAdapter(mArchitectsAdapter);

// Set Adapter
        //rvProductsUsed.setAdapter(mProductsAdapter);



//        mAdapter.SetOnItemClickListener(this);


       // rvProjectGallery.setAdapter(mAdapter);
        rlLeaveAReview=(RelativeLayout)findViewById(R.id.rl_leave_review);
        rlLeaveAReview.setOnClickListener(this);



        String images = getArchitects_list.getPictures();
        Log.d("sdlvd;",images);
        // Spilt String
        String[] separated = images.split(",");
        String ImageUrl="https://banjaiga.com/uploads/projects-images/"+getArchitects_list.getProjectId()+"/"+separated[0];
        Glide.with(context).load(ImageUrl).into(Single_image);

       Log.d("dkkldvlkdmvdmvd", String.valueOf(separated));


        List<String> items = Arrays.asList(images.split(","));


        String aaaaa= String.valueOf(items);


        Log.d("mcvmc", String.valueOf(items));
        try {
            JSONArray jsonArray = new JSONArray(aaaaa);
            String[] strArr = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                strArr[i] = jsonArray.getString(i);
                Log.d("array", strArr[i]);



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    public void onItemClick(View view, int position) {

        startActivity(new Intent(ProjectActivity.this, FullImageActivity.class));

    }

    @Override
    public void onClick(View v) {
        if(v==rlLeaveAReview)
        {

            CSPreferences.putBolean(this,"Projectactivity",false);
        startActivity(new Intent(context,VendorRatingNewActivity.class));
        }


    }
}
