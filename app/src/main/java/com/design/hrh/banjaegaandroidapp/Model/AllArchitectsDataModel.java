package com.design.hrh.banjaegaandroidapp.Model;

import android.content.Context;

import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;

import org.json.JSONObject;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class AllArchitectsDataModel {
    private String archId = "";
    private String Id = "";
    private String userName = "";
    private String phoneNumberONE = "";
    private String phoneNumberTWO = "";
    private String logo = "";
    private String archCity = "";
    private String archCountry = "";
    private String archaddress = "";
    private String latitude = "";
    private String longitude = "";
    private String views = "";
    private String flags = "";
    private String userReviews = "";
    private String avgRating = "";
    private String avgPrice = "";
    private String avgQuality = "";
    private String avgServices = "";
    private String isVerified = "";
    private String isTrusted = "";
    private String isMobile = "";
    private String isEmail = "";
    private String isFeatured = "";
    private String vendorId = "";
    private String architechtId = "";
    private String membershipType = "";
    private String cloud_tags = "";

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    private String user_type = "";


    public AllArchitectsDataModel(Context context, JSONObject jsonObject) {
        try {
            setArchId(MyHelper.checkStringIsNull(jsonObject, "user_id", ""));
            setUserName(MyHelper.checkStringIsNull(jsonObject, "username", ""));
            setPhoneNumberONE(MyHelper.checkStringIsNull(jsonObject, "phone", ""));
            setPhoneNumberTWO(MyHelper.checkStringIsNull(jsonObject, "mobile", ""));
            setArchaddress(MyHelper.checkStringIsNull(jsonObject, "address", ""));
            setLogo(MyHelper.checkStringIsNull(jsonObject, "logo", ""));
            setArchCountry(MyHelper.checkStringIsNull(jsonObject, "country", ""));
            setArchCity(MyHelper.checkStringIsNull(jsonObject, "city", ""));
            setLatitude(MyHelper.checkStringIsNull(jsonObject, "lat", ""));
            setLongitude(MyHelper.checkStringIsNull(jsonObject, "long", ""));
            setViews(MyHelper.checkStringIsNull(jsonObject, "views", ""));
            setUser_type(MyHelper.checkStringIsNull(jsonObject, "user_type", ""));
            setId(MyHelper.checkStringIsNull(jsonObject, "id", ""));
            setUserReviews(MyHelper.checkStringIsNull(jsonObject, "user_reviews", "0"));
            setAvgPrice(MyHelper.checkStringIsNull(jsonObject, "avg_pricing", "0"));
            setAvgQuality(MyHelper.checkStringIsNull(jsonObject, "avg_quality", "0"));
            setAvgServices(MyHelper.checkStringIsNull(jsonObject, "avg_service", "0"));
            setAvgRating(MyHelper.checkStringIsNull(jsonObject, "avg_rating", "0"));
            setIsVerified(MyHelper.checkStringIsNull(jsonObject, "is_verified", ""));
            setIsEmail(MyHelper.checkStringIsNull(jsonObject, "is_email", ""));
            setIsTrusted(MyHelper.checkStringIsNull(jsonObject, "is_trusted", ""));
            setIsMobile(MyHelper.checkStringIsNull(jsonObject, "is_mobile", ""));
            setIsFeatured(MyHelper.checkStringIsNull(jsonObject, "is_featured", ""));
            setVendorId(MyHelper.checkStringIsNull(jsonObject, "vendor_id", ""));
            setMembershipType(MyHelper.checkStringIsNull(jsonObject, "membership_type", ""));
            setCloud_tags(MyHelper.checkStringIsNull(jsonObject, "cloud_tags", ""));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public String getCloud_tags() {
        return cloud_tags;
    }

    public void setCloud_tags(String cloud_tags) {
        this.cloud_tags = cloud_tags;
    }

    public String getAvgServices() {
        return avgServices;
    }

    public void setAvgServices(String avgServices) {
        this.avgServices = avgServices;
    }

    public String getUserReviews() {
        return userReviews;
    }

    public void setUserReviews(String userReviews) {
        this.userReviews = userReviews;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    public String getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(String avgPrice) {
        this.avgPrice = avgPrice;
    }

    public String getAvgQuality() {
        return avgQuality;
    }

    public void setAvgQuality(String avgQuality) {
        this.avgQuality = avgQuality;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getArchId() {
        return archId;
    }

    public void setArchId(String archId) {
        this.archId = archId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumberONE() {
        return phoneNumberONE;
    }

    public void setPhoneNumberONE(String phoneNumberONE) {
        this.phoneNumberONE = phoneNumberONE;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getArchCity() {
        return archCity;
    }

    public void setArchCity(String archCity) {
        this.archCity = archCity;
    }

    public String getArchCountry() {
        return archCountry;
    }

    public void setArchCountry(String archCountry) {
        this.archCountry = archCountry;
    }

    public String getArchaddress() {
        return archaddress;
    }

    public void setArchaddress(String archaddress) {
        this.archaddress = archaddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getIsTrusted() {
        return isTrusted;
    }

    public void setIsTrusted(String isTrusted) {
        this.isTrusted = isTrusted;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }

    public String getIsEmail() {
        return isEmail;
    }

    public void setIsEmail(String isEmail) {
        this.isEmail = isEmail;
    }

    public String getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getArchitechtId() {
        return architechtId;
    }

    public void setArchitechtId(String architechtId) {
        this.architechtId = architechtId;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getPhoneNumberTWO() {
        return phoneNumberTWO;
    }

    public void setPhoneNumberTWO(String phoneNumberTWO) {
        this.phoneNumberTWO = phoneNumberTWO;
    }
}
