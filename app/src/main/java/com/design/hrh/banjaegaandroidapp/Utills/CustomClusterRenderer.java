package com.design.hrh.banjaegaandroidapp.Utills;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.design.hrh.banjaegaandroidapp.AppController.DashboardNewActivity;

import com.design.hrh.banjaegaandroidapp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class CustomClusterRenderer extends DefaultClusterRenderer<DashboardNewActivity.StringClusterItem> {

    private final Context mContext;
    private final IconGenerator mClusterIconGenerator;
    String UserType;


    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<DashboardNewActivity.StringClusterItem> clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        mClusterIconGenerator = new IconGenerator(mContext.getApplicationContext());

    }




    @Override
    protected void onBeforeClusterItemRendered(DashboardNewActivity.StringClusterItem item, MarkerOptions markerOptions) {
//        final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(R.drawable.app_icon);


        if (item.usertype.equalsIgnoreCase("architect")) {
            if (item.type.equals("gold")) {
                getDrawable(mContext, R.drawable.gold_architects_map, item, markerOptions);
                // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).snippet(item.type);
            } else if (item.type.equals("regular")) {
                getDrawable(mContext, R.drawable.silver_architects, item, markerOptions);
                // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1)).snippet(item.type);

            } else if (item.type.equals("")) {
                getDrawable(mContext, R.drawable.silver_architects, item, markerOptions);
            }

        } else if (item.usertype.equalsIgnoreCase("vendor")) {

            if (item.type.equals("gold")) {
                getDrawable(mContext, R.drawable.gold_vendor, item, markerOptions);
                // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).snippet(item.type);
            } else if (item.type.equals("regular")) {
                getDrawable(mContext, R.drawable.silver_vendor, item, markerOptions);
                // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1)).snippet(item.type);
            } else if (item.type.equals("")) {
                getDrawable(mContext, R.drawable.silver_vendor, item, markerOptions);
            }

        } else {
       /* if (item.type.equals("gold")) {
            getDrawable(mContext, R.drawable.gold_members, item, markerOptions);
            // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).snippet(item.type);
        } else if (item.type.equals("regular")) {
            getDrawable(mContext, R.drawable.silver_members, item, markerOptions);
            // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1)).snippet(item.type);
        }*/

        }
    }

/*
if (item.type.equals("gold")){
    getDrawable(mContext,R.drawable.gold_architects_map,item,markerOptions);
   // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).snippet(item.type);
}else if (item.type.equals("regular")){
    getDrawable(mContext,R.drawable.silver_architects,item,markerOptions);
   // markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker1)).snippet(item.type);

}else {
}*//* {
    if (item.type.equals("gold") || item.type.equals("architect")){

}else if (item.type.equals("regular") || item.type.equals("architect"))

}*/


     //   BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.gold_architects_map);







    @Override
    protected void onBeforeClusterRendered(Cluster<DashboardNewActivity.StringClusterItem> cluster,
                                                     MarkerOptions markerOptions) {
        mClusterIconGenerator.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_circle));
        mClusterIconGenerator.setTextAppearance(R.style.CustomTextTheme);
        final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

       /* List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
        int width = mDimension;
        int height = mDimension;

        for (Person p : cluster.getItems()) {
            // Draw 4 at most.
            if (profilePhotos.size() == 4) break;
            Drawable drawable = getResources().getDrawable(p.profilePhoto);
            drawable.setBounds(0, 0, width, height);
            profilePhotos.add(drawable);
        }
        MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
        multiDrawable.setBounds(0, 0, width, height);

        mClusterImageView.setImageDrawable(multiDrawable);
        Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));*/






    }
    public static final Drawable getDrawable(Context context, int id, DashboardNewActivity.StringClusterItem item, MarkerOptions markerOptions) {
        int height = 80;
        int width = 70;
        BitmapDrawable bitmapdraw=(BitmapDrawable)context.getResources().getDrawable(id);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap bitmap = Bitmap.createScaledBitmap(b, width, height, false);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap)).snippet(item.type);
        return context.getResources().getDrawable(id);

    }

    /*@Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        return cluster.getSize() > 5; // if markers <=5 then not clustering
    }*/
}
