package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.design.hrh.banjaegaandroidapp.Model.ReviewModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetuserReview_geterseter;
import com.willy.ratingbar.ScaleRatingBar;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Haroon G on 2/28/2018.
 */

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.MyViewHolder> {
    private final Context context;

    private List<GetuserReview_geterseter> arraynavicons;

    private ReviewModel Navscrollitemes;
    OnItemClickListener mItemClickListener;
    int selected_position = -1;


    public ReviewListAdapter(Context context, List<GetuserReview_geterseter> arraynavicons) {

        this.context = context;
        this.arraynavicons = arraynavicons;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView ReviewerPic;
        TextView  txtReviewerName;
        ScaleRatingBar mRatingBar;
        TextView txtReviewText;


        public MyViewHolder(View view) {
            super(view);
            ReviewerPic = view.findViewById(R.id.img_icon);
            txtReviewerName = view.findViewById(R.id.txt_reviewer_name);
            mRatingBar=(ScaleRatingBar)view.findViewById(R.id.rating);
            txtReviewText=(TextView)view.findViewById(R.id.txt_review);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_item, parent, false);

        return new MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        GetuserReview_geterseter getuserReview_geterseter = arraynavicons.get(position);

        Glide.with(context).load(getuserReview_geterseter.getImageUrl()).into(holder.ReviewerPic);
//        holder.ReviewerPic.setImageResource(selected_position == position ? arraynavicons.get(position).getChangedrawable() : arraynavicons.get(position).getDrawable());

//        holder.txtReviewerName.setText(arraynavicons.get(position).getImagetag());


        holder.txtReviewerName.setText(getuserReview_geterseter.getUsername());
        holder.mRatingBar.setRating(Float.parseFloat(getuserReview_geterseter.getService()));

        holder.txtReviewText.setText(getuserReview_geterseter.getReview().replace('+',' '));

       /* if (arraynavicons.get(position).isImagechanged()) {
            holder.ReviewerPic.setImageResource(arraynavicons.get(position).getChangedrawable());

        } else {
            holder.ReviewerPic.setImageResource(arraynavicons.get(position).getDrawable());
        }*/


    }


    @Override
    public int getItemCount() {
        return arraynavicons.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }
    public void clearselection()
    {

        notifyItemChanged(1);
        selected_position = -1;
        notifyItemChanged(selected_position);
        notifyDataSetChanged();

    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}





