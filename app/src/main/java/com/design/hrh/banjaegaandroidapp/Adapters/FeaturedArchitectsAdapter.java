package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.design.hrh.banjaegaandroidapp.Model.FeaturedArchitects;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Get_architects_data;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Haroon G on 2/28/2018.
 */

public class FeaturedArchitectsAdapter extends RecyclerView.Adapter<FeaturedArchitectsAdapter.MyViewHolder> {
    private final Context context;

    private List<Get_architects_data> arraynavicons;

    private FeaturedArchitects Navscrollitemes;
    OnArchitectItemClickListener mItemClickListener;
    int selected_position = -1;


    public FeaturedArchitectsAdapter(Context context, List<Get_architects_data> arraynavicons) {

        this.context = context;
        this.arraynavicons = arraynavicons;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView Navicon;
        TextView txtImgtag;


        public MyViewHolder(View view) {
            super(view);
            Navicon = view.findViewById(R.id.img_icon);
//            txtImgtag = view.findViewById(R.id.txt_category_name);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);

            if (mItemClickListener != null) {
                mItemClickListener.OnArchitectItemClick(view, getPosition());
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.architect_item, parent, false);

        return new MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Get_architects_data navscrollitemes = arraynavicons.get(position);


        Glide.with(context).load(navscrollitemes.getLogo()).into(holder.Navicon);



//        holder.Navicon.setImageResource(selected_position == position ? arraynavicons.get(position).getChangedrawable() : arraynavicons.get(position).getDrawable());

//        holder.txtImgtag.setText(arraynavicons.get(position).getImagetag());


//        holder.txtImgtag.setText(Navscrollitemes.getCategoryName());

       /* if (arraynavicons.get(position).isImagechanged()) {
            holder.Navicon.setImageResource(arraynavicons.get(position).getChangedrawable());

        } else {
            holder.Navicon.setImageResource(arraynavicons.get(position).getDrawable());
        }*/


    }


    @Override
    public int getItemCount() {
        return arraynavicons.size();
    }

    public interface OnArchitectItemClickListener {
        public void OnArchitectItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnArchitectItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }
    public void clearselection()
    {

        notifyItemChanged(1);
        selected_position = -1;
        notifyItemChanged(selected_position);
        notifyDataSetChanged();

    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}





