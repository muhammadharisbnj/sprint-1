package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/19/2018.
 */

public class FeaturedArchitects {

    String FeaturedArchitects;
    int ArchitectImage;
    String Rating;
    String ReviewsCount;

    public FeaturedArchitects(String featuredArchitects, int architectImage, String rating, String reviewsCount) {
        FeaturedArchitects = featuredArchitects;
        ArchitectImage = architectImage;
        Rating = rating;
        ReviewsCount = reviewsCount;
    }


    public String getFeaturedArchitects() {
        return FeaturedArchitects;
    }

    public void setFeaturedArchitects(String featuredArchitects) {
        FeaturedArchitects = featuredArchitects;
    }

    public int getArchitectImage() {
        return ArchitectImage;
    }

    public void setArchitectImage(int architectImage) {
        ArchitectImage = architectImage;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getReviewsCount() {
        return ReviewsCount;
    }

    public void setReviewsCount(String reviewsCount) {
        ReviewsCount = reviewsCount;
    }
}
