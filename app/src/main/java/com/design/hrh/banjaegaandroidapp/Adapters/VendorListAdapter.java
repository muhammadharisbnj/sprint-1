package com.design.hrh.banjaegaandroidapp.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.design.hrh.banjaegaandroidapp.AppController.VendorListingOnMap;
import com.design.hrh.banjaegaandroidapp.AppController.VendorRatingNewActivity;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Model.VendorModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Search_model_category;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.List;


/**
 * Created by Haroon G on 2/28/2018.
 */

public class VendorListAdapter extends RecyclerView.Adapter<VendorListAdapter.MyViewHolder> {
    private final Context context;

    private List<Search_model_category> arraynavicons;

    private VendorModel Navscrollitemes;
    OnArchitectItemClickListener mItemClickListener;
    int selected_position = -1;

     Search_model_category  search_model_category;
    public VendorListAdapter(Context context, List<Search_model_category> arraynavicons) {

        this.context = context;
        this.arraynavicons = arraynavicons;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView Navicon;
        TextView txtImgtag,cuntory,txt_leave_review;
        ScaleRatingBar ratingBar;
        Button call,message;
        RelativeLayout raview;

        public MyViewHolder(View view) {
            super(view);
            Navicon=view.findViewById(R.id.logo);
            call=view.findViewById(R.id.btn_call);
            message=view.findViewById(R.id.btn_message);
//            Navicon = view.findViewById(R.id.img_icon);
            raview=view.findViewById(R.id.reltivelayout);

            txtImgtag = view.findViewById(R.id.txt_vendor_name);
            cuntory = view.findViewById(R.id.contry);

            txt_leave_review=(TextView)view.findViewById(R.id.txt_leave_review);

            view.setOnClickListener(this);


            ratingBar=view.findViewById(R.id.rating_architecture);


            call.setOnClickListener(this);
            message.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);

            if (mItemClickListener != null) {
                mItemClickListener.OnArchitectItemClick(view, getPosition());
            }

            if (view == call){
                MyHelper.makeCall(context,search_model_category.getPhone());
            } if (view == message){
                MyHelper.sendSMS(context,search_model_category.getAddress());
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vendor_item, parent, false);

        return new MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
          search_model_category = arraynavicons.get(position);


        holder.txtImgtag.setText( stripHtml(search_model_category.getUsername()));
        holder.cuntory.setText( stripHtml(search_model_category.getCity()));

        Glide.with(context).load(search_model_category.getLogo()).error(R.drawable.profile_image_with_badge).into(holder.Navicon);

        holder.ratingBar.setRating(Float.parseFloat(search_model_category.getUserReviews()));







//        holder.Navicon.setImageResource(selected_position == position ? arraynavicons.get(position).getChangedrawable() : arraynavicons.get(position).getDrawable());



//        holder.Navicon.setImageResource(Navscrollitemes.getVendorImage());
//        holder.txtImgtag.setText(Navscrollitemes.getCategoryName());

       /* if (arraynavicons.get(position).isImagechanged()) {
            holder.Navicon.setImageResource(arraynavicons.get(position).getChangedrawable());

        } else {
            holder.Navicon.setImageResource(arraynavicons.get(position).getDrawable());
        }*/

       holder.txt_leave_review.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               CSPreferences.putBolean(context,"Projectactivity",true);
               context.startActivity(new Intent(context,VendorRatingNewActivity.class));
           }
       });

    }
    @SuppressLint("InlinedApi")
    public Spanned stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    @Override
    public int getItemCount() {
        return arraynavicons.size();
    }

    public interface OnArchitectItemClickListener {
        public void OnArchitectItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnArchitectItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }


    public void clearselection() {
        notifyItemChanged(1);
        selected_position = -1;
        notifyItemChanged(selected_position);
        notifyDataSetChanged();

    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}





