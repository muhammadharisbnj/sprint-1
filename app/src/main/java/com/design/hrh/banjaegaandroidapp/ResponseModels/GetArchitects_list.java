package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetArchitects_list {

    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("keywords")
    @Expose
    private String keywords;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("user_reviews")
    @Expose
    private String userReviews;
    @SerializedName("avg_service")
    @Expose
    private Object avgService;
    @SerializedName("avg_pricing")
    @Expose
    private Object avgPricing;
    @SerializedName("avg_quality")
    @Expose
    private Object avgQuality;
    @SerializedName("pictures")
    @Expose
    private String pictures;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetArchitects_list() {
    }

    public GetArchitects_list(String projectId, String title, String userId, String keywords, String city, String country, String username, String userReviews, Object avgService, Object avgPricing, Object avgQuality, String pictures) {
        super();
        this.projectId = projectId;
        this.title = title;
        this.userId = userId;
        this.keywords = keywords;
        this.city = city;
        this.country = country;
        this.username = username;
        this.userReviews = userReviews;
        this.avgService = avgService;
        this.avgPricing = avgPricing;
        this.avgQuality = avgQuality;
        this.pictures = pictures;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserReviews() {
        return userReviews;
    }

    public void setUserReviews(String userReviews) {
        this.userReviews = userReviews;
    }

    public Object getAvgService() {
        return avgService;
    }

    public void setAvgService(Object avgService) {
        this.avgService = avgService;
    }

    public Object getAvgPricing() {
        return avgPricing;
    }

    public void setAvgPricing(Object avgPricing) {
        this.avgPricing = avgPricing;
    }

    public Object getAvgQuality() {
        return avgQuality;
    }

    public void setAvgQuality(Object avgQuality) {
        this.avgQuality = avgQuality;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }


}
