package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/21/2018.
 */

public class VendorModel {

  String VendorName;
  int VendorImage;


    public VendorModel(String vendorName, int vendorImage) {
        VendorName = vendorName;
        VendorImage = vendorImage;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }

    public int getVendorImage() {
        return VendorImage;
    }

    public void setVendorImage(int vendorImage) {
        VendorImage = vendorImage;
    }

}
