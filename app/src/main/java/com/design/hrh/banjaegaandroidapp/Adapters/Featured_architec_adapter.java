package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjects_list;

import java.util.List;

public class Featured_architec_adapter extends RecyclerView.Adapter<Featured_architec_adapter.MyViewHolder> {
    private final Context context;

    private List<GetProjects_list> arraynavicons;

    Featured_architec_adapter.OnItemClickListener mItemClickListener;
    int selected_position = -1;


    public Featured_architec_adapter(Context context, List<GetProjects_list> arraynavicons) {

        this.context = context;
        this.arraynavicons = arraynavicons;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView Navicon;
        TextView txtImgtag;


        public MyViewHolder(View view) {
            super(view);
            Navicon = view.findViewById(R.id.img_icon);
            txtImgtag = view.findViewById(R.id.txt_category_name);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    @Override
    public Featured_architec_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.architecture_item, parent, false);

        return new Featured_architec_adapter.MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final Featured_architec_adapter.MyViewHolder holder, int position) {
        GetProjects_list getArchitects_list=arraynavicons.get(position);




//        holder.txtImgtag.setText(getArchitects_list.getCountry());

        String images = getArchitects_list.getPictures();
        Log.d("sdlvd;",images);
        // Spilt String
        String[] separated = images.split(",");
        String ImageUrl="https://banjaiga.com/uploads/projects-images/"+getArchitects_list.getProjectId()+"/"+separated[0];
        Glide.with(context).load(ImageUrl).into(holder.Navicon);





//        holder.Navicon.setImageResource(selected_position == position ? arraynavicons.get(position).getChangedrawable() : arraynavicons.get(position).getDrawable());

//        holder.txtImgtag.setText(arraynavicons.get(position).getImagetag());


//        holder.txtImgtag.setText(Navscrollitemes.getCategoryName());

       /* if (arraynavicons.get(position).isImagechanged()) {
            holder.Navicon.setImageResource(arraynavicons.get(position).getChangedrawable());

        } else {
            holder.Navicon.setImageResource(arraynavicons.get(position).getDrawable());
        }*/


    }


    @Override
    public int getItemCount() {
        return arraynavicons.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final Featured_architec_adapter.OnItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }


    public void clearselection()
    {

        notifyItemChanged(1);
        selected_position = -1;
        notifyItemChanged(selected_position);
        notifyDataSetChanged();

    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}




