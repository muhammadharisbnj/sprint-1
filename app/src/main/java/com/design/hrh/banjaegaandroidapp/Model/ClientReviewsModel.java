package com.design.hrh.banjaegaandroidapp.Model;

import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;

import org.json.JSONObject;

/**
 * Created by MuhammadAbubakar on 3/21/2018.
 */

public class ClientReviewsModel {
    String clientId = "";
    String clientUserName = "";
    String email = "";
    String imageUrl = "";
    String userId = "";
    String userType = "";
    String serviceRating ="";
    String pricingRating ="";
    String qualityRating ="";
    String reviewDescription ="";

    public ClientReviewsModel (JSONObject jsonObject){
        setClientId(MyHelper.checkStringIsNull(jsonObject, "id", ""));
        setClientUserName(MyHelper.checkStringIsNull(jsonObject, "username", ""));
        setEmail(MyHelper.checkStringIsNull(jsonObject, "email", ""));
        setImageUrl(MyHelper.checkStringIsNull(jsonObject, "image_url", ""));
        setUserId(MyHelper.checkStringIsNull(jsonObject, "user_id", ""));
        setUserType(MyHelper.checkStringIsNull(jsonObject, "user_type", ""));
        setServiceRating(MyHelper.checkStringIsNull(jsonObject, "service", ""));
        setPricingRating(MyHelper.checkStringIsNull(jsonObject, "pricing", ""));
        setQualityRating(MyHelper.checkStringIsNull(jsonObject, "quality", ""));
        setReviewDescription(MyHelper.checkStringIsNull(jsonObject, "review", ""));
    }
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientUserName() {
        return clientUserName;
    }

    public void setClientUserName(String clientUserName) {
        this.clientUserName = clientUserName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getServiceRating() {
        return serviceRating;
    }

    public void setServiceRating(String serviceRating) {
        this.serviceRating = serviceRating;
    }

    public String getPricingRating() {
        return pricingRating;
    }

    public void setPricingRating(String pricingRating) {
        this.pricingRating = pricingRating;
    }

    public String getQualityRating() {
        return qualityRating;
    }

    public void setQualityRating(String qualityRating) {
        this.qualityRating = qualityRating;
    }

    public String getReviewDescription() {
        return reviewDescription;
    }

    public void setReviewDescription(String reviewDescription) {
        this.reviewDescription = reviewDescription;
    }
}
