package com.design.hrh.banjaegaandroidapp.AppController;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.design.hrh.banjaegaandroidapp.Adapters.Viewpager_adapter;
import com.design.hrh.banjaegaandroidapp.Fragments.Quality;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;
import com.design.hrh.banjaegaandroidapp.R;


public class VendorRatingNewActivity extends AppCompatActivity implements View.OnClickListener{
    private ViewPager viewPager;
     Viewpager_adapter mAdapter;
    Button next;
    LinearLayout back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_rating_new);
        init();
    }

    private void init() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        next=(Button)findViewById(R.id.next);
        back=(LinearLayout)findViewById(R.id.back);

        mAdapter = new Viewpager_adapter(getSupportFragmentManager(),this);
        viewPager.setAdapter(mAdapter);
        viewPager.setEnabled(false);

        next.setOnClickListener(this);
        back.setOnClickListener(this);viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
// Check current postion in Viewpager
                if (position == 0) {
                    back.setVisibility(View.GONE);
                } else  if (position == 1) {

                    back.setVisibility(View.VISIBLE);
                } else if (position == 3) {

                    back.setVisibility(View.VISIBLE);
                    next.setText("SUBMIT");
                } else   if (position == 4) {
                    back.setVisibility(View.GONE);
                    next.setText("NO PROBLEM");
                }else {

                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setCurrentItem (int item, boolean smoothScroll) {
        viewPager.setCurrentItem(item, smoothScroll);
    }

    @Override
    public void onClick(View v) {

        if (v == next){
            viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true);
        }if (v == back){
            viewPager.setCurrentItem(viewPager.getCurrentItem()-1, true);
            next.setText("NEXT");
        }

    }
}
