package com.design.hrh.banjaegaandroidapp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.design.hrh.banjaegaandroidapp.AppController.Architects_review;
import com.design.hrh.banjaegaandroidapp.AppController.VendorRatingNewActivity;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;
import com.design.hrh.banjaegaandroidapp.R;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;


public class Price extends Fragment {

    ScaleRatingBar scaleRatingBar;
    TextView exprence;
    public Price() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_price, container, false);

        exprence=(TextView)view.findViewById(R.id.txt_rating_header) ;

        Button button=(Button)view.findViewById(R.id.next);
        scaleRatingBar=(ScaleRatingBar)view.findViewById(R.id.rating_architecture);
        if (CSPreferences.getBoolean(getActivity(),"Projectactivity")== false) {
            exprence.setText("Experience");
        }

       scaleRatingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
           @Override
           public void onRatingChange(BaseRatingBar baseRatingBar, float v) {

               CSPreferences.putString(getActivity(),"price", String.valueOf(v));
           }
       });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //
                if (CSPreferences.getBoolean(getActivity(),"Projectactivity") == false){
                    ((VendorRatingNewActivity)getActivity()).setCurrentItem (3, true);
                }else {
                    ((VendorRatingNewActivity)getActivity()).setCurrentItem (1, true);
                }



            }
        });
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    //
}
