package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/19/2018.
 */

public class ProjectGalleryModel {

    int ProjectImage;


    public ProjectGalleryModel(int projectImage) {
        ProjectImage = projectImage;
    }

    public int getProjectImage() {
        return ProjectImage;
    }

    public void setProjectImage(int projectImage) {
        ProjectImage = projectImage;
    }
}
