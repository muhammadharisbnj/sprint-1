package com.design.hrh.banjaegaandroidapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.design.hrh.banjaegaandroidapp.Adapters.Adapter_register;
import com.design.hrh.banjaegaandroidapp.Adapters.Adapters_Nav;
import com.design.hrh.banjaegaandroidapp.AppController.DashboardNewActivity;
import com.design.hrh.banjaegaandroidapp.Model.NavScrollitems;
import com.design.hrh.banjaegaandroidapp.R;

import java.util.ArrayList;


public class Register extends Fragment {
     RecyclerView recyclerView;
    private NavScrollitems Navscrollitemes;
    private ArrayList<NavScrollitems> arraynavicons = new ArrayList<>();
    int[] myImageList = new int[]{R.drawable.whit_architect, R.drawable.whit_vendor, R.drawable.whit_builders,
            R.drawable.whit_consultants, R.drawable.whit_freequote};



    int[] myImageListcolor = new int[]{R.drawable.architect_selected, R.drawable.gold_rvendor, R.drawable.builders_selected,
            R.drawable.gold_consultant, R.drawable.gold_freequote };
    String[] imagestag = new String[]{"I am an Architect", "I am a Vendor", "I am a Client", "I am a Consultant","I am a Free Quote"};

    Adapter_register adapters_navicons;
    public Register() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_view);


        populaterecyclerviewdata();

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void populaterecyclerviewdata() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        for (int i = 0; i < myImageList.length; i++) {
            Navscrollitemes = new NavScrollitems();
            Navscrollitemes.setDrawable(myImageList[i]);
            Navscrollitemes.setImagetag(imagestag[i]);
            Navscrollitemes.setChangedrawable(myImageListcolor[i]);
            arraynavicons.add(Navscrollitemes);
        }

        adapters_navicons = new Adapter_register(getActivity(), arraynavicons);
        recyclerView.setAdapter(adapters_navicons);



        adapters_navicons.SetOnItemClickListener(new Adapter_register.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                ((DashboardNewActivity) getActivity()).drawrclose();
                if (position == 0){
                    Fragment fragment = null;
                    FragmentManager fragmentManager = getFragmentManager();
                    fragment = new I_am_art();
                    fragmentManager.beginTransaction().replace(R.id.framelayout, fragment).addToBackStack(null).commit();

                }

            }
        });
    }
}
