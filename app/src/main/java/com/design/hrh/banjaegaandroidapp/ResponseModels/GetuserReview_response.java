package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class GetuserReview_response implements Serializable
{

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("Response")
    @Expose
    private List<GetuserReview_geterseter> response = new ArrayList<GetuserReview_geterseter>();
    private final static long serialVersionUID = -6204511811305439687L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetuserReview_response() {
    }

    /**
     *
     * @param response
     * @param total
     * @param status
     */
    public GetuserReview_response(String status, Integer total, List<GetuserReview_geterseter> response) {
        super();
        this.status = status;
        this.total = total;
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<GetuserReview_geterseter> getResponse() {
        return response;
    }

    public void setResponse(List<GetuserReview_geterseter> response) {
        this.response = response;
    }

}
