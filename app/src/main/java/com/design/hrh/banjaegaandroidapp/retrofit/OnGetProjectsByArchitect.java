package com.design.hrh.banjaegaandroidapp.retrofit;

import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjects_list;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Result;

import java.util.ArrayList;
import java.util.List;

public interface OnGetProjectsByArchitect {

    public void onGetProjects(List<Result> projects);
}
