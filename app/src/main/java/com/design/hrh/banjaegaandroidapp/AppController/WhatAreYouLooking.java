package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Model.YCategoryModel;
import com.design.hrh.banjaegaandroidapp.R;

import java.util.ArrayList;

public class WhatAreYouLooking extends AppCompatActivity implements View.OnClickListener{
    RecyclerView rvYCategory;
    Activity context = WhatAreYouLooking.this;
    YCategoryAdapter mAdapter;
    ArrayList<YCategoryModel> YCategories = new ArrayList<YCategoryModel>();
    RelativeLayout rlLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_are_you_looking);
        init();
    }

    private void init() {
        rlLocation=(RelativeLayout)findViewById(R.id.rl_location);
        YCategories.add(new YCategoryModel("Architect", R.drawable.architect_selected));
        YCategories.add(new YCategoryModel("Tiles", R.drawable.tiles_selected));
        YCategories.add(new YCategoryModel("Bathrooms", R.drawable.bathrooms_selected));
        YCategories.add(new YCategoryModel("Builder", R.drawable.builders_selected));
        YCategories.add(new YCategoryModel("Kitchen", R.drawable.kitchens_selected));
        YCategories.add(new YCategoryModel("More", R.drawable.more_selected));
        rvYCategory = (RecyclerView) findViewById(R.id.rv_category_yellow);
        rvYCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new YCategoryAdapter(context, YCategories);
        rvYCategory.setAdapter(mAdapter);
        rlLocation.setOnClickListener(this);

        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void onClick(View v) {

        startActivity(new Intent(WhatAreYouLooking.this,LocationActivity.class));

    }
}
