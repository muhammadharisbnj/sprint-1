package com.design.hrh.banjaegaandroidapp.Helper;

import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.Model.ClientReviewsModel;

import java.util.ArrayList;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class DataHelper {

    public static DataHelper instance;
    public  static String IntroTag="yes";
    public ArrayList<AllArchitectsDataModel> arrayArchitecht;
    public ArrayList<ClientReviewsModel> arrayClientReviews;

    public DataHelper(){

    }

    public static DataHelper getInstance (){
        if (instance == null)
            instance = new DataHelper();
        return instance;
    }

}
