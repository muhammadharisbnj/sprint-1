package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.design.hrh.banjaegaandroidapp.Model.ClientReviewsModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MuhammadAbubakar on 3/21/2018.
 */

public class AdapterImgAndDesc extends RecyclerView.Adapter<AdapterImgAndDesc.MyViewHolder> {
    private final Context context;

    private ArrayList<ClientReviewsModel> arrayClientReview;
    AdapterImgAndDesc.OnItemClickListener mItemClickListener;

    public AdapterImgAndDesc(Context context, ArrayList<ClientReviewsModel> arrayClientReview) {
        this.context = context;
        this.arrayClientReview = arrayClientReview;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView clientImageView;
        TextView tVClientName;
        ScaleRatingBar scaleRatingBar;

        public MyViewHolder(View view) {
            super(view);
            clientImageView = view.findViewById(R.id.img_logo);
            tVClientName = view.findViewById(R.id.tv_clientName);
            scaleRatingBar = view.findViewById(R.id.rating_based_overall);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    @Override
    public AdapterImgAndDesc.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_image_description, parent, false);

        return new AdapterImgAndDesc.MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final AdapterImgAndDesc.MyViewHolder holder, int position) {

        Picasso.with(context).load(arrayClientReview.get(position).getImageUrl()).error(R.drawable.appicon).into(holder.clientImageView);
        holder.tVClientName.setText(arrayClientReview.get(position).getReviewDescription());
        float total = 0;
        if(!arrayClientReview.get(position).getPricingRating().matches(""))
            total += total + Float.parseFloat(arrayClientReview.get(position).getPricingRating());
        if(!arrayClientReview.get(position).getQualityRating().matches(""))
            total += total + Float.parseFloat(arrayClientReview.get(position).getQualityRating());
        if(!arrayClientReview.get(position).getServiceRating().matches(""))
            total += total + Float.parseFloat(arrayClientReview.get(position).getServiceRating());

        float average = total / 3;
        holder.scaleRatingBar.setRating(average);
        holder.tVClientName.setText(arrayClientReview.get(position).getClientUserName());
    }


    @Override
    public int getItemCount() {
        return arrayClientReview.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final AdapterImgAndDesc.OnItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}






