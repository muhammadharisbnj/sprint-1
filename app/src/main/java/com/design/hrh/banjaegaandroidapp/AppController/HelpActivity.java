package com.design.hrh.banjaegaandroidapp.AppController;

import android.content.Intent;
import android.os.Bundle;

import com.cuneytayyildiz.onboarder.OnboarderActivity;
import com.cuneytayyildiz.onboarder.OnboarderPage;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.SplashController.SplashActivity;

import java.util.Arrays;
import java.util.List;

public class HelpActivity extends OnboarderActivity {
    String Tag;
    List<OnboarderPage> pages;
    int i=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* Intent finishtag = getIntent();
        Tag = finishtag.getStringExtra("TAG");
*/
        pages = Arrays.asList(
                new OnboarderPage.Builder()

                        .imageResourceId(R.drawable.screen_001)
                        .backgroundColor(R.color.yellow_app_dark)

                        .title("Quickly Choose From A Category To Find Professionals & Business in your Locality.")
                        .descriptionColor(R.color.white)
                        .multilineDescriptionCentered(true)
                        .titleTextSize(20)
                        .build(),



                 new OnboarderPage.Builder()

                        .imageResourceId(R.drawable.screen_02)
                        .backgroundColor(R.color.yellow_app_dark)
                        .title("Double Tap Clusters To Reveal Multiple Professionals & Businesses")
                        .descriptionColor(R.color.white)

                        .multilineDescriptionCentered(true)
                        .titleTextSize(20)
                        
                        .build(),

                new OnboarderPage.Builder()

                        .imageResourceId(R.drawable.screen_03)
                        .backgroundColor(R.color.yellow_app)
                        .title("Directly Call Or Message Professionals From The App Rate Their Products & Services")
                        .descriptionColor(R.color.white)
                        .multilineDescriptionCentered(true)
                        .titleTextSize(20)

                        .build(),
                new OnboarderPage.Builder()

                        .imageResourceId(R.drawable.screen_04)
                        .backgroundColor(R.color.yellow_app)
                        .title("At Any Time You Can Call Banjaiga's Team of Experts For Free Advice Consultation And Amazing Discount Offers On What You Are Looking For.")
                        .descriptionColor(R.color.white)
                        .multilineDescriptionCentered(true)
                        .titleTextSize(20)
                        .build()
        );

        initOnboardingPages(pages);


    }


    @Override
    public void onFinishButtonPressed() {
        if (DataHelper.IntroTag.equalsIgnoreCase("yes")) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onSkipButtonPressed() {
        // Optional: by default it skips onboarder to the end
       // super.onSkipButtonPressed();
        if (DataHelper.IntroTag.equalsIgnoreCase("yes")) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            finish();
        }




    }
}

