package com.design.hrh.banjaegaandroidapp.retrofit;

import android.content.Context;
import android.util.Log;

import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjects_list;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjectsall_response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Apicall {

    ApiInterface apiInterface;
    Context context;
    Gson gson;
    private String from;
    private String baseUrl;
    public Apicall(Context context) {
        this.context = context;
       // customProgressDialog = new CustomProgressDialog(context);
        this.from = from;


        baseUrl = MyHelper.BASE_URL;
        gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiInterface = retrofit.create(ApiInterface.class);

    }


    public void getEventsList(final Ongetarchitects ongetarchitects,String page) {
        try {
            final Ongetarchitects mOnGetEvents=ongetarchitects;
          //  customProgressDialog.show();
            Call<GetProjectsall_response> reviewResponseCall = apiInterface.getProjectsall(page);
            reviewResponseCall.enqueue(new Callback<GetProjectsall_response>() {
                @Override
                public void onResponse(Call<GetProjectsall_response> call, Response<GetProjectsall_response> response) {
                  //  customProgressDialog.dismiss();


                    if (response.body()!=null){
                        Log.d("redkmcvlkdmkcl", String.valueOf(response));
                        if (response.body().getStatus().equals("200")) {
                            ongetarchitects.Ongetarchitects((ArrayList<GetProjects_list>) response.body().getResult());

                        }
                        else{

                        }

                    }
                    else {
                     /*   if (customProgressDialog.isShowing())
                            customProgressDialog.dismiss();*/
                    }
                }

                @Override
                public void onFailure(Call<GetProjectsall_response> call, Throwable t) {
                   /* if (customProgressDialog.isShowing())
                        customProgressDialog.dismiss();
                    t.printStackTrace();
                    Utils.showToast("Poor Connection.", context);*/
                    Log.d("", "onFailure: " + t.getMessage());
                }
            });

        } catch (NullPointerException e) {
            System.err.println("Unknown Error !");
        }
    }



}
