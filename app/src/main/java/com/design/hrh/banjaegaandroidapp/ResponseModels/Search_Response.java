package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search_Response {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("total_pages")
    @Expose
    private String totalPages;
    @SerializedName("result")
    @Expose
    private List<Search_model_category> result = null;

    public Search_Response(Integer status, String totalPages, List<Search_model_category> result) {
        this.status = status;
        this.totalPages = totalPages;
        this.result = result;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public List<Search_model_category> getResult() {
        return result;
    }

    public void setResult(List<Search_model_category> result) {
        this.result = result;
    }

    // REsult
}
