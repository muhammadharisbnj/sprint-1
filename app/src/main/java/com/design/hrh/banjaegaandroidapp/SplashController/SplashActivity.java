package com.design.hrh.banjaegaandroidapp.SplashController;


import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.crashlytics.android.Crashlytics;
import com.design.hrh.banjaegaandroidapp.AppController.DashboardNewActivity;

import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.Utills.GPSTracker;
import com.design.hrh.banjaegaandroidapp.Utills.RuntimePermissionsActivity;
import com.google.maps.android.clustering.ClusterManager;

import io.fabric.sdk.android.Fabric;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SplashActivity extends RuntimePermissionsActivity {
    final private int REQUEST_PERMISSIONS = 20;
    int duration = 6000; // milliseconds
    public static String activityName;
    private static final String TAG = DashboardNewActivity.class.getSimpleName();

    double latitude = 33.7205829, longitude = 73.0736015;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    HashMap<String, String> searchallparams = new HashMap<String, String>();

    public static String getActivityName() {
        return activityName;
    }

    public static void setActivityName(String activity) {
        activityName = activity;
    }

    private ClusterManager<DashboardNewActivity.StringClusterItem> mClusterManager;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    DataHelper dataHelper;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Fabric.with(this, new Crashlytics());
      /*  prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        if (!prefs.getBoolean("isFirstTimeintro", false)) {
            Intent helpintent=new Intent(SplashActivity.this,HelpActivity.class);
            helpintent.putExtra("TAG","No");
            startActivity(helpintent);
          //  editor.putBoolean("isFirstTimeintro", true).commit();

        }*/


        SplashActivity.super.requestAppPermissions(new
                        String[]{
                        Manifest.permission.INTERNET,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,

                }, R.string.runtime_permissions_txt
                , REQUEST_PERMISSIONS);


        gps = new GPSTracker(this);
            if (gps.canGetLocation()) {
                gps = new GPSTracker(this);
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
            } else {
                latitude = 33.7205829;
                longitude = 73.0736015;
                gps.showSettingsAlert();
            }


            initVolleyCallback();

            dataHelper = DataHelper.getInstance();
            mVolleyService = new VolleyService(mResultCallback, this);
            if (latitude == 0.0)
                latitude = 33.7205829;
            longitude = 73.0736015;


            searchallparams.put("lat",String.valueOf(latitude));
            searchallparams.put("long",String.valueOf(longitude));
            mVolleyService.getDataVolley("search",searchallparams);
            JSONObject sendObj = null;


//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        startActivity(new Intent(SplashActivity.this, MapsParent.class));
//                        finish();
//                    }
//                }, duration);
//            }


            }




    @Override
    public void onPermissionsGranted(int requestCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dataHelper.arrayArchitecht != null) {
//                            startActivity(new Intent(SplashActivity.this, DashboardNewActivity.class));
//                            finish();

                        }
                    }
                }, duration);
            }
        });
    }


    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Progressloader.hideloader();
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySuccess(String requestType, JSONArray response) {
//                Progressloader.hideloader();
                onResponseFinished(response);
                startActivity(new Intent(SplashActivity.this, DashboardNewActivity.class));
                finish();
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
//                startActivity(new Intent(SplashActivity.this, MapsParent.class));
//                finish();
/*
               Progressloader.hideloader();
               startActivity(new Intent(SplashActivity.this,MapsParent.class));
*/
                 if (error instanceof NetworkError) {

                    Toast.makeText(SplashActivity.this,
                            "Please check your internet connection and try again",
                            Toast.LENGTH_LONG).show();

                    //TODO
                } else if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(SplashActivity.this,
                            "Something went wrong please try again",
                            Toast.LENGTH_LONG).show();
                }


                   }

            @Override
            public void notifySucessEmail(String servicename, JSONObject response) {

            }

            @Override
            public void notifyLocationsucess(String servicename, JSONObject response) {

            }

            @Override
            public void notifyClientReviews(String servicename, JSONObject response) {

            }

            @Override
            public void notifyRatingSuccess(String servicename, JSONObject response) {

            }
        };
    }

    private void onResponseFinished(JSONArray response) {
        try {
            if (dataHelper.arrayArchitecht == null) {
                dataHelper.arrayArchitecht = new ArrayList<>();
            }
            if (response.length() > 0) {
                dataHelper.arrayArchitecht.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);
                    AllArchitectsDataModel dataModel = new AllArchitectsDataModel(SplashActivity.this, jsonObject);
                    dataHelper.arrayArchitecht.add(dataModel);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

}
