package com.design.hrh.banjaegaandroidapp.retrofit;

import com.design.hrh.banjaegaandroidapp.ResponseModels.GetArchitects_term_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjectsall_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetuserReview_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Saveuserrating_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Search_Response;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("search")
    Call<Search_Response> search (@Field("lat") Double lat, @Field("long_") Double long_, @Field("page") int page, @Field("rating_sorting") int rating_sorting, @Field("sort_order") int sort_order, @Field("term") String term);

    @FormUrlEncoded
    @POST("search")
    Call<Search_Response> search_category(@Field("category") String category);

    @FormUrlEncoded
    @POST("get_architects")
    Call<GetArchitects_term_response> get_architects_userid(@Field("user_id") String user_id);


    @GET("getUserReviews/24966")
    Call<GetuserReview_response> getuserreview();

    @FormUrlEncoded
    @POST("getProjects")
    Call<GetProjectsall_response> getProjectsall(@Field("Page")String page );

    @FormUrlEncoded
    @POST("getProjects")
    Call<GetProjectsall_response> getprojectresponse(@Field("user_id")String user_id );


    @FormUrlEncoded
    @POST("saveUserRating")
    Call<Saveuserrating_response> getprojectresponse(@Field("user_id")String user_id ,@Field("username")String username,@Field("email")String email,@Field("type")String type,@Field("image_url")String image_url,@Field("service")String service,@Field("pricing")String pricing,@Field("quality")String quality,@Field("reviews")String reviews);



}
