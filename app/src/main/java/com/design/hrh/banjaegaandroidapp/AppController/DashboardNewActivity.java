package com.design.hrh.banjaegaandroidapp.AppController;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.design.hrh.banjaegaandroidapp.Adapters.AdapterSearchNames;
import com.design.hrh.banjaegaandroidapp.Adapters.Adapters_Nav;
import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Fragments.History;
import com.design.hrh.banjaegaandroidapp.Fragments.Register;
import com.design.hrh.banjaegaandroidapp.Fragments.Setting;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.Model.NavScrollitems;
import com.design.hrh.banjaegaandroidapp.Model.YCategoryModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.Utills.CustomClusterRenderer;
import com.design.hrh.banjaegaandroidapp.Utills.GPSTracker;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.google.android.gms.common.zzp.isGooglePlayServicesAvailable;

public class DashboardNewActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, LocationSource.OnLocationChangedListener, View.OnClickListener, ClusterManager.OnClusterClickListener, ClusterManager.OnClusterItemClickListener , YCategoryAdapter.OnItemClickListener  {

    TabLayout tableLayout;
    ViewPager viewPager;
    RecyclerView rvYCategory;
    Activity context = DashboardNewActivity.this;
    YCategoryAdapter mAdapter;
    ArrayList<YCategoryModel> YCategories = new ArrayList<YCategoryModel>();

    //Location and Map
    private static LatLng LOCATIONS_POINTS = null;
    private GoogleMap mMap;
    private ClusterManager<DashboardNewActivity.StringClusterItem> mClusterManager;
    DataHelper dataHelper;
    private boolean isFoundArch = false;
    double currentLatitude, currentLongitude;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    GPSTracker gpsTracker;
    private static final String TAG = DashboardNewActivity.class.getSimpleName();
    double latitude = 33.7205829, longitude = 73.0736015, searchLatitude, searchLongitude, vendorLatitude, vendorLongitude;
    HashMap<String, String> requestCall = new HashMap<String, String>();




    CardView contactInfo, cardEmailCall, cardupdateloc;
    RelativeLayout cardProfileInfo;
    TextView title, tvHeading, tvAddressName, tvOptionDesc, btnRateMe, tvcontactus, tvReviews, tvOptionDescdialog;
    ImageView canceCard, cancelArchView, cancelupdatefrom, cancelupdatefromdialog, cancelcardemaildialog;
    Button buttonSend, buttonSendVendorloc, buttonSendVendorlocdialog, buttonSenddialog, buttonsearch;
    EditText etUsername, etContact, etVendorName, etVendorContact, etvendorLocation,
            etVendorNamedialog, etVendorContactdialog, etvendorLocationdialog, etUsernamedialog, etContactdialog;
    ImageView ivThumbs, ivHome, ivCurtain, ivManGrey, ivFurniture, ivBath, ivNearBy, ivKitchen, ivCall, ivMsg, ivAdminCall, ivAdminMessage,
            ivCardHome, ivCardInterior, ivCardArch, ivCardCal, ivCardHomedialog, ivCardInteriordialog, ivCardArchdialog, ivCardCaldialog, ivNext,
            ivBack, ivVerifiedBadge, ivMedalBadge, ivMobileBadge, ivEmailBadge, ivpophelpcancel;


    int viewWidth, mWidth, PLACE_AUTOCOMPLETE_REQUEST_CODE = 1, PLACE_AUTOCOMPLETE_REQUEST_CODE1 = 2, PLACE_AUTOCOMPLETE_REQUEST_CODE2 = 3;
    String ImgTag = "", Usertype = "", phoneNum, bitmapString, Showhidekeyboard = "yes";

    LinearLayout footerLayout;
    RadioButton emailView, archProfileView, updateloc;

    AutoCompleteTextView autoCompleteTextView, autoCompleteTextViewLoc;

    CircleImageView circleImageView;
    String EMAILTAG;

    RecyclerView recyclerView;
    Adapters_Nav adapters_navicons;

    private ArrayList<NavScrollitems> arraynavicons = new ArrayList<>();
    private ArrayList<String> autocompletearray = new ArrayList<String>();
    private NavScrollitems Navscrollitemes;
    RadioGroup radioGroup;
//
    DrawerLayout drawer;
    Toolbar toolbar;
    NavigationView navigationView;
    AlertDialog alertDialog;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ScaleRatingBar Ratingbasedprofile;
    LinearLayout llNavMenu, llArchitechts, llKitchen, llFurniture, llBathrooms, llTiles, llProjects, llLight, llInterior;
    RelativeLayout popupHelp;

    Button call,message;
    // Top menu default images,change on click images and tag arrays

//    int[] myImageList = new int[]{R.drawable.man_grey, R.drawable.img_kitchen, R.drawable.furniture_grey,
//            R.drawable.bath_grey, R.drawable.tiles_grey, R.drawable.home_grey, R.drawable.lights_grey};

    int[] myImageList = new int[]{R.drawable.architect_grey, R.drawable.tiles, R.drawable.bathrooms_grey,
            R.drawable.builders_grey, R.drawable.kitchens_grey , R.drawable.more};



    int[] myImageListcolor = new int[]{R.drawable.architect_selected, R.drawable.tiles_selected, R.drawable.bathrooms_selected,
            R.drawable.builders_selected, R.drawable.kitchens_selected , R.drawable.more_selected};
    String[] imagestag = new String[]{"Architects_lat_lang", "Kitchens", "Furniture", "Bathrooms", "Tiles", "More"};
    HashMap<String, String> searchparams = new HashMap<String, String>();

    android.app.AlertDialog alertDialogRequestCall;





    CircleImageView logo;
    TextView name,contry;
    ScaleRatingBar ratingBar;


           ImageView cancal;
EditText search;

ImageView close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_new);
        getGpsLocation();
        init();

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();
        prefs.getBoolean("isFirstTime", false);
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            gpsTracker = new GPSTracker(this);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

           /* searchLatitude = gpsTracker.getLatitude();
            searchLongitude = gpsTracker.getLongitude();
*/

        } else {
            gpsTracker.showSettingsAlert();
        }

        adjustKeyoardSettings();
        initalizeControls();
        settingNavigationdrawer();
        initVolleyCallback();

        //  Recyclervha
        // iew  top menu
        populaterecyclerviewdata();


        dataHelper = DataHelper.getInstance();
        if (savedInstanceState == null) {
            setupMapFragment();
        }

        onClickCalls();

        changelatlongtoaddress(latitude, longitude, "etvendor");

        contactInfo.setVisibility(View.GONE);
        mVolleyService = new VolleyService(mResultCallback, this);
        JSONObject sendObj = null;
        Display display = getWindowManager().getDefaultDisplay();
        mWidth = display.getWidth(); // deprecated
        viewWidth = mWidth / 3;
//        onPopUpHelpClick();
    }


    public void getGpsLocation()
    {
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            gpsTracker = new GPSTracker(this);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
           /* searchLatitude = gpsTracker.getLatitude();
            searchLongitude = gpsTracker.getLongitude();
*/
        } else {
            gpsTracker.showSettingsAlert();
        }
    }


    private void getData()
    {
        mVolleyService = new VolleyService(mResultCallback, this);
        JSONObject sendObj = null;
    }



    private void init() {
        cancal=(ImageView)findViewById(R.id.imageView2);
        rvYCategory=(RecyclerView)findViewById(R.id.rv);
        rvYCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        YCategories.add(new YCategoryModel("Architect", R.drawable.architect_grey));
        YCategories.add(new YCategoryModel("Tiles", R.drawable.tiles));
        YCategories.add(new YCategoryModel("Bathrooms", R.drawable.bathrooms_grey));
        YCategories.add(new YCategoryModel("Builder", R.drawable.builders_grey));
        YCategories.add(new YCategoryModel("Kitchen", R.drawable.kitchens_grey));
        YCategories.add(new YCategoryModel("More", R.drawable.more));
        mAdapter = new YCategoryAdapter(context, YCategories);
        mAdapter.SetOnItemClickListener(this);
        rvYCategory.setAdapter(mAdapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initializationid();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
Button button=(Button)toolbar.findViewById(R.id.buttion);
        search=(EditText)toolbar.findViewById(R.id.search);
        // Navigation Drawerlayout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        cancal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            drawrclose();
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (search.getText().toString().length()==3){
                    Progressloader.loader(DashboardNewActivity.this);
                    mVolleyService.getDataVolley("search", searchparamsmethod(search.getText().toString()));
                }



            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void drawrclose(){
        drawer.closeDrawer(GravityCompat.START);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // set Id in tab and viewpager
    private void initializationid() {
        tableLayout = (TabLayout) findViewById(R.id.tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewPager);
        tableLayout.setupWithViewPager(viewPager);

        // Divider line in tab
        LinearLayout linearLayout = (LinearLayout) tableLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.WHITE);
        drawable.setSize(2, 1);
        linearLayout.setDividerPadding(0);
        linearLayout.setDividerDrawable(drawable);
    }

    // add fragment
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new History(), "History");
        adapter.addFragment(new Register(), "Register");
        adapter.addFragment(new Setting(), "Setting");
        viewPager.setAdapter(adapter);
    }
    AllArchitectsDataModel userData;
    @Override
    public void onClick(View v) {

        if (v == ivCall){
        if (userData.getPhoneNumberONE()!=null){
            MyHelper.makeCall(this,userData.getPhoneNumberONE());
        }
        }

        if (v == ivMsg){
            MyHelper.sendSMS(this,userData.getIsMobile());
        }if (v == cancal){
            emailView.setChecked(true); // default selection of radio button ist one
            cardProfileInfo.setVisibility(View.GONE);
            cardupdateloc.setVisibility(View.GONE);
            contactInfo.setVisibility(View.GONE);
            radioGroup.setVisibility(View.GONE);
            tvcontactus.setVisibility(View.GONE);

            clearcardstodefault();

        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
//        mMap.setMyLocationEnabled(true);
        mClusterManager = new ClusterManager<>(this, mMap);
        mMap.setOnCameraChangeListener(mClusterManager);
        if (dataHelper != null && dataHelper.arrayArchitecht != null && dataHelper.arrayArchitecht.size() > 0) {
            isFoundArch = true;
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                AllArchitectsDataModel arcData = dataHelper.arrayArchitecht.get(i);

                Double lat = 0.0;
                Double longitude = 0.0;
//                autocompletearray.add(dataHelper.arrayArchitecht.get(i).getUserName());

                if (!arcData.getLatitude().matches(""))
                    lat = Double.valueOf(arcData.getLatitude());
                if (!arcData.getLongitude().matches(""))
                    longitude = Double.valueOf(arcData.getLongitude());
                LOCATIONS_POINTS = new LatLng(lat, longitude);
                mClusterManager.addItem(new DashboardNewActivity.StringClusterItem("Marker #" + (i + 1), LOCATIONS_POINTS, i,arcData.getMembershipType(),arcData.getUser_type()));
            }
        } else {
            isFoundArch = false;
        }
        mClusterManager.cluster();


        // Setting autocompelte keyword suggestions

//        Autocompletetextdata();


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                ClearSearchview();

            }
        });


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (isFoundArch)
                    //    mMap.animateCamera(CameraUpdateFactory.newLatLng(LOCATIONS_POINTS));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATIONS_POINTS, 11f));
            }
        });


        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterClickListener(this);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getPosition() != null) {
                     userData = getUserData(marker.getPosition());
                    if (userData != null) {
                        showMarkerDetail(userData);
                    }
                }

                if (emailView.isChecked()) {
                    contactInfo.setVisibility(View.GONE);
                   cardProfileInfo.setVisibility(View.VISIBLE);

                } else if (archProfileView.isChecked()) {
                    cardProfileInfo.setVisibility(View.GONE);
                    contactInfo.setVisibility(View.VISIBLE);


                }if (footerLayout.getVisibility() == View.GONE || footerLayout.getVisibility() == View.VISIBLE) {
                    radioGroup.setVisibility(View.VISIBLE);
                    tvcontactus.setVisibility(View.VISIBLE);
               }

                return false;
            }
        });
    }



    @Override
    public boolean onClusterClick(Cluster cluster) {


        return true;
    }

    @Override
    public boolean onClusterItemClick(ClusterItem clusterItem) {
        return false;
    }



    @Override
    public void onItemClick(View view, int position) {

        if(position==0)
        {
            startActivity(new Intent(context,ArchitectsActivity.class));
        }
        else
        {
            startActivity(new Intent(context,VendorOnMap.class));
        }



    }

    // View pager adapter
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }







    private void adjustKeyoardSettings() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }


    // Inti Iddddddddddddddd
    private void initalizeControls() {

        recyclerView = (RecyclerView) findViewById(R.id.rv);



        ivVerifiedBadge = (ImageView) findViewById(R.id.iv_verified);
        ivMedalBadge = (ImageView) findViewById(R.id.badge_star);
        ivMobileBadge = (ImageView) findViewById(R.id.tv_phone_verified);
        ivEmailBadge = (ImageView) findViewById(R.id.email_verified_badge);
        ivpophelpcancel = (ImageView) findViewById(R.id.cancel_image_popup);

        llNavMenu = (LinearLayout) findViewById(R.id.top_navigation_menu);
        llArchitechts = (LinearLayout) findViewById(R.id.architechts);
        llKitchen = (LinearLayout) findViewById(R.id.kitchens);
        llFurniture = (LinearLayout) findViewById(R.id.furniture);
        llBathrooms = (LinearLayout) findViewById(R.id.bathrooms);
        llTiles = (LinearLayout) findViewById(R.id.tiles);
        llProjects = (LinearLayout) findViewById(R.id.projects);
        llLight = (LinearLayout) findViewById(R.id.light);
        llInterior = (LinearLayout) findViewById(R.id.interior);

        popupHelp = (RelativeLayout) findViewById(R.id.popup_help);
        contactInfo = (CardView) findViewById(R.id.card_contact_form);
        btnRateMe = (TextView) findViewById(R.id.btn_rate_me);
        cardProfileInfo = (RelativeLayout) findViewById(R.id.vinder_layout);
        cardEmailCall = (CardView) findViewById(R.id.card_email_callform);
        cardupdateloc = (CardView) findViewById(R.id.card_update_location);
        title = (TextView) findViewById(R.id.tv_title);
        canceCard = (ImageView) findViewById(R.id.cancel_image);
        cancelArchView = (ImageView) findViewById(R.id.arch_view_cancel);
        cancelupdatefrom = (ImageView) findViewById(R.id.cancel_updateform);
        buttonSend = (Button) findViewById(R.id.btnSend);
        buttonsearch = (Button) findViewById(R.id.btn_search);
        buttonSendVendorloc = (Button) findViewById(R.id.btn_updateloc);
        ivThumbs = (ImageView) findViewById(R.id.iv_thumbs);
        ivCall = (ImageView) findViewById(R.id.img_call);
        tvHeading = (TextView) findViewById(R.id.tvHeading);
        tvcontactus = (TextView) findViewById(R.id.txt_contactus);
        tvOptionDesc = (TextView) findViewById(R.id.tv_option_desc);
        tvReviews = (TextView) findViewById(R.id.tv_reviews);
        tvAddressName = (TextView) findViewById(R.id.tvaddress_name);
        ivMsg = (ImageView) findViewById(R.id.img_msg);

        circleImageView = (CircleImageView) findViewById(R.id.img_logo);



        ivNearBy = (ImageView) findViewById(R.id.img_near_by);
        ivCardHome = (ImageView) findViewById(R.id.img_card_home);
        ivCardInterior = (ImageView) findViewById(R.id.img_card_interior);
        ivCardArch = (ImageView) findViewById(R.id.img_card_arch);
        ivCardCal = (ImageView) findViewById(R.id.img_card_cal);
        radioGroup = (RadioGroup) findViewById(R.id.radio_btngroup);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        Ratingbasedprofile = (ScaleRatingBar) findViewById(R.id.baseratingbar_main);

        ivAdminCall = (ImageView) findViewById(R.id.iv_admin_call);
        ivAdminMessage = (ImageView) findViewById(R.id.iv_admin_message);
        emailView = (RadioButton) findViewById(R.id.emailViewRb);
        updateloc = (RadioButton) findViewById(R.id.thirdView);
        etUsername = (EditText) findViewById(R.id.et_name);
        etContact = (EditText) findViewById(R.id.et_contact);
        etVendorName = (EditText) findViewById(R.id.et_vendorname);
        etVendorContact = (EditText) findViewById(R.id.et_vendorcontact);
        etvendorLocation = (EditText) findViewById(R.id.et_vendor_loc);
        etvendorLocation.setFocusable(false);
        archProfileView = (RadioButton) findViewById(R.id.profileView);
        footerLayout = (LinearLayout) findViewById(R.id.footer);
        ivNext = (ImageView) findViewById(R.id.img_next);
        ivBack = (ImageView) findViewById(R.id.img_back);

        autoCompleteTextViewLoc = (AutoCompleteTextView) findViewById(R.id.search_by_text);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.search_by_tags);

        ivCall.setOnClickListener(this);
        ivMsg.setOnClickListener(this);
        buttonsearch.setOnClickListener(this);

        ivCardHome.setOnClickListener(this);
        ivCardInterior.setOnClickListener(this);
        ivCardArch.setOnClickListener(this);
        ivCardCal.setOnClickListener(this);

        ivNearBy.setOnClickListener(this);
        ivNext.setOnClickListener(this); // top muenu next button click event
        ivBack.setOnClickListener(this); // top muenu next button click event

//        if (!prefs.getBoolean("isFirstTime", false)) {
//            llNavMenu.setVisibility(View.GONE);
//            popupHelp.setVisibility(View.VISIBLE);
//        } else {
//            llNavMenu.setVisibility(View.VISIBLE);
//            popupHelp.setVisibility(View.GONE);
//        }


        logo=(CircleImageView)findViewById(R.id.logo);
        name=(TextView)findViewById(R.id.txt_vendor_name);
        contry=(TextView)findViewById(R.id.contry);
        cancal=(ImageView)findViewById(R.id.imageView5);
        cancal.setOnClickListener(this);

    }

    private void settingNavigationdrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {

                Progressloader.hideloader();
                if (requestType.matches("requestcall")) {
                    try {
                        Toast.makeText(getApplicationContext(), response.getString("Message"), Toast.LENGTH_SHORT).show();
                        alertDialogRequestCall.cancel();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void notifySuccess(String requestType, JSONArray response) {
                Progressloader.hideloader();
                onResponseFinished(response);

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySucessEmail(String requestType, JSONObject response) {
                Progressloader.hideloader();
                try {

                    Toast.makeText(DashboardNewActivity.this, response.getString("Message"), Toast.LENGTH_SHORT).show();
                    etContact.setText("");
                    etUsername.setText("");
                    if (etUsernamedialog != null && etContactdialog != null) {
                        etContactdialog.setText("");
                        etUsernamedialog.setText("");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);

            }

            @Override
            public void notifyRatingSuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifyLocationsucess(String requestType, JSONObject response) {
                try {

                    Progressloader.hideloader();
                    Toast.makeText(DashboardNewActivity.this, response.getString("Message"), Toast.LENGTH_SHORT).show();
                    // radioGroup.setVisibility(View.GONE);
                    etVendorName.setText("");
                    etVendorContact.setText("");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyClientReviews(String requestType, JSONObject response) {

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

                Progressloader.hideloader();

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + "That didn't work!");

                if (error instanceof NetworkError) {

                    Toast.makeText(DashboardNewActivity.this,
                            "Please check your internet connection and try again",
                            Toast.LENGTH_LONG).show();

                    //TOimg_msgDO
                } else if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(DashboardNewActivity.this,
                            "Something went wrong please try again",
                            Toast.LENGTH_LONG).show();
                }


            }


        };
    }

    public static class StringClusterItem implements ClusterItem {
        public final String title;
        public final String type;
        public final String usertype;
        public final LatLng latLng;
        public final int tag;

        public StringClusterItem(String title, LatLng latLng, int tag,String type,String usertype) {
            this.title = title;
            this.latLng = latLng;
            this.tag = tag;
            this.type = type;
            this.usertype = usertype;
        }

        @Override
        public LatLng getPosition() {
            return latLng;
        }
    }

    private void populaterecyclerviewdata() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        for (int i = 0; i < myImageList.length; i++) {
            Navscrollitemes = new NavScrollitems();
            Navscrollitemes.setDrawable(myImageList[i]);
            Navscrollitemes.setImagetag(imagestag[i]);
            Navscrollitemes.setChangedrawable(myImageListcolor[i]);
            arraynavicons.add(Navscrollitemes);
        }

        adapters_navicons = new Adapters_Nav(DashboardNewActivity.this, arraynavicons);
        recyclerView.setAdapter(adapters_navicons);



        adapters_navicons.SetOnItemClickListener(new Adapters_Nav.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // adapters_navicons.changeimage(position);
                ImgTag = arraynavicons.get(position).getImagetag();
                ClearSearchview();
                keyboardDown(view);
               // autoCompleteTextView.setText(ImgTag);

                if (position == 0){
                    startActivity(new Intent(DashboardNewActivity.this,ArchitectsActivity.class));
                }else if (position == 1){

                    startActivity(new Intent(DashboardNewActivity.this,VendorOnMap.class));
                }else if (position == 5){
                    startActivity(new Intent(DashboardNewActivity.this,CategoryActivity.class));

                }
                if (ImgTag.equalsIgnoreCase("Architects_lat_lang")) {

                    // Clearing searchview hint and keyboard down on every top menu click
//                    Progressloader.loader(MapsParent.this);
                    //    mVolleyService.getDataVolley("GETCALL", "archlocation");

//                    mVolleyService.getDataVolley("get_architects", searchparams_architects());


                }
                if (ImgTag.equalsIgnoreCase("interior")) {
                   // Progressloader.loader(DashboardNewActivity.this);
                    //mVolleyService.getDataVolley("GETCALL", "category&id=108&lat=" + latitude + "&long=" + longitude);
                    //  mVolleyService.getDataVolley("search", searchparamsmethod("108"));
                //    mVolleyService.getDataVolley("search", searchparamsmethod("interior"));



                }
                if (ImgTag.equalsIgnoreCase("tiles")) {
                    Progressloader.loader(DashboardNewActivity.this);
                    // mVolleyService.getDataVolley("GETCALL", "tiles");
                    //  mVolleyService.getDataVolley("search", "tiles&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("tiles"));


                }
                if (ImgTag.equalsIgnoreCase("kitchens")) {
                    Progressloader.loader(DashboardNewActivity.this);
                    //mVolleyService.getDataVolley("search", "category&id=5&lat=" + latitude + "&long=" + longitude);

                    mVolleyService.getDataVolley("search", searchparamsmethod("kitchens"));

                }
                if (ImgTag.equalsIgnoreCase("furniture")) {
                    Progressloader.loader(DashboardNewActivity.this);
                    //mVolleyService.getDataVolley("search", "category&id=15&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("furniture"));

                }
                if (ImgTag.equalsIgnoreCase("Bathrooms")) {
                    Progressloader.loader(DashboardNewActivity.this);
                    // mVolleyService.getDataVolley("search", "category&id=6&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("bathrooms"));

                }

                if (ImgTag.equalsIgnoreCase("lights")) {
                    Progressloader.loader(DashboardNewActivity.this);

                    //   mVolleyService.getDataVolley("GETCALL", "category&id=148&lat=" + latitude + "&long=" + longitude);
                    mVolleyService.getDataVolley("search", searchparamsmethod("lights"));

                }
                if (ImgTag.equalsIgnoreCase("More")) {
//                    Progressloader.loader(MapsParent.this);

                    //   mVolleyService.getDataVolley("GETCALL", "category&id=148&lat=" + latitude + "&long=" + longitude);
//                    mVolleyService.getDataVolley("search", searchparamsmethod("lights"));

                }

            }
        });
    }
    private void ClearSearchview() {

        // above function is searchview clear hint and for hiding keyboard

        //  autoCompleteTextView.setHint("Enter keyword");
        autoCompleteTextViewLoc.setText("");
        autoCompleteTextViewLoc.clearFocus();
        autoCompleteTextView.clearFocus();
        autoCompleteTextViewLoc.setFocusable(false);
        emailView.setChecked(true);
        contactInfo.setVisibility(View.GONE);
        cardupdateloc.setVisibility(View.GONE);
        cardProfileInfo.setVisibility(View.GONE);


        // default set value email sending card and  updatelocation card
//        clearcardstodefault();


    }
    private void setupMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setRetainInstance(true);
        mapFragment.getMapAsync(this);
    }

    void changelatlongtoaddress(double latitude, double longitude, String ettoselect) {
        List<Address> addresses;
        Geocoder geoCoder = new Geocoder(DashboardNewActivity.this);
        try {

            addresses = geoCoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                etvendorLocation.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
                if (ettoselect.equalsIgnoreCase("etvendor")) {
                    etvendorLocation.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
                } else {
                    etvendorLocationdialog.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
                }
            }
        } catch (IOException e) { // TODO Auto-generated catch block
            e.printStackTrace();
        }

       /* pos = new LatLng(latitude, longtitude);
        googleMap.addMarker(new MarkerOptions().icon(
                BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(pos));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);*/
    }

    private void onClickCalls() {
        canceCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true);  // default selection of radio button ist one
                contactInfo.setVisibility(View.GONE);
                cardProfileInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                tvcontactus.setVisibility(View.GONE);

                //setting  default values of email and updatelocation card
                clearcardstodefault();
            }
        });

        cancelArchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true); // default selection of radio button ist one
                cardProfileInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                contactInfo.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                tvcontactus.setVisibility(View.GONE);

                clearcardstodefault();
            }
        });

        cancelupdatefrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true); // default selection of radio button ist one
                cardProfileInfo.setVisibility(View.GONE);
                contactInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                tvcontactus.setVisibility(View.GONE);

                clearcardstodefault();
            }
        });

        ivAdminCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall("03041116663");
            }
        });
        ivpophelpcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llNavMenu.setVisibility(View.VISIBLE);
                popupHelp.setVisibility(View.GONE);
            }
        });


        ivAdminMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS("03000801546");
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etContact.getText().length() == 0 || etUsername.getText().length() == 0) {
                    Toast.makeText(DashboardNewActivity.this, "Please enter your information", Toast.LENGTH_SHORT).show();
                } else {
                    ivThumbs.setVisibility(View.VISIBLE);

                    Progressloader.loader(DashboardNewActivity.this);

                    mVolleyService.get_sendemailDataVolley("memberInquiry", Become_memberparams(etUsername.getText().toString(), etContact.getText().toString(), EMAILTAG));

                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dataHelper.arrayArchitecht != null) {
                            ivThumbs.setVisibility(View.GONE);
                        }
                    }
                }, 500);
            }
        });

        emailView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.VISIBLE);
                        contactInfo.setVisibility(View.GONE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.GONE);
                    }
                }
            }
        });

        updateloc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.GONE);
                        contactInfo.setVisibility(View.GONE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        archProfileView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.GONE);
                        contactInfo.setVisibility(View.VISIBLE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.GONE);
                    }
                }
            }
        });

      /*  btnRateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToArchticechClass();
            }
        });*/

        buttonSendVendorloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etVendorName.getText().length() == 0 || etVendorContact.getText().length() == 0 || etvendorLocation.getText().length() == 0) {
                    Toast.makeText(DashboardNewActivity.this, "Please enter complete information", Toast.LENGTH_SHORT).show();
                } else {
                    Progressloader.loader(DashboardNewActivity.this);
                    mVolleyService.updateUserLocation("GetCall", "https://www.banjaiga.com/web_services/index.php/updateLocation/"
                            + tvHeading.getText().toString() + "/" + Usertype + "/" + etVendorName.getText().toString() + "/"
                            + etVendorContact.getText().toString() + "/" + vendorLatitude + "/" + vendorLongitude
                    );
                }

            }
        });

        etvendorLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showplacefragment(PLACE_AUTOCOMPLETE_REQUEST_CODE1);
            }
        });

        autoCompleteTextViewLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearSearchview();
                showplacefragment(PLACE_AUTOCOMPLETE_REQUEST_CODE);

            }


        });


       /* autoCompleteTextViewLoc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    return true;
                }
                return false;
            }
        });*/



        /*autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "search&search=" + autoCompleteTextView.getText().toString() + "&lat=" + "" + "&long=" + "");
                   ClearSearchview();
                    return true;
                }
                return false;
            }
        });
*/

        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearSearchview();

                autoCompleteTextView.setFocusableInTouchMode(true);
                autoCompleteTextView.requestFocus();

            }
        });

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                Progressloader.loader(DashboardNewActivity.this);
                String query = autoCompleteTextView.getText().toString();

                searchparams.put("term", query);
                searchparams.put("lat", String.valueOf(latitude));
                searchparams.put("long", String.valueOf(longitude));
                mVolleyService.getDataVolley("search", searchparams);
                keyboardDown(arg1);
            }

        });

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {
                    adapters_navicons.clearselection();
                    ImgTag = "";
                    Progressloader.loader(DashboardNewActivity.this);
                    searchparams.clear();
                    searchparams.put("lat", String.valueOf(

                            latitude));
                    searchparams.put("long", String.valueOf(longitude));
                    mVolleyService.getDataVolley("search", searchparams);
                    keyboardDown(autoCompleteTextView);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void showplacefragment(int requestcode) {
        try {
            //AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("PK").setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)//.setFilter(typeFilter)
                    .build(DashboardNewActivity.this);
            startActivityForResult(intent, requestcode);
        } catch (GooglePlayServicesRepairableException e) {
            isGooglePlayServicesAvailable(DashboardNewActivity.this);
        } catch (GooglePlayServicesNotAvailableException e) {
            isGooglePlayServicesAvailable(DashboardNewActivity.this);
        }

    }

    private void onResponseFinished(JSONArray response) {
        try {
            if (dataHelper.arrayArchitecht == null) {
                // mMap.clear();
                dataHelper.arrayArchitecht = new ArrayList<>();
            }
            if (response.length() > 0) {
                dataHelper.arrayArchitecht.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);

                    AllArchitectsDataModel dataModel = new AllArchitectsDataModel(this, jsonObject);
                    dataHelper.arrayArchitecht.add(dataModel);
                }
                plotAddressData();
            } else {
                Toast.makeText(this, "No record found", Toast.LENGTH_SHORT).show();
                mMap.clear();
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
    void plotAddressData() {

        //  mClusterManager = new ClusterManager<>(this, mMap);

        mClusterManager.clearItems();
        mMap.setOnCameraChangeListener(mClusterManager);
        if (dataHelper != null && dataHelper.arrayArchitecht.size() > 0) {

            isFoundArch = true;
            mMap.clear();
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                AllArchitectsDataModel arcData = dataHelper.arrayArchitecht.get(i);
                Log.d("usertyor",arcData.getUser_type());
                Double lat = 0.0;
                Double longitude = 0.0;
                if (!arcData.getLatitude().matches(""))
                    lat = Double.valueOf(arcData.getLatitude());
                if (!arcData.getLongitude().matches(""))
                    longitude = Double.valueOf(arcData.getLongitude());
                LOCATIONS_POINTS = new LatLng(lat, longitude);
                mClusterManager.addItem(new DashboardNewActivity.StringClusterItem("Marker #" + (i + 1), LOCATIONS_POINTS, i,arcData.getMembershipType(),arcData.getUser_type()));
            }
        } else {
            isFoundArch = false;
        }
        mClusterManager.cluster();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (isFoundArch)

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATIONS_POINTS, 10f));


            }
        });

        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterClickListener(this);

        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);






    }
    private HashMap searchparamsmethod(String id) {

        searchparams.clear();
        searchparams.put("category", id);
        searchparams.put("lat", String.valueOf(latitude));
        searchparams.put("long", String.valueOf(longitude));
        return searchparams;
    }
private HashMap searchparamsmethod1(String id) {

        searchparams.clear();
        searchparams.put("trem",id);
     /*   searchparams.put("lat", String.valueOf(latitude));
        searchparams.put("long", String.valueOf(longitude));*/
        return searchparams;
    }

    private void keyboardDown(View v) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
    }

    private AllArchitectsDataModel getUserData(LatLng position) {
        AllArchitectsDataModel allArchitectsDataModel = null;
        if (dataHelper.arrayArchitecht != null && dataHelper.arrayArchitecht.size() > 0) {
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                String latitude = String.valueOf(position.latitude);
                String longitude = String.valueOf(position.longitude);
                String curentLat = "";
                String curentLong = "";
                if (!dataHelper.arrayArchitecht.get(i).getLatitude().matches("")) {
                    curentLat = dataHelper.arrayArchitecht.get(i).getLatitude();
                }
                if (!dataHelper.arrayArchitecht.get(i).getLongitude().matches("")) {
                    curentLong = dataHelper.arrayArchitecht.get(i).getLongitude();
                }
                if (curentLat.matches(latitude) && curentLong.matches(longitude)) {
                    allArchitectsDataModel = dataHelper.arrayArchitecht.get(i);
                    return allArchitectsDataModel;
                }
            }
        }
        return allArchitectsDataModel;
    }

    private void showMarkerDetail(AllArchitectsDataModel userData) {
        MyHelper.currentUserData = userData;
        title.setText("Get banjaiga services");
        phoneNum = userData.getPhoneNumberONE();

        bitmapString = userData.getLogo();
        name.setText(userData.getUserName());
        contry.setText(userData.getArchaddress() + "\n" + userData.getViews() + " Views");
        Usertype = userData.getFlags();
        if (userData.getViews().matches("0") || userData.getViews().matches("")) {
            //tvReviews.setVisibility(View.INVISIBLE);
        } else {
//            tvReviews.setText(userData.getViews() + " Views");
        }
        if (userData.getIsEmail().matches("1")) {
//            ivEmailBadge.setVisibility(View.VISIBLE);
        }else {
            //  ivEmailBadge.setVisibility(View.GONE);

        } if (userData.getIsMobile().matches("1")) {
            // ivMobileBadge.setVisibility(View.VISIBLE);
        }else {
            // ivMobileBadge.setVisibility(View.GONE);

        } if (userData.getIsVerified().matches("1")) {
           // ivVerifiedBadge.setVisibility(View.VISIBLE);
            Picasso.with(this).load(userData.getLogo()).error(R.drawable.error_logo).into(logo);
        } else {
           // ivVerifiedBadge.setVisibility(View.GONE);
            Picasso.with(this).load(R.drawable.unverified).error(R.drawable.error_logo).into(logo);

        }


        if (userData.getMembershipType().matches("gold")) {
//            ivMedalBadge.setVisibility(View.VISIBLE);
            //ivMedalBadge.setImageResource(R.drawable.badgestargolden);
        } else if (userData.getMembershipType().matches("silver")) {
         //   ivMedalBadge.setVisibility(View.VISIBLE);
         //   ivMedalBadge.setImageResource(R.drawable.badgestarsilver);
        } else {
          //  ivMedalBadge.setVisibility(View.GONE);
        }
        if (userData.getFlags().equalsIgnoreCase("architect")) {
            //Ratingbasedprofile.setRating(Float.parseFloat(userData.getAvgServices()));

        } else {

            float total = 0;
            if (userData.getAvgQuality() != null)
                total += Float.parseFloat(userData.getAvgQuality());
            else if (userData.getAvgPrice() != null)
                total += Float.parseFloat(userData.getAvgPrice());
            else if (userData.getAvgServices() != null)
                total += Float.parseFloat(userData.getAvgServices());

            float average = total / 3;

         //   Ratingbasedprofile.setRating(average);

        }

        Log.d("Logo", userData.getLogo());
    }

    private void clearcardstodefault() {
        ivCardHome.setImageResource(R.drawable.projects_round);
        ivCardInterior.setImageResource(R.drawable.vendor_round);
        ivCardArch.setImageResource(R.drawable.man_greyround);
        ivCardCal.setImageResource(R.drawable.calculator_round);
        etContact.setText("");
        etUsername.setText("");
        etVendorContact.setText("");
        etVendorName.setText("");
        EMAILTAG = "";

    }

    private HashMap Become_memberparams(String name, String contact, String type) {
        searchparams.clear();
        searchparams.put("type", type);
        searchparams.put("name", name);
        searchparams.put("contact", contact);
        return searchparams;


    }

    private void goToArchticechClass() {
        startActivity(new Intent(DashboardNewActivity.this, ArchitechtActivity.class));
    }

    // Phone Call
    private void makeCall(String phone_num) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    // Send Message
    public void sendSMS(String phone_num) {
        // The number on which you want to send SMS
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone_num, null)));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                autoCompleteTextViewLoc.setText(String.valueOf(place.getName()));
                autoCompleteTextViewLoc.setSelection(autoCompleteTextViewLoc.getText().length());
               /* autoCompleteTextViewLoc.setFocusableInTouchMode(true);
                autoCompleteTextViewLoc.requestFocus();
               */
                LatLng destination = place.getLatLng();
                searchLatitude = destination.latitude;
                searchLongitude = destination.longitude;
                Showhidekeyboard = "no";

            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                etvendorLocation.setText(place.getName());
                LatLng destination = place.getLatLng();
                vendorLatitude = destination.latitude;
                vendorLongitude = destination.longitude;

            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE2) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                etvendorLocationdialog.setText(place.getName());
                LatLng destination = place.getLatLng();
                vendorLatitude = destination.latitude;
                vendorLongitude = destination.longitude;

            }
        }
    }

    @Override
    public void onResume() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        super.onResume();
    }

    @Override
    public void onStart() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onStart();
    }
}
