package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetArchitects_term_response {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("total_pages")
    @Expose
    private String totalPages;
    @SerializedName("result")
    @Expose
    private List<Get_architects_data> result ;

    @SerializedName("projects")
    @Expose
    private Object projects;
    @SerializedName("trusted_vendors")
    @Expose
    private List<Object> trustedVendors = null;


    /**
     * No args constructor for use in serialization
     *
     */

    public GetArchitects_term_response() {
    }

    /**
     *
     * @param projects
     * @param result
     * @param trustedVendors
     * @param status
     * @param totalPages
     */
    public GetArchitects_term_response(Integer status, String totalPages, List<Get_architects_data> result, Object projects, List<Object> trustedVendors) {
        super();
        this.status = status;
        this.totalPages = totalPages;
        this.result = result;
        this.projects = projects;
        this.trustedVendors = trustedVendors;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public List<Get_architects_data> getResult() {
        return result;
    }

    public void setResult(List<Get_architects_data> result) {
        this.result = result;
    }

    public Object getProjects() {
        return projects;
    }

    public void setProjects(Object projects) {
        this.projects = projects;
    }

    public List<Object> getTrustedVendors() {
        return trustedVendors;
    }

    public void setTrustedVendors(List<Object> trustedVendors) {
        this.trustedVendors = trustedVendors;
    }

}