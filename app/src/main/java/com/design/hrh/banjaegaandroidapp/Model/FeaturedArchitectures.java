package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/19/2018.
 */

public class FeaturedArchitectures {


    String ArchitectureName;
    int ArchitectureImage;
    String Rating;
    String ReviewsCount;


    public FeaturedArchitectures(String architectureName, int architectureImage, String rating, String reviewsCount) {
        ArchitectureName = architectureName;
        ArchitectureImage = architectureImage;
        Rating = rating;
        ReviewsCount = reviewsCount;
    }

    public FeaturedArchitectures(String architectureName, int architectureImage) {
        ArchitectureName = architectureName;
        ArchitectureImage = architectureImage;
    }

    public String getArchitectureName() {
        return ArchitectureName;
    }

    public void setArchitectureName(String architectureName) {
        ArchitectureName = architectureName;
    }

    public int getArchitectureImage() {
        return ArchitectureImage;
    }

    public void setArchitectureImage(int architectureImage) {
        ArchitectureImage = architectureImage;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getReviewsCount() {
        return ReviewsCount;
    }

    public void setReviewsCount(String reviewsCount) {
        ReviewsCount = reviewsCount;
    }
}
