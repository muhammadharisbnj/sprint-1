package com.design.hrh.banjaegaandroidapp.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Get_architects_data {
    @SerializedName("architect_id")
    @Expose
    private String architectId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("membership_type")
    @Expose
    private String membershipType;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("is_mobile")
    @Expose
    private String isMobile;
    @SerializedName("is_email")
    @Expose
    private String isEmail;
    @SerializedName("is_trusted")
    @Expose
    private String isTrusted;
    @SerializedName("is_featured")
    @Expose
    private String isFeatured;
    @SerializedName("cloud_tags")
    @Expose
    private String cloudTags;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("user_reviews")
    @Expose
    private String userReviews;
    @SerializedName("avg_service")
    @Expose
    private Object avgService;

    /**
     * No args constructor for use in serialization
     *
     */
    public Get_architects_data() {
    }

    /**
     *
     * @param isVerified
     * @param logo
     * @param phone
     * @param isEmail
     * @param isFeatured
     * @param isTrusted
     * @param userReviews
     * @param architectId
     * @param userType
     * @param city
     * @param country
     * @param membershipType
     * @param username
     * @param _long
     * @param address
     * @param views
     * @param userId
     * @param cloudTags
     * @param avgService
     * @param isMobile
     * @param lat
     * @param mobile
     */
    public Get_architects_data(String architectId, String userId, String username, String membershipType, String phone, String mobile, String userType, String logo, String city, String country, String address, String views, String isVerified, String isMobile, String isEmail, String isTrusted, String isFeatured, String cloudTags, String lat, String _long, String userReviews, Object avgService) {
        super();
        this.architectId = architectId;
        this.userId = userId;
        this.username = username;
        this.membershipType = membershipType;
        this.phone = phone;
        this.mobile = mobile;
        this.userType = userType;
        this.logo = logo;
        this.city = city;
        this.country = country;
        this.address = address;
        this.views = views;
        this.isVerified = isVerified;
        this.isMobile = isMobile;
        this.isEmail = isEmail;
        this.isTrusted = isTrusted;
        this.isFeatured = isFeatured;
        this.cloudTags = cloudTags;
        this.lat = lat;
        this._long = _long;
        this.userReviews = userReviews;
        this.avgService = avgService;
    }

    public String getArchitectId() {
        return architectId;
    }

    public void setArchitectId(String architectId) {
        this.architectId = architectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }

    public String getIsEmail() {
        return isEmail;
    }

    public void setIsEmail(String isEmail) {
        this.isEmail = isEmail;
    }

    public String getIsTrusted() {
        return isTrusted;
    }

    public void setIsTrusted(String isTrusted) {
        this.isTrusted = isTrusted;
    }

    public String getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getCloudTags() {
        return cloudTags;
    }

    public void setCloudTags(String cloudTags) {
        this.cloudTags = cloudTags;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getUserReviews() {
        return userReviews;
    }

    public void setUserReviews(String userReviews) {
        this.userReviews = userReviews;
    }

    public Object getAvgService() {
        return avgService;
    }

    public void setAvgService(Object avgService) {
        this.avgService = avgService;
    }

}