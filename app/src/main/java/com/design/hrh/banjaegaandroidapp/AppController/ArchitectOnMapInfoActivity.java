package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.design.hrh.banjaegaandroidapp.Adapters.FeaturedArchitecturesAdapter;

import com.design.hrh.banjaegaandroidapp.Adapters.Featured_architec_adapter;
import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.FeaturedArchitectures;
import com.design.hrh.banjaegaandroidapp.Model.YCategoryModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjects_list;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjectsall_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Get_architects_data;


import com.design.hrh.banjaegaandroidapp.ResponseModels.Result;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiClient;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiInterface;
import com.design.hrh.banjaegaandroidapp.retrofit.OnGetProjectsByArchitect;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArchitectOnMapInfoActivity extends AppCompatActivity implements FeaturedArchitecturesAdapter.OnItemClickListener,View.OnClickListener,OnGetProjectsByArchitect{
    Activity context = ArchitectOnMapInfoActivity.this;
    FeaturedArchitecturesAdapter mArchitecturesAdapter;
    RecyclerView rvFeaturedArchitectures,rvYCategory;
    ArrayList<YCategoryModel> YCategories = new ArrayList<>();
    ArrayList<FeaturedArchitectures> Architectures = new ArrayList<>();
    YCategoryAdapter mAdapter;
    Get_architects_data get_architects_data=null;
    RelativeLayout rlArchitectInfo;
    String ProjectJson;
    TextView textView,city;
    Featured_architec_adapter architec_adapter;

     CircleImageView circleImageView;

     Button call,messag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_architect_on_map_info);

        ProjectJson=getIntent().getStringExtra("Architectures_json");
        Gson gson=new Gson();
        get_architects_data=gson.fromJson(ProjectJson,Get_architects_data.class);


        MyHelper.name_=get_architects_data.getUsername();
        MyHelper.image_=get_architects_data.getLogo();
        MyHelper.address=get_architects_data.getAddress();
        MyHelper.number=get_architects_data.getPhone();


        init();
    }

    private void init() {
        call=(Button)findViewById(R.id.btn_call);
        messag=(Button)findViewById(R.id.btn_message);

        textView=(TextView)findViewById(R.id.txt_project_name);
        city=(TextView)findViewById(R.id.city);

        textView.setText(get_architects_data.getUsername());
        city.setText(get_architects_data.getAddress());

        circleImageView=(CircleImageView)findViewById(R.id.cireimage);
        Glide.with(context).load(get_architects_data.getLogo()).into(circleImageView);


        rlArchitectInfo=(RelativeLayout)findViewById(R.id.rl_archtect_info);
        rlArchitectInfo.setOnClickListener(this);
        rvYCategory=(RecyclerView) findViewById(R.id.rv_category_yellow);
        rvYCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        YCategories.add(new YCategoryModel("Architect", R.drawable.architect_grey));
        YCategories.add(new YCategoryModel("Tiles", R.drawable.tiles));
        YCategories.add(new YCategoryModel("Bathrooms", R.drawable.bathrooms_grey));
        YCategories.add(new YCategoryModel("Builder", R.drawable.builders_grey));
        YCategories.add(new YCategoryModel("Kitchen", R.drawable.kitchens_grey));
        YCategories.add(new YCategoryModel("More", R.drawable.more));
        mAdapter = new YCategoryAdapter(context, YCategories);
        //rvYCategory.setAdapter(mAdapter);


        rvFeaturedArchitectures = (RecyclerView) findViewById(R.id.rv_featured_architectures);
        rvFeaturedArchitectures.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
       /* Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
*/
        Apicall();
        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

call.setOnClickListener(this);
messag.setOnClickListener(this);
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onClick(View v) {

        if(v==rlArchitectInfo)
        {
            startActivity(new Intent(context,ArchitectProfileActivity.class));
        }if (v == call){
            MyHelper.makeCall(this,get_architects_data.getPhone());
        }if (v == messag){
            MyHelper.sendSMS(this,get_architects_data.getAddress());
        }

    }



    List<GetProjects_list> movies;
    private void Apicall() {
        Progressloader.loader(ArchitectOnMapInfoActivity.this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetProjectsall_response> call = apiService.getprojectresponse("24966");
        call.enqueue(new Callback<GetProjectsall_response>() {
            @Override
            public void onResponse(Call<GetProjectsall_response> call, Response<GetProjectsall_response> response) {
                Progressloader.hideloader();

                int statusCode = response.code();

                movies = response.body().getResult();

                mArchitecturesAdapter = new FeaturedArchitecturesAdapter(context, movies);
                rvFeaturedArchitectures.setAdapter(mArchitecturesAdapter);
                mArchitecturesAdapter.SetOnItemClickListener(ArchitectOnMapInfoActivity.this);
            }

            @Override
            public void onFailure(Call<GetProjectsall_response> call, Throwable t) {
                // Log error here since request failed
                //   Log.e(TAG, t.toString());
            }
        });
    }

//    List<GetProjectsSingle> ProjectsByArchitect;
//
//    private void GetProjectsByArchitect() {
//        Apicall webApiCall=new Apicall(this);
//        webApiCall.getProjectsByArchitect(this,"1");
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//        Call<GetProjectsSingle> call = apiService.getproject_user_id("1");
//        call.enqueue(new Callback<GetProjectsSingle>() {
//            @Override
//            public void onResponse(Call<GetProjectsSingle> call, Response<GetProjectsSingle> response) {
//                int statusCode = response.code();
//
//                ProjectsByArchitect = response.body().getResult();
//
//                architec_adapter = new Featured_architec_adapter(context,ProjectsByArchitect);
//                rvFeaturedArchitectures.setAdapter(architec_adapter);
////                architec_adapter.SetOnItemClickListener(this);
//
//
//            }
//
//            @Override
//            public void onFailure(Call<GetProjectsSingle> call, Throwable t) {
//                // Log error here since request failed
//                //   Log.e(TAG, t.toString());
//            }
//        });
//    }


    @Override
    public void onGetProjects(List<Result> projects) {


    }
}
