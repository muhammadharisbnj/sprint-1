package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by gippy on 4/23/2018.
 */

public class ProductsUsed {

    int ProductImage;


    public ProductsUsed(int productImage) {
        ProductImage = productImage;
    }

    public int getProductImage() {
        return ProductImage;
    }

    public void setProductImage(int productImage) {
        ProductImage = productImage;
    }


}
