package com.design.hrh.banjaegaandroidapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.design.hrh.banjaegaandroidapp.Model.ClientReviewsModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MuhammadAbubakar on 3/21/2018.
 */

public class AdapterImages extends RecyclerView.Adapter<AdapterImages.MyViewHolder> {
    private final Context context;

    private ArrayList<ClientReviewsModel> arrayClientReview;
    AdapterImages.OnItemClickListener mItemClickListener;

    public AdapterImages(Context context, ArrayList<ClientReviewsModel> arrayClientReview) {
        this.context = context;
        this.arrayClientReview = arrayClientReview;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView clientImageView;

        public MyViewHolder(View view) {
            super(view);
            clientImageView = view.findViewById(R.id.img_logo);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    @Override
    public AdapterImages.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_image, parent, false);

        return new AdapterImages.MyViewHolder(itemView);
        //    return null;
    }

    @Override
    public void onBindViewHolder(final AdapterImages.MyViewHolder holder, int position) {

        Picasso.with(context).load(arrayClientReview.get(position).getImageUrl()).error(R.drawable.appicon).into(holder.clientImageView);

       /* if (arraynavicons.get(position).isImagechanged()) {
            holder.Navicon.setImageResource(arraynavicons.get(position).getChangedrawable());

        } else {
            holder.Navicon.setImageResource(arraynavicons.get(position).getDrawable());
        }*/


    }


    @Override
    public int getItemCount() {
        return arrayClientReview.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final AdapterImages.OnItemClickListener mItemClickListeners) {
        this.mItemClickListener = mItemClickListeners;
    }

    /*public void changeimage(int index) {

          notifyItemChanged(index);

    }*/
}






