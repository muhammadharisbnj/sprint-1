package com.design.hrh.banjaegaandroidapp.AppController;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.design.hrh.banjaegaandroidapp.Adapters.AdapterImages;
import com.design.hrh.banjaegaandroidapp.Adapters.AdapterImgAndDesc;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.CSPreferences;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.ClientReviewsModel;
import com.design.hrh.banjaegaandroidapp.Model.SocialUserModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArchitechtActivity extends AppCompatActivity implements View.OnClickListener,
    GoogleApiClient.OnConnectionFailedListener {
    LoginButton loginfButton;
    CallbackManager callbackManager;
    MyHelper myHelper;
    SocialUserModel socialUserModel;
    LinearLayout socialLayout, ratingMenu;
    CircleImageView circleImageView;
    TextView tvHeading, tvAddressName, tvReview, tvCategory, tvViews;
    ImageView ivCall, ivMsg;
    ScaleRatingBar Ratingbasedoverall, Ratingbasedquality, Ratingbasedpricing, Rating_based_service, ratingInputData;
    String phoneNum, userid, userType;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    private static final String TAG = ArchitechtActivity.class.getSimpleName();
    ImageView buttonNext, buttonBack;
    int count = 0;
    float ratingPrice;
    float ratingQuality;
    float ratingService;
    Button buttonSubmit;
    GoogleApiClient mGoogleApiClient;
    Context con;
    SignInButton btnSignIn;
    private static final int RC_SIGN_IN = 007;
    LinearLayout ivDone, reviewsLayout;
    boolean socialLogin = false;
    EditText etSubmitReview;
    HashMap<String, String> valueshash = new HashMap<String, String>();
    RecyclerView rvImages;
    RecyclerView rvImagesAndDescription;
    RelativeLayout rlReview;
    TextView reviewsCounter, txtUserName, txtMsg;
    LinearLayout llOverall, llPricing, llQuality, llService;
    AccessToken token;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    private GoogleSignInOptions gso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_architecht);
        con = this;
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

/*
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(con)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .enableAutoManage((ArchitechtActivity) con*//* FragmentActivity *//*, this *//* OnConnectionFailedListener *//*)
                .addOnConnectionFailedListener(this)
                .addScope(new Scope(Scopes.PROFILE))
                .build();*/



        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        myHelper = MyHelper.getInstance(ArchitechtActivity.this);
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        // Customizing G+ button
        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        loginfButton = (LoginButton) findViewById(R.id.login_button);
        initVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback, this);


        setUpFaceBook();
        init();
        //getvendordata();
        getClientReviews();
        OnClickCalls();
        VolleyCallRatings();
        checkfbloginstatus();
        fblogoutstatus();


    }

    private void fblogoutstatus() {
        AccessTokenTracker tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) {

                    prefs.edit().clear();

                }
            }
        };
    }

    private void checkfbloginstatus() {

        token = AccessToken.getCurrentAccessToken();
        if (token != null) {
            count = 1;
            socialLayout.setVisibility(View.GONE);
            manageRatingView();
            socialLogin = true;
            txtMsg.setText("Please submit your reviews");
            txtUserName.setVisibility(View.VISIBLE);
            txtUserName.setText("Welcome " + prefs.getString("Fb_Name", "Username"));
            btnSignIn.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getClientReviews() {
        mVolleyService.getClientReviews("getUserReviews", userid);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                startActivity(new Intent(ArchitechtActivity.this, DashboardNewActivity.class));
                                finish();
                            }
                        }
                    });
        }
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    private void VolleyCallRatings() {
        Progressloader.loader(ArchitechtActivity.this);
        mVolleyService.getvendorRating("GETCALL", "getUserRating/" + userid);
    }

    private void OnClickCalls() {
        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall(phoneNum);
            }
        });

        ivMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS(phoneNum);
            }
        });
        if (count == 0) {
            buttonBack.setVisibility(View.INVISIBLE);
            buttonNext.setVisibility(View.INVISIBLE);
            socialLayout.setVisibility(View.VISIBLE);
        }

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    manageRatingView();
                } else if (count > 0 && count < 6) {
                    count = count - 1;
                    manageRatingView();
                }
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 1) {
                    saveVendorRating();
                    count = count + 1;
                    manageRatingView();
                } else if (count > 1 && count < 6) {
                    saveVendorRating();
                    count = count + 1;
                    manageRatingView();
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //SocialUserModel socialUserModel = myHelper.socialUserModels.get(0);
                    String username = null;
                    username = prefs.getString("Fb_Name", "Username");
                    String review =etSubmitReview.getText().toString();
                    valueshash.clear();
                    valueshash.put("user_id", userid);
                    valueshash.put("username", username);
                    valueshash.put("email", prefs.getString("Fb_Email","Null"));
                    valueshash.put("user_type", userType);
                    valueshash.put("image_url",prefs.getString("Fb_Image","Null") );
                    valueshash.put("service", String.valueOf(ratingService));
                    valueshash.put("pricing", String.valueOf(ratingPrice));
                    valueshash.put("quality", String.valueOf(ratingQuality));
                    valueshash.put("review", review);

                    Progressloader.loader(ArchitechtActivity.this);
                   /* String review = URLEncoder.encode(etSubmitReview.getText().toString());
                    String param = "saveUserRating" + "/" + "banjaiga/" + userid + "/" + username + "/"
                            + socialUserModel.getUserEmail() + "/" + userType + "/" + socialUserModel.getImageUrl() + "/" + ratingService + "/"
                            + ratingPrice + "/" + ratingQuality + "/" + review;*/

                    mVolleyService.saveUserRatings(valueshash, "POST");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

   /* private void getvendordata() {
        Intent intent = getIntent();
        if(!MyHelper.currentUserData.getLogo().matches(""))
            Picasso.with(this).load(MyHelper.currentUserData.getLogo()).error(R.drawable.error_logo).into(circleImageView);
        else
            Picasso.with(this).load(R.drawable.error_logo).error(R.drawable.error_logo).into(circleImageView);

        tvHeading.setText(MyHelper.currentUserData.getUserName());
        tvAddressName.setText(MyHelper.currentUserData.getArchaddress());
        if(MyHelper.currentUserData.getViews().matches("") || MyHelper.currentUserData.getViews().matches("0") )
            tvViews.setText("0 Views");
        else
            tvViews.setText( MyHelper.currentUserData.getViews() + " Views");
        phoneNum = MyHelper.currentUserData.getPhoneNumberONE();
        userid = MyHelper.currentUserData.getArchId();
        userType = MyHelper.currentUserData.getFlags();

        if (userType.matches("architect")) {
            llService.setVisibility(View.VISIBLE);
            llOverall.setVisibility(View.VISIBLE);
            llQuality.setVisibility(View.GONE);
            llPricing.setVisibility(View.GONE);
        }
    }
*/
    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        llOverall = (LinearLayout) findViewById(R.id.ll_overall);
        llPricing = (LinearLayout) findViewById(R.id.ll_pricing);
        llQuality = (LinearLayout) findViewById(R.id.ll_quality);
        llService = (LinearLayout) findViewById(R.id.ll_service);
        rvImages = (RecyclerView) findViewById(R.id.images_rl);
        reviewsCounter = (TextView) findViewById(R.id.reviews_counter);
        rvImagesAndDescription = (RecyclerView) findViewById(R.id.rv_client_desc);
        rlReview = (RelativeLayout) findViewById(R.id.rl_review);
        txtUserName = (TextView) findViewById(R.id.txt_User_Name);
        txtMsg = (TextView) findViewById(R.id.txt_msg);
        etSubmitReview = (EditText) findViewById(R.id.et_submit_review);
        tvHeading = (TextView) findViewById(R.id.tvHeading);
        tvAddressName = (TextView) findViewById(R.id.tvaddress_name);
        tvReview = (TextView) findViewById(R.id.review);
        tvViews = (TextView) findViewById(R.id.tv_views);
        tvCategory = (TextView) findViewById(R.id.category);
        buttonNext = (ImageView) findViewById(R.id.btn_next);
        buttonBack = (ImageView) findViewById(R.id.btn_back);
        buttonSubmit = (Button) findViewById(R.id.btn_submit);
        ivDone = (LinearLayout) findViewById(R.id.lv_done);
        reviewsLayout = (LinearLayout) findViewById(R.id.reviews_layout);
        ivDone.setVisibility(View.GONE);
        Ratingbasedoverall = (ScaleRatingBar) findViewById(R.id.rating_based_overall);
        Rating_based_service = (ScaleRatingBar) findViewById(R.id.rating_based_service);
        Ratingbasedpricing = (ScaleRatingBar) findViewById(R.id.rating_based_pricing);
        Ratingbasedquality = (ScaleRatingBar) findViewById(R.id.rating_based_quality);
        ratingInputData = (ScaleRatingBar) findViewById(R.id.rating_input_data);
        Ratingbasedoverall.setIsIndicator(true);
        Ratingbasedquality.setIsIndicator(true);
        Rating_based_service.setIsIndicator(true);
        Ratingbasedpricing.setIsIndicator(true);

        circleImageView = (CircleImageView) findViewById(R.id.img_logo);
        ivCall = (ImageView) findViewById(R.id.img_call);
        ivMsg = (ImageView) findViewById(R.id.img_msg);

        socialLayout = (LinearLayout) findViewById(R.id.social_lay);
        ratingMenu = (LinearLayout) findViewById(R.id.rating_menu);
        rvImages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvImagesAndDescription.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //recyclerView = (RecyclerView) findV iewById(R.id.rv_layout);
        //recyclerView.setLayoutManager(layoutManager);

    }


    private void setUpFaceBook() {
        loginfButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginfButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                count = 1;
                socialLayout.setVisibility(View.GONE);
                manageRatingView();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    socialLogin = true;
                                    myHelper.socialUserModels = new ArrayList<>();
                                    socialUserModel = new SocialUserModel(object, "FB");
                                    myHelper.socialUserModels.add(socialUserModel);
                                    txtUserName.setVisibility(View.VISIBLE);
                                    txtMsg.setText("Please submit your reviews");
                                    txtUserName.setText("Welcome " + myHelper.socialUserModels.get(0).getUserName());

                                    CSPreferences.putString(getApplication(),"fb_email",myHelper.socialUserModels.get(0).getUserEmail());
                                    CSPreferences.putString(getApplication(),"fb_image",String.valueOf(myHelper.socialUserModels.get(0).getImageUrl()));
                                    CSPreferences.putBolean(getApplication(),"fb_login",true);
                                    editor.putString("Fb_Name", myHelper.socialUserModels.get(0).getUserName());
                                    editor.putString("Fb_Email",myHelper.socialUserModels.get(0).getUserEmail());
                                    editor.putString("Fb_Image", String.valueOf(myHelper.socialUserModels.get(0).getImageUrl()));
                                    editor.commit();

                                    startActivity(new Intent(ArchitechtActivity.this,VendorRatingNewActivity.class));
                                   // btnSignIn.setVisibility(View.GONE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }// 01/31/1980 format
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                try {
                    Log.v("LoginActivity", exception.getCause().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("ssssssssss", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            loginfButton.setVisibility(View.GONE);
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e("name", "display name: " + acct.getDisplayName());
            myHelper.socialUserModels = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", acct.getId());
                jsonObject.put("name", acct.getDisplayName());
                jsonObject.put("email", acct.getEmail());
                jsonObject.put("imageUrl", acct.getPhotoUrl().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            SocialUserModel socialUserModel = new SocialUserModel(jsonObject, "Google");
            myHelper.socialUserModels.add(socialUserModel);
            txtUserName.setVisibility(View.VISIBLE);
            txtUserName.setText("Welcome " + myHelper.socialUserModels.get(0).getUserName());
            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            count = 1;
            startActivity(new Intent(ArchitechtActivity.this,VendorRatingNewActivity.class));
            //socialLayout.setVisibility(View.GONE);
            manageRatingView();
            Log.e("ffffffffffffffffffff", "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);
            updateUI(true);
        } else {
            updateUI(false);
            // Signed out, show unauthenticated UI.
        }
    }

    private void makeCall(String phone_num) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }
    public void sendSMS(String phone_num) {
        // The number on which you want to send SMS
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone_num, null)));
    }



    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            Toast.makeText(this, "login", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "login fail", Toast.LENGTH_SHORT).show();
        }

    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String servicename, JSONObject response) {
                Progressloader.hideloader();

                try {
                    onSuccess(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifySuccess(String servicename, JSONArray response) {
                //     mSearchviewTag.clearQuery();

                Log.d(TAG, "Volley requester " + servicename);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySucessEmail(String servicename, JSONObject response) {


            }

            @Override
            public void notifyRatingSuccess(String servicename, JSONObject response) {

                Progressloader.hideloader();
                onResponseFinished(response);
            }

            @Override
            public void notifyLocationsucess(String servicename, JSONObject response) {

            }

            @Override
            public void notifyClientReviews(String servicename, JSONObject response) {
                 Progressloader.hideloader();
                onClientReviewsSuccess(response);
            }

            @Override
            public void notifyError(String servicename, VolleyError error) {
                Progressloader.hideloader();
                Log.d(TAG, "Volley requester " + servicename);
                Log.d(TAG, "Volley JSON post" + "That didn't work!");

                if (error instanceof NetworkError) {

                    Toast.makeText(ArchitechtActivity.this,
                            "Please check your internet connection and try again",
                            Toast.LENGTH_LONG).show();

                    //TODO
                } else if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(ArchitechtActivity.this,
                            "Something went wrong please try again",
                            Toast.LENGTH_LONG).show();
                }

        }
        };
    }

    private void onClientReviewsSuccess(JSONObject response) {
        DataHelper dataHelper = DataHelper.getInstance();
        if (dataHelper.arrayClientReviews == null)
            dataHelper.arrayClientReviews = new ArrayList<>();
            dataHelper.arrayClientReviews.clear();
        try {
            JSONArray jsonArray = response.getJSONArray("Response");
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ClientReviewsModel clientReviewsModel = new ClientReviewsModel(jsonObject);
                    dataHelper.arrayClientReviews.add(clientReviewsModel);
                }
                if (dataHelper.arrayClientReviews.size() > 5) {
                    int remai = dataHelper.arrayClientReviews.size() - 5;
                    reviewsCounter.setText(" +" + remai);
                }
                AdapterImages adapterImages = new AdapterImages(con, dataHelper.arrayClientReviews);
                rvImages.setAdapter(adapterImages);
                AdapterImgAndDesc adapterImgAndDesc = new AdapterImgAndDesc(con, dataHelper.arrayClientReviews);
                rvImagesAndDescription.setAdapter(adapterImgAndDesc);
            } else {
                rlReview.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void onResponseFinished(JSONObject response) {
        try {
            float total = 0;
            if (response.getString("avg_quality").matches("null"))
                response.put("avg_quality", "0.0");
            if (response.getString("avg_pricing").matches("null"))
                response.put("avg_pricing", "0.0");
            if (response.getString("avg_service").matches("null"))
                response.put("avg_service", "0.0");

            Ratingbasedquality.setRating(Float.parseFloat(response.getString("avg_quality")));
            Ratingbasedpricing.setRating(Float.parseFloat(response.getString("avg_pricing")));
            Rating_based_service.setRating(Float.parseFloat(response.getString("avg_service")));

               /* if (jsonObject.has("service"))
                    Rating_based_service.setRating(Float.parseFloat(jsonObject.getString("service")));
                else if (jsonObject.has("quality"))
                    Ratingbasedoverall.setRating(Float.parseFloat(jsonObject.getString("quality")));
                else if (jsonObject.has("price"))
                    Ratingbasedpricing.setRating(Float.parseFloat(jsonObject.getString("price")));*/


            if (Ratingbasedquality != null)
                total += Ratingbasedquality.getRating();
            else if (Ratingbasedpricing != null)
                total += Ratingbasedpricing.getRating();
            else if (Rating_based_service != null)
                total += Rating_based_service.getRating();

            float average = total / 3;
            if (Ratingbasedoverall != null)
                Ratingbasedoverall.setRating(average);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void saveVendorRating() {
        if (count == 1) {
            ratingQuality = ratingInputData.getRating();
            ratingInputData.setRating(0);
        } else if (count == 2) {
            ratingPrice = ratingInputData.getRating();
            ratingInputData.setRating(0);

        } else if (count == 3) {
            ratingService = ratingInputData.getRating();
            ratingInputData.setRating(0);

        }
    }

    private void manageRatingView() {
        if (count == 0) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            buttonNext.setVisibility(View.INVISIBLE);
            buttonBack.setVisibility(View.INVISIBLE);
            socialLayout.setVisibility(View.VISIBLE);
            ratingMenu.setVisibility(View.GONE);
        } else if (count == 1) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
            buttonBack.setVisibility(View.VISIBLE);
            ratingMenu.setVisibility(View.VISIBLE);
            tvCategory.setText("Quality");
        } else if (count == 2) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
            buttonBack.setVisibility(View.VISIBLE);
            tvCategory.setText("Price");
        } else if (count == 3) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
            buttonBack.setVisibility(View.VISIBLE);
            tvCategory.setText("Service");
        } else if (count == 4) {
            etSubmitReview.setVisibility(View.VISIBLE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
        } else if (count == 5) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.VISIBLE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        signIn();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void onSuccess(JSONObject object) throws JSONException {
        Toast.makeText(con, object.getString("Message"), Toast.LENGTH_SHORT).show();
       // logoutSocial();
        etSubmitReview.setText("");
        checkfbloginstatus();
        getClientReviews();
        etSubmitReview.setVisibility(View.GONE);
        buttonSubmit.setVisibility(View.GONE);
        socialLayout.setVisibility(View.GONE);
        ratingMenu.setVisibility(View.GONE);
        txtMsg.setText("Thank you for submiting reviews");
        txtUserName.setVisibility(View.GONE);
        buttonBack.setVisibility(View.INVISIBLE);
        buttonNext.setVisibility(View.INVISIBLE);




    }

    private void logoutSocial() {
        // LoginManager.getInstance().logOut();
        startActivity(new Intent(ArchitechtActivity.this, DashboardNewActivity.class));
        finish();
    }

}
//https://www.banjaiga.com/web_services/index.php/saveUserRating/banjaiga/5141224/aliraza/raza@banjaiga.com/architect/https://www.banjaiga.com/themes/banjaiga/img/aliRazaH.jpg/5/5/5/very%20good%20services
///