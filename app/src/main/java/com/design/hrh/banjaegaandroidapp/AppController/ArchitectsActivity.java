package com.design.hrh.banjaegaandroidapp.AppController;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.design.hrh.banjaegaandroidapp.Adapters.FeaturedArchitectsAdapter;
import com.design.hrh.banjaegaandroidapp.Adapters.Featured_architec_adapter;
import com.design.hrh.banjaegaandroidapp.Adapters.YCategoryAdapter;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.YCategoryModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetArchitects_term_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjects_list;
import com.design.hrh.banjaegaandroidapp.ResponseModels.GetProjectsall_response;
import com.design.hrh.banjaegaandroidapp.ResponseModels.Get_architects_data;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiClient;
import com.design.hrh.banjaegaandroidapp.retrofit.ApiInterface;
import com.design.hrh.banjaegaandroidapp.retrofit.Apicall;
import com.design.hrh.banjaegaandroidapp.retrofit.Ongetarchitects;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArchitectsActivity extends AppCompatActivity implements Featured_architec_adapter.OnItemClickListener ,FeaturedArchitectsAdapter.OnArchitectItemClickListener,Ongetarchitects{
    Activity context = ArchitectsActivity.this;
    YCategoryAdapter mAdapter;
    ArrayList<YCategoryModel> YCategories = new ArrayList<YCategoryModel>();
    RecyclerView rvFeaturedArchitectures, rvFeaturedArchitects,rvYCategory;
    FeaturedArchitectsAdapter mArchitectsAdapter;



    Featured_architec_adapter architec_adapter;


   // ArrayList<FeaturedArchitects> Architects = new ArrayList<>();
   List<GetProjects_list> movies;
   List<Get_architects_data> get_architects_data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_architects);
        Apicall();
        SecondAPi();
        init();
    }

    private void init() {

        ImageView back=(ImageView)findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rvFeaturedArchitects = (RecyclerView) findViewById(R.id.rv_featured_architects);



        rvFeaturedArchitectures = (RecyclerView) findViewById(R.id.rv_featured_architectures);


        rvFeaturedArchitects.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        rvFeaturedArchitectures.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        rvYCategory=(RecyclerView) findViewById(R.id.rv_category_yellow);
        rvYCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        YCategories.add(new YCategoryModel("Architect", R.drawable.architecturalselected));
        YCategories.add(new YCategoryModel("Tiles", R.drawable.designers_gold));
        YCategories.add(new YCategoryModel("Bathrooms", R.drawable.interior_gold));
        YCategories.add(new YCategoryModel("Builder", R.drawable.landscaping_gold));
        YCategories.add(new YCategoryModel("Kitchen", R.drawable.planner_gold));
        YCategories.add(new YCategoryModel("More", R.drawable.urban_gold));
        mAdapter = new YCategoryAdapter(context, YCategories);
        rvYCategory.setAdapter(mAdapter);

      /*  Architects.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        Architects.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        Architects.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        Architects.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
        Architects.add(new FeaturedArchitects("",R.drawable.profile_image_with_badge,"",""));
*/

      /*  Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
        Architectures.add(new FeaturedArchitectures("",R.drawable.project,"",""));
*/

      //  mArchitectsAdapter = new FeaturedArchitectsAdapter(context, Architects);



       // mArchitecturesAdapter = new FeaturedArchitecturesAdapter(context, Architectures);

        // Featured architects&Practises


        //Featured architects
      //  rvFeaturedArchitectures.setAdapter(mArchitecturesAdapter);




       // architec_adapter.SetOnItemClickListener(this);
     //   rvYCategory.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {

        Gson gson = new Gson();
        String json =gson.toJson(movies.get(position));
        startActivity(new Intent(ArchitectsActivity.this,ProjectActivity.class).putExtra("ProjectJson",json));



    }

    @Override
    public void OnArchitectItemClick(View view, int position) {
        Gson gson = new Gson();
        String json =gson.toJson(get_architects_data.get(position));
        startActivity(new Intent(ArchitectsActivity.this,ArchitectOnMapInfoActivity.class).putExtra("Architectures_json",json));

    }

    private void Apicall() {
        Progressloader.loader(ArchitectsActivity.this);
        Apicall webApiCall=new Apicall(this);
        webApiCall.getEventsList(this,"1");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetProjectsall_response> call = apiService.getProjectsall("1");
        call.enqueue(new Callback<GetProjectsall_response>() {
            @Override
            public void onResponse(Call<GetProjectsall_response> call, Response<GetProjectsall_response> response) {
                Progressloader.hideloader();

                int statusCode = response.code();

                movies = response.body().getResult();

                architec_adapter = new Featured_architec_adapter(ArchitectsActivity.this,movies);
                rvFeaturedArchitectures.setAdapter(architec_adapter);
                architec_adapter.SetOnItemClickListener(ArchitectsActivity.this);


            }

            @Override
            public void onFailure(Call<GetProjectsall_response> call, Throwable t) {
                // Log error here since request failed
                //   Log.e(TAG, t.toString());
            }
        });
    }


   private void SecondAPi() {
        Apicall webApiCall=new Apicall(this);
        webApiCall.getEventsList(this,"1");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<GetArchitects_term_response> call = apiService.get_architects_userid("furniture");
        call.enqueue(new Callback<GetArchitects_term_response>() {
            @Override
            public void onResponse(Call<GetArchitects_term_response> call, Response<GetArchitects_term_response> response) {
                int statusCode = response.code();
                get_architects_data=response.body().getResult();


                mArchitectsAdapter=new FeaturedArchitectsAdapter(context,get_architects_data);
                rvFeaturedArchitects.setAdapter(mArchitectsAdapter);
                mArchitectsAdapter.SetOnItemClickListener(ArchitectsActivity.this);


            }

            @Override
            public void onFailure(Call<GetArchitects_term_response> call, Throwable t) {
                // Log error here since request failed
                //   Log.e(TAG, t.toString());
            }
        });
    }






    @Override
    public void Ongetarchitects(ArrayList<GetProjects_list> architects_lists) {
        this.movies.clear();
        this.movies.addAll(architects_lists);
        System.err.println(architects_lists.size()+"::::::Architectlist");

    }


}
